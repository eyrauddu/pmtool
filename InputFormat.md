# Input format specification

`pmtool` accepts two different input formats for specifying instances: an in-house format,
and one based on `librec` which allows easy interaction with `StarPU`. Examples of each format
can be found in the `data/` directory: files with the `.pm` extension follow the in-house format, files with the `.rec`
extension follow the recfile format.  
 


## In-house format
 
In the default in-house format, all the information about an instance
is specified in one file, specified with several arrays. Each array
starts with a `[`, ends with a `]`, and contains a comma-separated
list of values.
  
A file must contain, in this order, the following arrays:
+ the number of ressource of each type. The length of this array
specifies the number of resource types, and its $`j`$-th coordinate
indicates how many resources of type $`j`$ are available.
+ one array per resource type, specifying the execution time of tasks on this resource type. 
All these arrays should have the same length, equal to the number of task types. The $`i`$-th coordinate of the 
$`j`$-th array specifies the execution time of task type $`i`$ on resource type $`j`$. 
+ one array specifies the type of each task. The length of this array is equal to the number $`n`$ of tasks in 
the instance, and the $`k`$-th coordinate specifies the type of task $`k`$. Values in this array should be 
between 0 and the length of the "execution time" arrays minus one.
+ one array per task, specifying the precedence constraints. The $`k`$-th array indicates the indices (between $`1`$ and $`n`$)
of the predecessors of task $`k`$.  
 
Arrays do not have to be separated, however it is good practice in my opinion to keep one per line.
Task numbering can be arbitrary (and does not have to follow dependencies), `pmtool` automatically computes a 
topological ordering of the graph anywhere it is needed, and thus checks that there is no cycle in the graph.  
 
## Recfile format
 
This format uses a more standard representation (the same that Debian uses in its package system, and that StarPU uses
as an output). It is more verbose, more extensible, and allows to differentiate between the graph and the 
platform description. `pmtool` automatically uses this format if the file name ends with `.rec`.  
 
In the recfile format, information is specified as *records*, which contain one *key/value* pair per line. The precise
format is `Key: Value`, where the value may be anything (and may contain spaces). The key and value are separated by `: `.
Records are separated by double newlines. 
 
### The instance file
 
The format of the instance file is designed as to accept the `tasks.rec` files as provided by StarPU's export tools. 
The instance file contains one record per task, with the following keys:

+ `Model` and `Footprint` specify the task type. All tasks with identical names and footprints belong to the same task type. 
+ `JobId` is an integer identifier of the task
+ `DependsOn` contains a space-separated list of the `JobId`s of the predecessor of the current task.
+ `EstimatedTime` contains a space-separated list of the execution time of this task on each resource (if there are 
  several resources of the same type, the corresponding value is repeated as many times as needed). The number of values
  in this field should be the same for all tasks. 
 
The fields `Name`, `Footprint` and `JobId` are compulsory. The field `EstimatedTime` is compulsory if no platform file 
is provided. The field `DependsOn` is optional (defaults to no dependencies). Other optional fields exist:

+ `SubmitOrder` provides another, more robust identifier from StarPU, which allows to identify tasks from one StarPU run 
to the next. It is used as an identifier of tasks in exported schedules if the `--subimt-order` option is specified
on the command line. It should be an integer. 
+  `Tag` is a custom information provided by the application programmer in StarPU, providing another way of identifying tasks 
in a user-defined manner. It is appended to the task identifier (either job id or submit order) if the 
`--use-tags` option is specified on the command line. 
+ `WorkerId`, `StartTime` and `EndTime` allow to specify a schedule, providing a convienient way to compare 
the actual schedule from StarPU to schedules computed with `pmtool`'s algorithms. If these fields are specified for
all tasks, then a shared data is added to the instance with the key `rl`; the schedule can then be recovered 
by adding the `rep` (reproduce) algorithm to the command line, with the option `key=rl`, in the following way:
```
pmtool tasks.rec -a rep:key=rl
``` 
+ `Handles`, `Modes` and `Sizes` allow to specify which data this task
  requires and produces. All three are space-separated lists, which
  should be the same size. `Handles` contains strings which are
  identifiers of data objects. `Modes` can be either `R`, `W` or `RW`,
  and indicates whether the corresponding data object is read,
  written, or both. `Sizes` are integers and indicate the size of this
  object. The parser performs *versioning* of these data objects
  according to the precedence specified by the `DependsOn` field: a
  data object read by a task is matched to the last data object
  produced with the same handle, in a topological ordering of the
  dependencies.
  
  It is expected, but not checked, that tasks which write on a data
  object have dependencies with all tasks writing or reading the same
  data object. It is also expected that data accessed in read mode
  have the same size as the data produced by the previous task which
  wrote to this handle. Tasks which write to a handle may change its
  size.
 
Because of internal behavior of StarPU, this format allows *virtual* tasks to be added to the instance. A task is virtual 
if it has no `Name` field. `JobId` is still compulsory for virtual tasks. Dependencies from real tasks go *through* virtual tasks
to make them depend on any real task that this virtual task depends on. 

### The platform file
 
To avoid specifying `EstimatedTime` for all tasks, it is possible to
specify the platform file separately. In StarPU, this file is provided
by the `starpu_perfmodel_recdump` utility in the `tools/`
directory. This file contains four parts: the number of resources of
each type, the assignment of resources to memory nodes, the
communication performance between memory nodes, and the timings of
each type of task.
 
The first part starts with ```%rec: worker_count``` on a separate line
(this is a special feature of the recfile format to separate a file
into different databases). It then contains one record per resource
type, with two fields:

+ `Architecture` provides an identifier for this resource type
+ `NbWorkers` should contain an integer specifying the number of resources of this type
 
The second part starts with ```%rec: memory_workers``` on a separate
line, and contains one record for each task memory node.  Records
contain the following fields:

+ `MemoryNode` indicates the index of the memory node.
+ `Name` represents its name.
+ `Size` indicates the memory size in bytes, -1 for an unbounded memory.
+ `Workers` contain the list of workers that belong to that memory node.

The third part starts with `%rec: memory_performance` on a separate
line, and contains one record for each pair of different memory
nodes. Records contain the following fields:

+ `MemoryNodeSrc` and `MemoryNodeDst` indicate the source and
  destination node respectively.
+ `Bandwidth` and `Latency` indicate the bandwidth and latency, in
  units compatible with the duration of tasks in the `timing` section,
  and with the size of handles in the `tasks.rec` file. In StarPU,
  this is respectively microseconds and bytes, so the latency is given
  in microseconds and the bandwidth in bytes per microseconds. 
 
The second part starts with ```%rec: timing``` on a separate line, and
contains one record for each task type/resource type combination.
Records contain the following fields:

+ `Model` and `Footprint` represent the task type, similarly to the instance file.
+ `Architecture` represent the resource type, as specified in the first part. 
+ `Mean` contain the execution time (which is computed as an average by StarPU).
+ Files produced by StarPU also contain an `Stddev` field, which contains the standard deviation 
of the measurements, but this field is ignored by `pmtool`.
 
    
 
 
