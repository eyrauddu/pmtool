cmake_minimum_required(VERSION 3.0.2)

project(pmtool)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}  ${PROJECT_SOURCE_DIR})
add_definitions("--std=c++11")
include_directories("include")

option(PROFILING "Include profiling information" OFF)

if(PROFILING)
  # I do not like this way of doing things, but did not find anything else.
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pg")
  SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -pg")
  SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -pg")
endif()

option(DEBUG "Include debug information" ON)

if(DEBUG)
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g")
else()
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3")
endif()


find_package(LibRec REQUIRED)
include_directories(${LIBREC_INCLUDE_DIRS})

set(THREADS_PREFER_PTHREAD_FLAG)
find_package(Threads REQUIRED)

find_package(CPLEX)
if(CPLEX_FOUND)
  include_directories(${CPLEX_INCLUDE_DIRS})
  #  target_link_libraries(${CPLEX_LIBRARIES})
  add_definitions("-DWITH_CPLEX")
  add_definitions("-DIL_STD")
endif()

add_subdirectory("core")
add_subdirectory("schedulers")

if(CPLEX_FOUND)
  add_subdirectory("bounds")
endif()

set(PROG_SRC
  pmtool.cpp
  ProgramOptions.cpp
  )

add_executable(pmtool ${PROG_SRC})

target_link_libraries(pmtool schedulers)

if(CPLEX_FOUND)
  target_link_libraries(pmtool bounds)
  target_link_libraries(pmtool ${CPLEX_LIBRARIES})
endif()

target_link_libraries(pmtool core)
target_link_libraries(pmtool ${LIBREC_LIBRARIES})

if(THREADS_FOUND)
  target_link_libraries(pmtool Threads::Threads)
endif()

install(TARGETS pmtool RUNTIME DESTINATION bin/)
add_executable(instanceinfo instanceinfo.cpp)
target_link_libraries(instanceinfo core)
