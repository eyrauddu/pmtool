#include <iostream>
#include <getopt.h>
#include <string>
#include <vector>

#include "instance.h"
#include "util.h"

using namespace std;


static const int opt_no_convert = 10;
static const int opt_no_header = 12;

static struct option long_options[] = {
  {"no-convert",   no_argument, 0,  opt_no_convert},
  {"no-header", no_argument, 0, opt_no_header},
  {0,         0,                 0,  0 }
};

static const char* optstring = "";

int main(int argc, char* argv[]) {

  
  bool convertIndices = true;
  bool printHeader = true; 
  vector<string> inputFiles; 

  int longindex = 0; 
  int opt = getopt_long(argc, argv, optstring, long_options, & longindex);
  while(opt != -1) {
    switch(opt) {
    case opt_no_convert:
      convertIndices = false;
      break;
    case opt_no_header:
      printHeader = false;
      break;
    default:
      break;
    }
    opt = getopt_long(argc, argv, optstring, long_options, & longindex);
  }
  for(int a = optind; a < argc; ++a)
    inputFiles.push_back(string(argv[a]));

  if(printHeader) {
    cout << "input nbTasks depth" << endl; 
  }
  
  for(string &file: inputFiles) {
    Instance* instance = new Instance(file, convertIndices);
    int nbTasks = instance->nbTasks;
    int depth = getMax(instance->computeUnitRank());
    cout << file << " " << nbTasks << " " << depth << endl;
    delete instance; 
  }
  
}
