#include <iostream>
#include <assert.h>
#include "SynchroMap.h"

using namespace std;

SynchroMap::SynchroMap() {
}

// If you want more performance when using many keys, have this function
// return an iterator to the newly inserted pair
// Need to ensure safety though. 
// Assumes mutex is already taken, anyway. 
void SynchroMap::link(const string & key) {
  std::promise<void*>*  prom = new std::promise<void*>();
  std::shared_future<void*> fut = prom->get_future().share();
  dict[key] = make_pair(prom, fut);
}


void SynchroMap::insert(const string &key, void* value) {
  {
    lock_guard<mutex> lock(mtx); 
    if(dict.find(key) == dict.end())
      link(key);
    auto &pair = dict[key];
    if(pair.first) {
      pair.first->set_value(value);
      free(pair.first);
      pair.first = NULL; 
    }
  }
}

void SynchroMap::replace(const string &key, void* value) {
  {
    lock_guard<mutex> lock(mtx); 
    auto iterator = dict.find(key);
    assert(iterator != dict.end());
    assert(!iterator->second.first);

    link(key); 
    auto &pair = dict[key];
    pair.first->set_value(value);
    free(pair.first);
    pair.first = NULL; 
  }
}

void* SynchroMap::get(const string & key) {
  std::pair< std::promise<void*>*, std::shared_future<void*> > pair; 
  {
    lock_guard<mutex> lock(mtx); 
    if(dict.find(key) == dict.end())
      link(key);
    pair = dict[key];
  }
  return pair.second.get(); 
}

bool SynchroMap::isPresent(const string & key) {
  {
    lock_guard<mutex> lock(mtx); 
    if(dict.find(key) == dict.end())
      return false; 
    return true; 
  }
}

bool SynchroMap::hasValue(const string & key) {
  if(!isPresent(key)) return false;
  auto pair = dict[key];
  return (pair.second.wait_for(std::chrono::seconds(0)) == std::future_status::ready);
}
