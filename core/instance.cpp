#include <fstream>
#include <iostream>
#include <algorithm> // for max() and find()
#include <limits>
#include <cmath>
using namespace std;

#include <mutex>

#include "instance.h"
#include "util.h"

void Instance::doConvertIndices() {
    int i, j;
    for(i = 0; i < nbTasks; i++)
        for(j = 0; j < (int) dependencies[i].size(); j++)
            dependencies[i][j] -= 1;
}

Instance::Instance() {
}

Instance::Instance(const string input_file, int convertIndices) {
    //    cout << "Opening " << input_file << "..." << endl;

    sortedByLength = nullptr;

    this->inputFile = input_file;
    ifstream input(input_file);
    int i;

    if(!input) {
        cerr << "Could not open " << input_file << "!" << endl;
        exit(1);
    }
    int nbW = readArray(input, nbWorkers);
    if(nbW < 0) {
        cerr << "Error reading workers from " << input_file << endl;
        exit(1);
    }

    nbWorkerTypes = static_cast<unsigned int>(nbW);
    int s = getSum(nbWorkers);
    if(s < 0) {
        cerr << "Wrong number of workers in " << input_file << endl;
        exit(1);
    }
    totalWorkers = static_cast<unsigned int>(s);
    int v = 0;
    for(int k = 0; k < nbWorkerTypes; k++) {
        int n = nbWorkers[k]; 
        vector<int> ids;
	vector<int> nodes(n, k); // Size n, all equal to k
        for(int i = 0; i < n; i++) {
            ids.push_back(v + i);
	}
        v += n;
        workerIDs.push_back(ids);
	    memoryNodes.push_back(nodes);
    }
    execTimes.resize(nbWorkerTypes);
    nbMemoryNodes = nbWorkerTypes;

    memoryNodeBW.resize(nbMemoryNodes);
    memoryNodeLat.resize(nbMemoryNodes);
    for(int i = 0; i < nbMemoryNodes; ++i) {
      memoryNodeLat[i].resize(nbMemoryNodes, 0);
      memoryNodeBW[i].resize(nbMemoryNodes, 1);
    }
    
    for(i = 0; i < nbWorkerTypes; i++) {
        readArray(input, execTimes[i]);
        for(auto &t: execTimes[i])
            if(t < 0)
                t = numeric_limits<double>::infinity();
    }
    nbTaskTypes = static_cast<unsigned int>(execTimes[0].size());

    readArray(input, taskTypes);
    nbTasks = static_cast<unsigned int>(taskTypes.size());

    isIndependent = true;
    dependencies.resize(nbTasks);
    for(i = 0; i < nbTasks; i++) {
        readArray(input, dependencies[i]);
        if(! dependencies[i].empty())
            isIndependent = false;
    }

    itemsRequired.resize(nbTasks);
    itemsProduced.resize(nbTasks);

    int nbTaskNames = readArray(input, taskIDs);
    if(nbTaskNames != -1 && nbTaskNames != 0 && nbTaskNames != nbTasks) {
        cerr << "Task Names do not have the correct length: " << nbTaskNames << " and not " << nbTasks << endl;
        throw(1);
    }

    input.close();

    if(convertIndices == 1)
        doConvertIndices();
}


void Instance::removeDependencies() {
    for(auto && v: dependencies) {
        v.clear();
    }
    ancestors.clear();
    revDep.clear();
    topOrder.clear();
    isIndependent = true;
}


void Instance::revertDependencies() {
    computeRevDependencies();
    dependencies = revDep;
    ancestors.clear();
    revDep.clear();
    topOrder.clear();
}

string Instance::formatTask(int task) {
    string res = "#" + to_string(task);
    if(taskIDs.size() > 0)
        res += " " + taskIDs[task];
    return res;
}

void Instance::display(int verbosity) {
    int i;
    if(verbosity >= 1) {
        int totalWorkers = 0;
        for(auto it = nbWorkers.begin(); it != nbWorkers.end(); it++)
            totalWorkers += *it;
        cout << "WPT: " << nbWorkers << "; NBW: " << totalWorkers << "; NBT: " << taskTypes.size() << endl;
        if(workerNames.size() > 0) cout << "wNames \t";
        cout << taskTypeNames << endl;
        for(i = 0; i < (int) nbWorkers.size(); i++){
            if(workerNames.size() > 0) cout << workerNames[i] << " \t";
            cout << execTimes[i] << " " << workerIDs[i] << " " << memoryNodes[i] << endl;
        }
        if(verbosity >= 4) {
            cout << taskTypes << endl;
            cout << taskIDs << endl;
        }
        if(verbosity >= 5) {
            for(i = 0; i < (int) taskTypes.size(); i++)
                cout << i << " " << dependencies[i] << endl;
        }
    }
}


int Instance::getType(int m, int* index) {
    int i;
    for(i = 0; i < (int) nbWorkerTypes; i++) {
        if(m < nbWorkers[i]){
            if (index != NULL)
                *index = m;
            return i;
        } else
            m -= nbWorkers[i];
    }
    // That's an error if I get here: m was higher than total nb of workers.
    throw(-1);
}


void Instance::autoMerge(double tolerance /*default = 0.01*/ ) {

    bool hasChanged = true;

    while(hasChanged) {
        hasChanged = false;
        for(int i = 0; i < nbWorkerTypes; i++) {
            vector<int> toMerge;
            toMerge.push_back(i);
            for(int j = i+1; j < nbWorkerTypes; j++) {
                bool similar = true;
                for(int k = 0; k < nbTaskTypes; k++)
                    if (!isValidValue(execTimes[i][k])) {
                        if (!isValidValue(execTimes[j][k]))
                            continue; // If both values are infinite the workers are still similar
                        else {
                            similar = false; break;
                        }
                    } else {
                        if (!isValidValue(execTimes[j][k])) {
                            similar = false; break;
                        }
                        else if(abs(execTimes[i][k] - execTimes[j][k]) > tolerance * execTimes[i][k]){
                            similar = false; break;
                        }
                    }
                if(similar) toMerge.push_back(j);
            }
            if(toMerge.size() > 1) {
	      // cerr << "AutoMerging: " << toMerge << endl;
                mergeWorkerTypes(toMerge);
                hasChanged = true;
                break;
            }
        }
    }
}

void Instance::mergeWorkerTypes(const vector<int> indicesToMerge) {
    int oldNbWT = nbWorkers.size();
    int nbTaskTypes = execTimes[0].size();

    vector<int> newNbWorkers;
    vector< vector<double> > newExecTimes;
    vector<vector<int> > newWorkerIDs;
    vector<vector<int> > newMemoryNodes;
    
    int replacementNB = 0;
    vector<double> replacementExecTimes(nbTaskTypes, 0);

    // Computing the average execution time
    for(int i = 0; i < (int) indicesToMerge.size(); i++) {
        replacementNB += nbWorkers[indicesToMerge[i]];
        for(int j = 0; j < nbTaskTypes; j++)
            replacementExecTimes[j] = replacementExecTimes[j] + nbWorkers[indicesToMerge[i]] * execTimes[indicesToMerge[i]][j];
    }
    for(int j = 0; j < nbTaskTypes; j++)
        replacementExecTimes[j] /= replacementNB;

    // cerr << "Replacement : " << replacementNB << " " << replacementExecTimes << endl;

    for(int i = 0; i < oldNbWT; i++) {
        int j;
        for(j = 0; j < (int) indicesToMerge.size(); j++)
            if(i == indicesToMerge[j]) break;
        if(j == (int) indicesToMerge.size()) {
            newNbWorkers.push_back(nbWorkers[i]);
            newExecTimes.push_back(execTimes[i]);
            newWorkerIDs.push_back(workerIDs[i]);
	    newMemoryNodes.push_back(memoryNodes[i]); 
        }
    }

    vector<int> replacementIDs;
    vector<int> replacementMemNodes; 
    for(int i = 0; i < (int) indicesToMerge.size(); i++) {
        replacementIDs.insert(replacementIDs.end(), workerIDs[i].begin(), workerIDs[i].end());
	replacementMemNodes.insert(replacementMemNodes.end(), memoryNodes[i].begin(), memoryNodes[i].end()); 
    }
    newWorkerIDs.push_back(replacementIDs);
    newMemoryNodes.push_back(replacementMemNodes); 
    newNbWorkers.push_back(replacementNB);
    newExecTimes.push_back(replacementExecTimes);

    nbWorkers = newNbWorkers;
    execTimes = newExecTimes;
    workerIDs = newWorkerIDs;
    memoryNodes = newMemoryNodes; 
    nbWorkerTypes = nbWorkers.size();


    if(workerNames.size() > 0) {
        string replacementName = "";
        vector<string> newWorkerNames;

        for(int i = 0; i < (int) indicesToMerge.size(); i++) {
            if(i != 0) replacementName += ",";
            replacementName += workerNames[indicesToMerge[i]];
        }

        for(int i = 0; i < oldNbWT; i++) {
            int j;
            for(j = 0; j < (int) indicesToMerge.size(); j++)
                if(i == indicesToMerge[j]) break;
            if(j == (int) indicesToMerge.size()) {
                newWorkerNames.push_back(workerNames[i]);
            }
        }
        newWorkerNames.push_back(replacementName);

        workerNames = newWorkerNames;
    }

    if(extraData.isPresent("rl[worker]")) {
      // cerr << "Converting worker IDs" << endl;
        vector<int>* allocP = (vector<int>*) extraData.get("rl[worker]");
        vector<int>* allocWorkers = new vector<int>();
        for(unsigned int i = 0; i < nbTasks; i++) {
            allocWorkers->push_back(getWorkerWithWorkerID((*allocP)[i]));
        }
        extraData.replace("rl[worker]", allocWorkers);
    }

}

int Instance::getWorkerWithWorkerID(int workerid) {
    int result = 0;
    for(unsigned int w = 0; w < workerIDs.size(); result += workerIDs[w].size(), w++) {
        auto found = find(workerIDs[w].begin(), workerIDs[w].end(), workerid);
        if (found == workerIDs[w].end()) continue;
        return (result + (found - workerIDs[w].begin()));
    }
    return -1;
}

/* Accessors for static analysis */
vector<int> Instance::getTopOrder() {
    lock_guard<mutex> lock(mtx);
    computeTopOrder();
    return topOrder;
}

vector< vector<int> > Instance::getAncestors() {
    lock_guard<mutex> lock(mtx);
    computeAncestors();
    return ancestors;
}

vector< vector<int> >  Instance::getRevDependencies() {
    lock_guard<mutex> lock(mtx);
    computeRevDependencies();
    return revDep;
}

vector<double> Instance::computeRanks(vector<double> wbar, int to, int from) {
    {
        lock_guard<mutex> lock(mtx);
        computeRevDependencies();
        computeTopOrder();
    }
    vector<double> rank(nbTasks, -std::numeric_limits<double>::infinity());
    if(to == -1) to = topOrder[nbTasks -1];
    if(from == -1) from = topOrder[0];
    rank[to] = wbar[to];
    int j;
    for(j = nbTasks - 1; (j >= 0) && (topOrder[j] != to); j--);
    for(j--; j >= 0; j--) {
        int i = topOrder[j];
        // Compute max
        double m = -std::numeric_limits<double>::infinity();
	if(revDep[i].empty())
	  m = 0;
	else 
	  for(int k = 0; k < (int) revDep[i].size(); k++)
            if(rank[revDep[i][k]] > m)
	      m = rank[revDep[i][k]];
        rank[i] = wbar[i] + m;

        if (topOrder[j] == from)
            break;
    }

    return rank;
}



vector<double> Instance::computeHEFTRank() {
    vector<double> wbar(nbTasks, 0);

    int i, j;
    int z = totalWorkers;
    for(i = 0; i < nbTasks; i++) {
        for(j = 0; j < nbWorkerTypes; j++)
            wbar[i] += execTimes[j][taskTypes[i]] * nbWorkers[j];
        wbar[i] /= z;
    }

    vector<double> rank = computeRanks(wbar);
    wbar.clear();
    return rank;
}
vector<double> Instance::computeMinRank() {
    vector<double> wbar(nbTasks, 0);

    int i, j;
    for(i = 0; i < nbTasks; i++) {
        wbar[i] = execTimes[0][taskTypes[i]];
        for(j = 1; j < nbWorkerTypes; j++)
            wbar[i] = min(wbar[i],execTimes[j][taskTypes[i]]);
    }

    vector<double> rank = computeRanks(wbar);
    wbar.clear();
    return rank;
}

std::vector<double> Instance::computeUnitRank() {
    vector<double> wbar(nbTasks, 1);

    vector<double> rank = computeRanks(wbar);
    wbar.clear();
    return rank;
}


vector<double> Instance::computePreAllocRank(vector<int> alloc) {
    vector<double> wbar(nbTasks, 0);

    for(int i = 0; i < nbTasks; i++) {
        if(!isValidType(alloc[i], i)){
            cerr << "Instance PreAllocRank: warning, alloc " << alloc[i] << " is not valid for task " << i << endl;
        }
        wbar[i] = execTimes[alloc[i]][taskTypes[i]];
    }

    vector<double> rank = computeRanks(wbar);
    wbar.clear();
    return rank;
}


/* Computations for static analysis */

vector<double> Instance::computeLongestPath(const vector<double> &weights, bool display, vector<int>* storeCP, int to, int from) {
    int i, j;
    vector<double> rank = computeRanks(weights, to, from);
    if((storeCP != NULL || display)) {
        // Display the Critical Path
        double cp = getMax(rank);
        if(display) cout << "CP = " << cp << ": ";
        if((from == -1 ) || rank[from] < 0) {
            for(i = 0; i < nbTasks; i++) {
                if(rank[topOrder[i]] == cp) break;
            }
            i = topOrder[i];
        } else {
            i = from;
        }
        if(display) cout << i << "_" << taskTypes[i] << " ";
        if(storeCP != NULL) storeCP->push_back(i);
        while(revDep[i].size() > 0) {
            for(j = 0; j < (int) revDep[i].size(); j++)
                if(rank[revDep[i][j]] + weights[i] == rank[i])
                    break;
            i = revDep[i][j];
            if(display) cout << i << "_" << taskTypes[i] << " ";
            if(storeCP != NULL) storeCP->push_back(i);
            if(i == to) break;
        }
        if(display) cout << endl;
    }
    return rank;
}


vector<double> Instance::computeCriticalPath(bool display, vector<int>* storeCP) {

    vector<double> wmin(nbTasks, 0);

    int i, j;
    for(i = 0; i < nbTasks; i++) {
        wmin[i] = execTimes[0][taskTypes[i]];
        for(j = 1; j < (int) execTimes.size(); j++)
            wmin[i] = min(wmin[i], execTimes[j][taskTypes[i]]);
    }


    vector<double> result = computeLongestPath(wmin, display, storeCP);
    wmin.clear();
    return result;
}


void Instance::updateTopOrder(vector<int> &computed, int i, int &l) {
    int j;
    computed[i] = 2;
    for(j = 0; j < (int) dependencies[i].size(); j++) {
        int prev = dependencies[i][j];
        if(computed[prev] == 2) {
            cerr << "Cyclic dependency detected on task #" << i;
            if(taskIDs.size() > 0)
                cerr << ": " << taskIDs[i];
            cerr << " which depends on #" << prev;
            if(taskIDs.size() > 0)
                cerr << ": " << taskIDs[prev];
            cerr << endl;
            throw(1);
        }
        if(computed[prev] == 0) {
            updateTopOrder(computed, prev, l);
        }
    }
    computed[i] = 1;
    topOrder[l] = i;
    l++;
}

void Instance::computeTopOrder() {
    if(topOrder.size() != 0)
        return;

    vector<int> computed(nbTasks, 0);
    topOrder.resize(nbTasks);
    int i, l = 0;

    for(i = 0; i < nbTasks; i++) {
        if(computed[i] == 0) {
            updateTopOrder(computed, i, l /* passed by ref */);
        }
    }
    computed.clear();
}

// ancestors[i][j] == 1 iff there exists a path from j to i
void Instance::computeAncestors() {

    if(ancestors.size() != 0)
        return;

    computeTopOrder();

    ancestors.resize(nbTasks);
    for(int i = 0; i < nbTasks; i++) {
        ancestors[i].resize(nbTasks);
        for(int j = 0; j < nbTasks; j++)
            ancestors[i][j] = (i==j) ? 1 : 0;
    }

    for(int i = 0; i < nbTasks; i++) {
        int j = topOrder[i];
        for(int l = 0; l < (int) dependencies[j].size(); l++) {
            // Add all ancestors of precedence l
            for(int k = 0; k < nbTasks; k++) {
                if(ancestors[dependencies[j][l]][k] == 1)
                    ancestors[j][k] = 1;
            }
        }
    }
}


void  Instance::computeRevDependencies() {
    if(revDep.size() != 0)
        return;

    revDep.resize(nbTasks);
    int i, j;

    for(i = 0; i < nbTasks; i++)
        for(j = 0; j < (int) dependencies[i].size(); j++)
            revDep[dependencies[i][j]].push_back(i);
}

std::vector<std::vector<int> > *Instance::getSortedByLength() {
    lock_guard<mutex> lock(mtx);

    if(sortedByLength == nullptr) {
        sortedByLength = new vector<vector<int> >(nbWorkerTypes);
        auto & result = *sortedByLength;
        for (unsigned int i = 0; i < nbWorkerTypes; i++) {
            for (int j = 0; j < nbTaskTypes; j++)
                result[i].push_back(j);
            sort(result[i].begin(), result[i].end(),
                 [&](int a, int b) {
                     return execTimes[i][a] > execTimes[i][b];
                 });
        }
    }
    return sortedByLength;
}


#define TOTALOP(n, i) ((3*n*n + 6*n -3*n*(i) -3*(i) + (i)*(i) +2)*(i)/6)

int solve_equation(int n, int y)
{
    int i;
    for(i=1; i<=n; i++)
    {
        int temp = TOTALOP(n,i);

        if(temp==y)
            return i;
        else if(temp >y)
            return i-1;
    }
    cout << "Bug ! " << n << " " << y << endl;
    return -1;
}

CholeskyInstance::CholeskyInstance(int n, int nbCPU, int nbGPU, double execTimesCPU[], double execTimesGPU[]) {
    int i,j,k;
    nbTasks = TOTALOP(n, n);

    nbWorkers.push_back(nbCPU);
    nbWorkers.push_back(nbGPU);

    execTimes.resize(2);
    execTimes[0].resize(4);
    execTimes[1].resize(4);
    for(j = 0; j < 4; j++) {
        execTimes[0][j] = execTimesCPU[j];
        execTimes[1][j] = execTimesGPU[j];
    }

    taskTypes.resize(nbTasks);

    int taskno = 0;
    for(i=0; i<n; i++)
    {
        taskTypes[taskno++] = 3;
        for(j=i+1; j<n; j++)
            taskTypes[taskno++] = 2;

        for(j=i+1; j<n; j++)
        {
            taskTypes[taskno++] = 1;

            for(k=i+1; k<j; k++)
                taskTypes[taskno++] = 0;
        }
    }

    dependencies.resize(nbTasks);

    for(i=0; i<nbTasks; i++)
    {
        //assume task number starts from 1
        int presenttaskid = i+1;
        //check for each operations of tasksequence
        if(taskTypes[i] == 3)
        {
            //potrf is dependent on last syrk operation performed in the same tile
            int level = solve_equation(n, presenttaskid -1);
            if(level > 0)
            {
                int tasktillprevlevel = TOTALOP(n, level-1);
                int prevtrsmid = tasktillprevlevel + 1 + (n-level) + 1;
                dependencies[i].push_back(prevtrsmid-1);
            }
        }
        else if(taskTypes[i] == 2)
        {
            //trsm is dependent on prev gemm operation and potrf

            //find the taskid of potrf operation
            int level = solve_equation(n, presenttaskid);
            int tasktillprevlevel = TOTALOP(n, level);
            int potrfid = tasktillprevlevel + 1;
            dependencies[i].push_back(potrfid-1);

            int trsmnumber = presenttaskid - potrfid;
            if(level > 0)
            {
                int tasktillprevprevlevel = TOTALOP(n,level-1);
                int prevgemmid = tasktillprevprevlevel + 1 /* potrf operation*/
                                 + (n -level) /* trsm operations*/
                                 + trsmnumber * (trsmnumber +1)/2 /* gemm & syrk operations */
                                 + 1 /* syrk in the same row*/
                                 + 1 /* first gemm in the same row*/;
                dependencies[i].push_back(prevgemmid-1);
            }
        }
        else if(taskTypes[i] == 1)
        {
            //Syrk operation is dependent on the trsm operation and syrk operation performed
            int level = solve_equation(n, presenttaskid-1);
            //-1 is required so that it returns the previous completed level
            int tasktillprevlevel = TOTALOP(n, level);
            int temp = tasktillprevlevel + 1 /*potrf */
                       + (n-(level+1)) /*trsm*/;
            temp = presenttaskid - temp -1 ; // not considering the id of current syrk
            int levelofprevsyrk = (-1 + sqrt(1 + 8*temp))/2;
            int syrknumber = levelofprevsyrk + 1;
            int trsmtaskid = tasktillprevlevel + 1 + syrknumber;
            dependencies[i].push_back(trsmtaskid-1);

            if(level>0)
            {
                int tasktillprevprevlevel = TOTALOP(n,level - 1);
                int prevsyrkid = tasktillprevprevlevel + 1 /*potrf */
                                 + (n-level) /*trsm operations*/
                                 + (syrknumber-1+1)*(syrknumber-1+2)/2 + 1 /* syrk on present tile*/;
                dependencies[i].push_back(prevsyrkid-1);
            }
        }
        else if(taskTypes[i] == 0)
        {
            int level = solve_equation(n, presenttaskid -1);
            int tasktillprevlevel = TOTALOP(n, level);
            int temp = tasktillprevlevel + 1 + (n-(level+1));
            temp = presenttaskid - temp -1;
            //-1 is required so that solution reached to the previous row of gemm

            int levelofprevtrsm = (-1 + sqrt(1+8*temp))/2;
            int firsttrsmnumber = presenttaskid - (tasktillprevlevel + 1  + (n-(level+1)) + (levelofprevtrsm * (levelofprevtrsm + 1)/2) +1 /* syrk operation*/);
            int firsttrsmid = tasktillprevlevel + 1 + firsttrsmnumber;
            int secondtrsmid = tasktillprevlevel + 1 + (levelofprevtrsm + 1);

            dependencies[i].push_back(firsttrsmid-1);
            dependencies[i].push_back(secondtrsmid-1);
            if(level > 0)
            {
                //Find the taskid of the gemm operation performed on the same tile
                int tasktillprevprevlevel = TOTALOP(n, level -1);
                int gemmid = tasktillprevprevlevel + 1 /* potrf operation */
                             + (n - level) /*trsm operations */
                             +((levelofprevtrsm +1)*(levelofprevtrsm + 2)/2) + 1 /*syrk operation */
                             + (firsttrsmnumber +1);
                dependencies[i].push_back(gemmid-1);
            }

        }
    }
}
