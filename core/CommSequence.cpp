#include "CommSequence.h"
#include "util.h"
#include <iostream>

using namespace std;


CommSequence::CommSequence(Instance &ins): ins(&ins), nbWT(ins.totalWorkers), 
					   outgoing(nbWT), incoming(nbWT) {
}

double reserveALAP(vector<AvailSequence*> &seqs, double from, double len, double goal = -1) {
  vector<timeSeq::iterator> its; 
  vector<double> startTimes(seqs.size());
  for(unsigned int i = 0; i < seqs.size(); i++) {
    its.push_back(seqs[i]->getAvail(from, len, startTimes[i])); 
  }
  double finish = getMax(startTimes) + len; 
  finish = max(finish, goal); 

  //  cout << "reserveALAP: " << from << " len=" << len << " goal=" << goal << " finish=" << finish << endl; 

  for(unsigned int i = 0; i < seqs.size(); i++) {
    AvailSequence* seq = seqs[i]; 
    its[i] = seq->getAvailLatest(from, len, finish, startTimes[i], its[i]); 
    seq->insertBusy(its[i], startTimes[i], len); 
  }
  return finish; 
}

double CommSequence::newTransfer(double now, int source, int dest, int size, double goal) {
  vector<AvailSequence*> seqs; 
  seqs.push_back(&outgoing[source]); 
  seqs.push_back(&incoming[dest]); 
  double len = ins->memoryNodeLat[source][dest] + size / ins->memoryNodeBW[source][dest];
  return (reserveALAP(seqs, now, len, goal)); 
}
