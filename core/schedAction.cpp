#include "schedAction.h"
#include "util.h"
#include <iostream>

using namespace std;

/* Action Sequence: list of actions, performed in sequence */

void ActionSequence::add(SchedAction* a) {
  actions.push_back(a);
}

void ActionSequence::remove(SchedAction* a) {
  auto it = actions.begin(); 
  while(it != actions.end()) {
    if (a == *it) break; 
    ++it; 
  }
  if(it != actions.end())
    actions.erase(it);
  else {
    cerr << "Warning: unable to remove action " << a << " from list of actions" << endl; 
  }
}

void ActionSequence::onSchedule(int i, int w, double s, double f) {
  for (std::vector<SchedAction*>::iterator it = actions.begin() ; it != actions.end(); ++it) {
    (*it)->onSchedule(i, w, s, f);
  }
}
void ActionSequence::onTaskPush(int t) {
  for (std::vector<SchedAction*>::iterator it = actions.begin() ; it != actions.end(); ++it)
    (*it)->onTaskPush(t);
}

/* ExportSchedule: exports a schedule */

ExportSchedule::ExportSchedule(ostream *stream, Instance* ins, bool header, string name) : output(stream), instance(ins), name(name) { 
  if(header) outputHeader(); 
}
ExportSchedule::ExportSchedule(Instance* ins, string name) 
  : instance(ins), name(name) {
}

void ExportSchedule::outputHeader() {
  if(name != "")
    *output << "sched ";
  *output << "Tid   worker taskType ";
  if(instance->taskIDs.size() > 0)
    *output << (submitOrder ? "SubmitOrder " : "JobId ");
  *output << "start duration end" << endl;
}

void ExportSchedule::changeName(string newName) {
  if((name == "" && newName != "")) {
    cerr << "ExportSchedule: Warning, adding a name after the start breaks the header" << endl;
  }
  if(name != "" && newName == "") newName = "NA";
  name = newName;
}

void ExportSchedule::onSchedule(int i, int w, double s, double f) {
  if(name != "")
    *output << name << " ";
  *output << i << " " << w << " ";
  if(instance->taskTypeNames.size() > 0)
    *output << instance->taskTypeNames[instance->taskTypes[i]];
  else
    *output << instance->taskTypes[i];
  if(instance->taskIDs.size() > 0)
    *output << " " << instance->taskIDs[i];
  *output << " " << s << " " << (f - s) << " " << f << endl;
}


/* ExportToFile */

ExportToFile::ExportToFile(string filename, Instance* ins, bool header, string name, bool submitorder): ExportSchedule(ins, name), f(new ofstream(filename, ios::app)) {
  ExportSchedule::submitOrder = submitorder;
  output = f;
  if(header) outputHeader(); 
}

ExportToFile::~ExportToFile() {
  f->close();
}

/* ExportToString */

ExportToString::ExportToString(Instance* ins, bool header, string name)
  : ExportSchedule(ins, name), f(new ostringstream()) {
  output = f;
  if(header) outputHeader(); 
}

string ExportToString::getResult() {
  return f->str(); 
}

/* ExportAlloc: exports in .rec format */

ExportAlloc::ExportAlloc(string filename, Instance* ins, bool submitOrder, bool outputType, bool workerOrder)
  : output(filename), instance(ins), submitOrder(submitOrder),  outputType(outputType), workerOrder(workerOrder) {
  if (workerOrder) {
    if (!outputType) {
      workerTaskCount.resize(instance->totalWorkers, 0); 
    } else {
      cerr << "Export: workerOrder is not compatible with outputType, ignoring." << endl;
      workerOrder = false; 
    }
  }
}

void ExportAlloc::onSchedule(int i, int w, double s, double f) {
  if(instance->taskIDs.size() > 0) {
    string id = instance->taskIDs[i];
    vector<string> parts;
    split(id, ':', parts);
    if(parts.size() == 1)
      output << (submitOrder ? "SubmitOrder: " : "JobId: ") << instance->taskIDs[i] << endl;
    else {
      output << (submitOrder ? "SubmitOrder: " : "JobId: ") << parts[0] << endl;
      output << "Tag:" << parts[1] << endl;
    }
  }
  output << "TaskId: " << i << endl;
  if(instance->taskTypeNames.size() > 0) {
    output << "TaskType: " << instance->taskTypeNames[instance->taskTypes[i]] << endl; 
  }

  int index; 
  int type = instance->getType(w, &index);

  if (outputType && instance->workerIDs[type].size() > 1) {
    output << "Workers:";
    for(auto& i: instance->workerIDs[type])
      output << " " << i;
    output << endl;
  } else {
    output << "SpecificWorker: " << instance->workerIDs[type][index] << endl;
    if (workerOrder) {
      output << "Workerorder: " << workerTaskCount[w]+1 << endl;
      ++workerTaskCount[w]; 
    }
  }

  
  if(instance->workerNames.size() > 0) {
    output << "Architecture: "  << instance->workerNames[type] << endl;
  } else {
    output << "WorkerType: " << type << endl;
  }
  output << endl; 
}

ExportAlloc::~ExportAlloc() {
  output.close();
}

/* UtilAnalysis: compute stats about a schedule */


UtilAnalysis::UtilAnalysis(Instance* _ins, string saveFile) :
  ins(_ins), repartition(_ins->nbWorkerTypes, 
			 std::vector<int>(ins->nbTaskTypes, 0)), 
  output(saveFile, ios::app) {
  
  }
UtilAnalysis::~UtilAnalysis(){
  output.close(); 
}

void UtilAnalysis::onSchedule(int i, int w, double s, double f) {
  repartition[ins->getType(w)][ins->taskTypes[i]] += 1; 
}

std::vector<std::vector<int> > UtilAnalysis::getRepartition() {
  return repartition; 
}

void UtilAnalysis::reset() {
  for(auto &v: repartition)
    for(auto &n: v)
      n = 0; 
}

void UtilAnalysis::write(string prefix) {
  for(int i = 0; i < (int) repartition.size(); i++) {
    for(int j = 0; j < (int) repartition[i].size(); j++) {
      output << prefix << " " << i << " " << j << " " 
	     << repartition[i][j] << " " 
	     << repartition[i][j] * ins->execTimes[i][j] << endl;
    }
  }
  reset(); 
}

/* InternalShare: sharing schedules internally between algorithms */

InternalShare::InternalShare(string name, Instance* ins) : 
  shareName(name), instance(ins), allocation(ins->nbTasks, -1),
  worker(ins->nbTasks, -1), 
  startTimes(ins->nbTasks, -1),
  endTimes(ins->nbTasks, -1) {
}

void InternalShare::onSchedule(int i, int w, double s, double f) {
  allocation[i] = instance->getType(w);
  worker[i] = w; 
  startTimes[i] = s;
  endTimes[i] = f;
}

void InternalShare::finish() {
  instance->extraData.insert(shareName + "[worker]", new vector<int>(worker)); 
  instance->extraData.insert(shareName + "[alloc]", new vector<int>(allocation)); 
  instance->extraData.insert(shareName + "[start]", new vector<double>(startTimes)); 
  instance->extraData.insert(shareName + "[end]", new vector<double>(endTimes)); 
}

/* ExportBubble: deprecated */

ExportBubble::ExportBubble(string filename, Instance* ins, int btt) : 
  InternalShare(filename, ins), output(filename), bubbleTaskType(btt) {
}

void ExportBubble::finish() {
  vector<double> bubbleStarts; 
  vector<int> topOrder = instance->getTopOrder(); 
  int nbBubbles; 
  for(int i = 0; i < instance->nbTasks; i++) {
    if(instance->taskTypes[topOrder[i]] == bubbleTaskType) 
      bubbleStarts.push_back(startTimes[topOrder[i]]);  
  }
  nbBubbles = bubbleStarts.size(); 
  for(int i = 0; i < instance->nbTasks; i++) {
    double s = startTimes[i], e = startTimes[i] + instance->execType(allocation[i], i);
    int firstBubble = -1, lastBubble = -1; 
    for(int j = 0; j < nbBubbles; j++) {
      if(s >= bubbleStarts[j]) firstBubble = j; 
      if(e >= bubbleStarts[j]) lastBubble = j; 
      else break; 
    }
    output << firstBubble << " " << lastBubble << " " << allocation[i] << endl; 
  }
  output.close(); 
}

