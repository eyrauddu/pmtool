extern "C" {
#include <rec.h> 
}
#include <stdio.h>
#include <cstring>
#include <string>
#include <unordered_map>
#include <iostream>
#include <limits>

#include "util.h"
#include "instance.h"

using namespace std;
#define C_TEXT( text ) ((char*)std::string( text ).c_str())


static string recordValueOpt(const rec_record_t record, const char* field) {
  int nb = rec_record_get_num_fields_by_name(record, field); 
  if(nb == 0) 
    return ""; 
  else {
    string res(rec_field_value(rec_record_get_field_by_name(record, field, 0)));
    return res; 
  }
}

static string recordValue(const rec_record_t record, const char* field) {
  int nb = rec_record_get_num_fields_by_name(record, field); 
  if(nb == 0) {
    cerr << "RecFileReader: mandatory field " << field << " missing at line " <<  rec_record_location_str(record) << endl; 
    throw(1); 
  } else {
    string res(rec_field_value(rec_record_get_field_by_name(record, field, 0)));
    return res;
  }
}

static string recordValueAlternativeOpt(const rec_record_t record, const char* firstChoice, const char* secondChoice) {
  string value = recordValueOpt(record, firstChoice);
  if (value == "") {
    value = recordValueOpt(record, secondChoice);
  }
  return value; 
}

static string recordValueAlternative(const rec_record_t record, const char* firstChoice, const char* secondChoice) {
  string value = recordValueAlternativeOpt(record, firstChoice, secondChoice);
  if (value == "") {
    cerr << "RecFileReader: one of both fields " << firstChoice << " or " << secondChoice << " missing at line " <<  rec_record_location_str(record) << endl; 
    throw(1); 
  }
  return value; 
}

template <class T, typename Func = identity>
static void getVector(const rec_record_t &record, const char* &field, 
		      vector<T> &result, Func &&func = Func()) {
    
    string line = recordValueOpt(record, field); 
    if(line != "") {
      vector<string> words = split(line, ' '); 
      for(auto& w: words) result.push_back(func(w)); 
    }
}


class recTask {
public: 
  static const char* recModel; 
  static const char* recName; 
  static const char* recFootprint; 
  static const char* recJobId; 
  static const char* recSubmitOrder; 
  static const char* recEstimatedTime; 
  static const char* recDependsOn; 
  static const char* recTag;
  static const char* recWorkerId;
  static const char* recStartTime;
  static const char* recEndTime;
  static const char* recHandles;
  static const char* recModes;
  static const char* recSizes;

  static int outputCount;
  static void recReaderInit() {
    if(recModel == NULL) {
      recModel = rec_parse_field_name_str(C_TEXT("Model")); 
      recName = rec_parse_field_name_str(C_TEXT("Name")); 
      recFootprint = rec_parse_field_name_str(C_TEXT("Footprint")); 
      recJobId = rec_parse_field_name_str(C_TEXT("JobId")); 
      recSubmitOrder = rec_parse_field_name_str(C_TEXT("SubmitOrder")); 
      recEstimatedTime = rec_parse_field_name_str(C_TEXT("EstimatedTime")); 
      recDependsOn = rec_parse_field_name_str(C_TEXT("DependsOn")); 
      recTag = rec_parse_field_name_str(C_TEXT("Tag"));
      recHandles = rec_parse_field_name_str(C_TEXT("Handles"));
      recModes = rec_parse_field_name_str(C_TEXT("Modes"));
      recSizes = rec_parse_field_name_str(C_TEXT("Sizes"));
      recWorkerId = rec_parse_field_name_str("WorkerId");
      recStartTime= rec_parse_field_name_str("StartTime");
      recEndTime = rec_parse_field_name_str("EndTime");
    }
    outputCount = 0; 
  }

  string model;
  string footprint; 
  string taskType; 
  string tag; 
  vector<int> dependsOn; 
  bool convertedDeps; 
  bool isReal; 
  int jobId;
  int submitOrder;
  int outputId;
  int internalId;

  int workerId;
  double startTime, endTime;


  // Communication informations
  typedef enum {ModeR, ModeRW, ModeW} modes_t;
  vector<string> handles; 
  vector<modes_t> modes; 
  vector<int> sizes;

private: 
  static modes_t strToMode(string m) {
    if(m == "R") return ModeR; 
    if(m=="RW") return ModeRW; 
    if(m=="W") return ModeW; 
    cerr << "Unknown mode " << m << ", sorry" << endl; 
    throw(1); 
  }

public: 

  static int stoi(string s) {
    return std::stoi(s);
  }

  recTask(const rec_record_t record, int id) : internalId(id) {
    model = recordValueOpt(record, recModel);
    convertedDeps = false; 
    if(model == "") {
      isReal = false; outputId = -1; footprint = ""; taskType = ""; 
    } else {
      footprint = recordValue(record, recFootprint); 
      taskType = model + ":" + footprint; 
      isReal = true; 
      outputId = outputCount; 
      ++outputCount; 
    }

    tag = recordValueOpt(record, recTag); 
    jobId = stoi(recordValue(record, recJobId)); 

    getVector(record, recDependsOn, dependsOn, stoi); 
    getVector(record, recHandles, handles); 
    getVector(record, recModes, modes, strToMode);
    getVector(record, recSizes, sizes, stoi); 

    if( isReal && ((handles.size() != modes.size()) || (modes.size() != sizes.size()))) {
      cerr << "RecReader: data information from jobId " << jobId << " is inconsistent: "
	   << handles.size() << " Handles, " << modes.size() << " Modes, " << sizes.size() << " Sizes" << endl;
      throw(1); 
    }
    
    string submitOrderStr = recordValueOpt(record, recSubmitOrder);
    if (submitOrderStr != "")
      submitOrder = stoi(submitOrderStr);

    string deps = recordValueOpt(record, recDependsOn);
    if(deps != "") {
      vector<string> splitDeps = split(deps, ' '); 
      for(auto& s: splitDeps) dependsOn.push_back(stoi(s)); 
    }

    string workerIdStr = recordValueOpt(record, recWorkerId);
    if (workerIdStr != "") {
      workerId = stoi(workerIdStr);
      startTime = 1000 * stof(recordValue(record, recStartTime));
      endTime = 1000 * stof(recordValue(record, recEndTime));
    } else {
      workerId = -1;
      startTime = endTime = -1;
    }
  }

  void getDependencies(const unordered_map<int, int>& jobIdToInternal, vector<recTask>& tasks, 
		       vector<int>& result) {
    if(convertedDeps)
      result.insert(result.end(), dependsOn.begin(), dependsOn.end()); 
    else {
      for(auto & v : dependsOn) {
	auto i = jobIdToInternal.find(v); 
	if(i != jobIdToInternal.end()) {
	  recTask & vt = tasks[i->second]; 
	  if(vt.isReal){ 
	    result.push_back(vt.outputId);
	  }
	  else {
	    vt.getDependencies(jobIdToInternal, tasks, result);
	  }
	} /*else {
	    cerr << "recReader: warning: dependencies absent from file: " << v << " in task " << jobId << endl; 
	    }*/
      }
      dependsOn = result; 
      convertedDeps = true; 
    }
  }
};

const char* recTask::recModel = NULL; 
const char* recTask::recName = NULL; 
const char* recTask::recFootprint = NULL;
const char* recTask::recJobId = NULL;
const char* recTask::recSubmitOrder = NULL;
const char* recTask::recEstimatedTime = NULL;
const char* recTask::recDependsOn = NULL; 
const char* recTask::recTag = NULL; 
const char* recTask::recHandles = NULL; 
const char* recTask::recModes = NULL; 
const char* recTask::recSizes = NULL; 
const char* recTask::recWorkerId = NULL;
const char* recTask::recStartTime = NULL;
const char* recTask::recEndTime = NULL;
int recTask::outputCount = 0;


void RecFileInstance::readFromFile(const string inputFile, unordered_map<string, vector<double> > timings,
                                   bool useSubmitOrder, bool appendTags) {

  this->inputFile = inputFile;
  
  recTask::recReaderInit();

  char* inputFileC = new char[inputFile.length() + 1];
  strcpy(inputFileC, inputFile.c_str());
  FILE* input = fopen(inputFileC, "r"); 
  rec_parser_t parser = rec_parser_new(input, inputFileC); 
  rec_record_t record; 
  vector<recTask> tasks; 
  unordered_map<int, int> jobIdToInternal; 
  vector<int> realTasks; // Stores internal IDs 
  int nbTask = 0; 

  // Data representing the schedule contained in the .rec file
  vector<int> schedWorkerIDs;
  vector<double> schedStartTimes;
  vector<double> schedEndTimes;
  bool schedInclude = true;

  nbWorkerTypes = workerNames.size();
  
  while(rec_parse_record(parser, &record)) {
    tasks.emplace_back(record, nbTask); 
    recTask& t = tasks[nbTask]; 
    ++nbTask; 
    jobIdToInternal[t.jobId] = t.internalId; 
    if(t.isReal) {
      realTasks.push_back(t.internalId); 
      if(timings.find(t.taskType) == timings.end()) {
	string estimatedTimesStr = recordValue(record, recTask::recEstimatedTime); 
	if(estimatedTimesStr == "") {
	  cerr << "readFromRecFile: error: no timing for task " << t.jobId << endl; 
	  throw(1); 
	}
	vector<string> taskTimeStr = split(estimatedTimesStr, ' ');
	if(nbWorkerTypes == 0) 
	  nbWorkerTypes = taskTimeStr.size(); 
	else 
	  if(nbWorkerTypes != (int) taskTimeStr.size()) {
	    cerr << "readFromRecFile: error: timing for task " << t.jobId << " has incorrect number of workers: " << taskTimeStr.size() << ", expected " << nbWorkerTypes << endl; 
	    throw(1); 
	  }
	vector<double> taskTime; 
	for(auto & s: taskTimeStr) taskTime.push_back(stod(s)); 
	timings[t.taskType] = taskTime; 
      }
        if(schedInclude && t.workerId >= 0 && t.startTime >= 0 && t.endTime >= 0) {
            schedWorkerIDs.push_back(t.workerId);
            schedStartTimes.push_back(t.startTime);
            schedEndTimes.push_back(t.endTime);
/*
            cout << "Sched data for task " << t.jobId << "(" << t.submitOrder << ") : " <<
                 t.workerId << " " << t.startTime << " " << t.endTime << endl;
*/
        } else {
            schedInclude = false;
        }

    }

  }

  fclose(input);
  
  if(nbWorkers.size() == 0) {
    nbWorkers.resize(nbWorkerTypes, 1); 
    totalWorkers = nbWorkerTypes; 
  }
  execTimes.resize(nbWorkerTypes); 
  for(auto& pairs : timings) {
    taskTypeNames.push_back(pairs.first); 
    for(int i = 0; i < nbWorkerTypes; i++) 
      execTimes[i].push_back(pairs.second[i]);
  }
  nbTaskTypes = taskTypeNames.size();
  
  for(auto& intID: realTasks) {
    recTask &t = tasks[intID]; 
    vector<int> deps; 
    t.getDependencies(jobIdToInternal, tasks, deps); 
    dependencies.push_back(deps);
    int type = 0; 
    for(; type < nbTaskTypes; type++) if(t.taskType == taskTypeNames[type]) break; 
    taskTypes.push_back(type); 
    string taskID(useSubmitOrder ? to_string(t.submitOrder) : to_string(t.jobId));
    if(appendTags && t.tag != ""){
      taskID += ":" + t.tag; 
    }
    taskIDs.push_back(taskID); 
  }
  nbTasks = taskTypes.size();
  
  // Cleanup taskTypeNames (name:footprint) if possible (remove
  // footprint if no other typeName has the same name)
  for(int i = 0; i < nbTaskTypes; i++) {
    string n = split(taskTypeNames[i], ':')[0]; 
    bool other=false; 
    for(int j = i+1; j < nbTaskTypes; j++) 
      if(split(taskTypeNames[j], ':')[0] == n) {
	other = true; break; 
      }
    if(!other)
      taskTypeNames[i] = n; 
  }

  map<string, pair<int, int> > itemVersionAndIndex; 

  // Update the information about data items
    computeTopOrder(); 
  
  itemsRequired.resize(nbTasks); 
  itemsProduced.resize(nbTasks); 
  for(int & tID: topOrder) {
    recTask & t = tasks[realTasks[tID]]; 
    vector<int>& req = itemsRequired[tID]; 
    vector<int>& prod = itemsProduced[tID]; 
    for(int hindex = 0, nbItems = t.handles.size(); hindex < nbItems; hindex++) {
      string h = t.handles[hindex]; 
      recTask::modes_t mode = t.modes[hindex]; 
      int version = 0; 
      int index = itemNames.size(); 
      if(itemVersionAndIndex.count(h) > 0) {
	auto data = itemVersionAndIndex[h]; 
	version = data.first; 
	index = data.second; 
      } else {
	itemNames.push_back(h + ":" + to_string(version)); 
	itemSizes.push_back(t.sizes[hindex]); 
	itemVersionAndIndex.emplace(h, make_pair(version, index)); 
      }
      if(mode == recTask::ModeR || mode == recTask::ModeRW) {
	req.push_back(index);
	if((t.sizes[hindex] != itemSizes[index]) && (mode == recTask::ModeR)) {
	  cerr << "RecReader: Warning: Data #" << hindex << " of task " << t.jobId << " (handle " << h << ")"
	       << " has size " << t.sizes[hindex] << " but last produced handle has size " << itemSizes[index] << endl;
	}
      }
      if(mode == recTask::ModeW || mode == recTask::ModeRW) {
	auto &data = itemVersionAndIndex[h]; 
	++data.first; 
	data.second = itemNames.size(); 
	itemNames.push_back(h + ":" + to_string(data.first)); 
	itemSizes.push_back(t.sizes[hindex]); 
	prod.push_back(data.second); 
      }

    }
  }
  
  
  int v = 0;
  for(int n: nbWorkers) {
    vector<int> ids;
    for(int i = 0; i < n; i++)
      ids.push_back(v + i);
    v += n;
    workerIDs.push_back(ids);
  }

  if(schedInclude){
      double execStart = numeric_limits<double>::infinity();
      for(auto& s: schedStartTimes)
          if(s < execStart)
              execStart = s;
      for(auto&& s: schedStartTimes) s -= execStart;
      for(auto&& s: schedEndTimes) s -= execStart;
    extraData.insert("rl[worker]", new vector<int>(schedWorkerIDs));
    extraData.insert("rl[start]", new vector<double>(schedStartTimes));
    extraData.insert("rl[end]", new vector<double>(schedEndTimes));
  }

}

RecFileInstance::RecFileInstance(const string inputFile, bool useSubmitOrder, bool appendTags) {
  unordered_map<string, vector<double> > timings; 

  readFromFile(inputFile, timings, useSubmitOrder, appendTags);
}

RecFileInstance::RecFileInstance(const string inputFile, const string platformFile,
				 bool useSubmitOrder, bool appendTags) {

  this->platformFile = platformFile;

  char* platformFileC = new char[platformFile.length() + 1];
  strcpy(platformFileC, platformFile.c_str());
  FILE* platformFILE = fopen(platformFileC, "r"); 
  rec_parser_t parser = rec_parser_new(platformFILE, platformFileC); 
  
  unordered_map<string, vector<double> > timings; 

  rec_db_t platform; 
  rec_parse_db(parser, &platform); 
  rec_rset_t workerCount = rec_db_get_rset_by_type(platform, "worker_count"); 
  if(!workerCount) {
    cerr << "RecFileInstance: no 'worker_count' record set\n"<< endl; 
    throw(1); 
  }
  rec_mset_t workerSet = rec_rset_mset(workerCount); 
  rec_mset_iterator_t workerSetIt = rec_mset_iterator(workerSet); 
  rec_record_t worker; 
  const char* arch = rec_parse_field_name_str("Architecture"); 
  const char* count = rec_parse_field_name_str("NbWorkers"); 
  while(rec_mset_iterator_next(&workerSetIt, MSET_RECORD, (const void**) &worker, NULL)) {
    string name(rec_field_value(rec_record_get_field_by_name(worker, arch, 0))); 
    int nb = atoi(rec_field_value(rec_record_get_field_by_name(worker, count, 0)));
    workerNames.push_back(name);
    nbWorkers.push_back(nb); 
  }
  nbWorkerTypes = nbWorkers.size();
  totalWorkers = getSum(nbWorkers);

  rec_rset_t memoryNodesSet = rec_db_get_rset_by_type(platform, "memory_workers"); 
  if(!memoryNodesSet) {
    /* By default, if no 'memory_workers' record is present, cpus have memory node 0
       and each other worker has its own memory node */
    int memNodes = 1;
    int workerIndex = 0; 
    for(int k = 0; k < nbWorkerTypes; ++k) {
      int memnode = 0;
      if(workerNames[k].compare(0, 3, "cpu") != 0)
	memnode = memNodes++; 
      vector<int> nodes(nbWorkers[k], memnode); 
      memoryNodes.push_back(nodes); 
    }
    nbMemoryNodes = memNodes;
  } else {
    /* Initialize all memory nodes with -1 */
    memoryNodes.resize(nbWorkerTypes);
    for(int k = 0; k < nbWorkerTypes; ++k) {
      memoryNodes[k].resize(nbWorkers[k], -1); 
    }
    nbMemoryNodes = 0; 
    /* Read from file */
    rec_mset_iterator_t memNodeIt = rec_mset_iterator(rec_rset_mset(memoryNodesSet)); 
    rec_record_t recMemNode; 
    const char* MemoryNode = rec_parse_field_name_str("MemoryNode");
    const char* Workers = rec_parse_field_name_str("Workers");
    while(rec_mset_iterator_next(&memNodeIt, MSET_RECORD, (const void**) &recMemNode, NULL)) {
      vector<int> workers_in_node;
      int memNodeId = stoi(recordValue(recMemNode, MemoryNode));
      nbMemoryNodes = max(nbMemoryNodes, memNodeId + 1); 
      getVector(recMemNode, Workers, workers_in_node, recTask::stoi);
      for(int w: workers_in_node) {
	int index;
	int type = getType(w, &index);
	if (memoryNodes[type][index] >= 0) {
	  cerr << "RecFileInstance: Worker " << w << " appears in two memory nodes: " <<
	    memoryNodes[type][index] << " and " << memNodeId << endl;
	  throw(1); 
	}
	memoryNodes[type][index] = memNodeId;
      }
    }
    /* Check that all workers have a memory node */
    int wID = 0; 
    for (int k = 0; k < nbWorkerTypes; ++k) {
      for (int j = 0; j < nbWorkers[k]; ++j, ++wID) {
	if (memoryNodes[k][j] < 0) {
	  cerr << "RecFileInstance: Worker " << wID << " does not belong to any memory node" << endl;
	  throw(1); 
	}
      }
    }
    
  }

  memoryNodeBW.resize(nbMemoryNodes);
  memoryNodeLat.resize(nbMemoryNodes);
  for(int i = 0; i < nbMemoryNodes; ++i) {
    memoryNodeLat[i].resize(nbMemoryNodes, 0);
    memoryNodeBW[i].resize(nbMemoryNodes, 1);
  }
  rec_rset_t memoryPerfSet = rec_db_get_rset_by_type(platform, "memory_performance"); 
  if(memoryPerfSet) {
    rec_mset_iterator_t memPerfIt = rec_mset_iterator(rec_rset_mset(memoryPerfSet)); 
    rec_record_t recMemPerf; 
    const char* MemoryNodeSrc = rec_parse_field_name_str("MemoryNodeSrc");
    const char* MemoryNodeDst = rec_parse_field_name_str("MemoryNodeDst");
    const char* Bandwidth = rec_parse_field_name_str("Bandwidth");
    const char* Latency = rec_parse_field_name_str("Latency");
    while(rec_mset_iterator_next(&memPerfIt, MSET_RECORD, (const void**) &recMemPerf, NULL)) {
      double bw = stod(recordValue(recMemPerf, Bandwidth));
      double lat = stod(recordValue(recMemPerf, Latency));
      int src = stoi(recordValue(recMemPerf, MemoryNodeSrc));
      int dst = stoi(recordValue(recMemPerf, MemoryNodeDst));
      memoryNodeBW[src][dst] = bw; 
      memoryNodeLat[src][dst] = lat; 
    }
  }

  rec_rset_t timingSet = rec_db_get_rset_by_type(platform, "timing"); 
   if(!timingSet) {
    cerr << "RecFileInstance: no 'timing' record set\n"<< endl; 
    throw(1); 
  }
   rec_mset_iterator_t timingIt = rec_mset_iterator(rec_rset_mset(timingSet)); 
   rec_record_t recTime; 
   const char* Name = rec_parse_field_name_str("Name");
   const char* Model = rec_parse_field_name_str("Model");
   const char* Footprint = rec_parse_field_name_str("Footprint");
   const char* Mean = rec_parse_field_name_str("Mean");
   while(rec_mset_iterator_next(&timingIt, MSET_RECORD, (const void**) &recTime, NULL)) {
     string architecture = recordValue(recTime, arch); 
     string model = recordValueAlternative(recTime, Model, Name); 
     string footprint = recordValue(recTime, Footprint); 
     double mean = stod(recordValue(recTime, Mean)); 
     string type = model + ":" + footprint; 

     int archID; 
     for(archID = 0; archID < nbWorkerTypes; archID++)
       if(workerNames[archID] == architecture)
	 break; 
     if(archID == nbWorkerTypes) {
       cerr << "RecFileInstance: unknown architecture " << architecture << " at " << 
	 rec_record_location_str(recTime) << endl; 
       throw(1); 
     }
     
     auto entry = timings.find(type); 
     if(entry == timings.end()) {
       vector <double> values(nbWorkerTypes, std::numeric_limits<double>::infinity()); 
       timings[type] = values; 
     }
     timings[type][archID] = mean;
   }

   fclose(platformFILE);
  readFromFile(inputFile, timings, useSubmitOrder, appendTags);
}
