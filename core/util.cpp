#include "util.h"
#include <cmath>
#include <sstream>

using namespace std;

strictCompare::strictCompare(intCompare* _cmp, bool _largeFirst): cmp(_cmp), largeFirst(_largeFirst) {}
bool strictCompare::operator() (int a, int b) {
  if(!largeFirst) 
    if((*cmp)(a, b) == 0) 
      return a < b; 
    else 
      return (*cmp)(a, b) >= 1; 
  else 
    if((*cmp)(a, b) == 0) 
      return b < a; 
    else 
      return (*cmp)(a, b) <= -1; 
    
}

rankCompare::rankCompare(vector<double> rank, bool reverse): r(rank), rev(reverse) { }
int rankCompare::operator() (int a, int b) {
  if(rev) {
    /* Allows to still work when r[i] = inf forall i (nodeps case) */
    if((r[b] == r[a]) || (fabs(r[b] - r[a]) < 1e-6))
      return 0; 
    else return (r[a] < r[b]) ? 1 : -1;
  }
  if((r[b] == r[a]) || (fabs(r[a] - r[b]) < 1e-6))
    return 0; 
  else return(r[a] < r[b]) ? -1 : 1; 
}

lexCompare::lexCompare(vector<intCompare*> _cmps): cmps(_cmps) {}
int lexCompare::operator() (int a, int b) {
  for(intCompare* c: cmps) {
    int r = (*c)(a, b); 
    if(r != 0)
      return (r >= 1) ? 1 : -1; 
  }
  return 0; 
}


bool readChar(istream &input, char &c) {
  istream::sentry s(input); 
  if(!s || !input.get(c)) return false; 
  return true; 
}

bool readThisChar(istream &input, char c) {
  char r; 
  return(readChar(input, r) && r == c);
}


std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}




