#include "algoptions.h"
#include "util.h"
#include <vector>
#include <sstream>
#include <iostream>

using namespace std;


void AlgOptions::insert(const string & key, const string &value) {
  (*this)[key] = value; 
}

void AlgOptions::remove(const string& key) {
  if(isPresent(key)) {
    erase(key); 
  }
}

void AlgOptions::include(const string& opt) {
  vector<string> vals = split(opt, '=');
  if(vals.size() > 1)  {
    (*this)[vals[0]] =  vals[1];
  }
  else {
    insert(vals[0], "");
  }
}

string AlgOptions::parseWithName(const string &line) {
  vector<string> opts = split(line, ':');
  auto it = opts.begin(); 
  string name = *it;
  insert("name", name);
  for(it++; it < opts.end(); it++) {
    include(*it); 
  }
  return name;
}

void AlgOptions::parse(const std::string &line) {
  vector<string> opts = split(line, ':');
  for(string &s: opts) {
    include(s); 
  }
}

vector<AlgOptions> AlgOptions::multiParseWithName(const std::string & line, string & name) {
  vector<AlgOptions> result(1, *this); 
  vector<string> opts = split(line, ':');
  int start = 1; 
  for(string s: opts) {
    if(start == 1) {
      name = s; start = 0;
      result[0].insert("name", name);
      continue;
    }
    vector<string> vals = split(s, '=');
    if(vals.size() == 0) {
      for(AlgOptions & o: result) 
	o.insert(vals[0], "");
    } else {
      vector<string> cases = split(vals[1], ','); 
      vector<AlgOptions> dup(result); 
      for(AlgOptions & o: result) {
	o[vals[0]] = cases[0]; 
      }
      for(uint i = 1; i < cases.size(); i++) {
	for(AlgOptions & o: dup) {
	  o[vals[0]] = cases[i]; 
	}
	result.insert(result.end(), dup.begin(), dup.end()); 
      }
    }
  }
  // cerr << "MultiParse: " << name << " " << endl; 
  // for(AlgOptions o: result) {
  //   cerr << o.serialize() << endl; 
  // }

  return result; 
}

string AlgOptions::serialize () const {
  string result;
  result.append(asString("name"));
  for(auto pair  = begin(); pair != end(); pair++) {
    if(pair->first != "name") {
      result.append(":");
      result.append(pair->first).append("=").append(pair->second);
    }
  }
  return result; 
}

bool AlgOptions::isPresent(const string& key) const {
  auto opt = find(key); 
  return(opt != end()); 
}

string AlgOptions::asString(const string& key, const string& def) const {
  auto opt = find(key);
  if(opt == end()) return (def); 
  return(opt->second); 
}

int AlgOptions::asInt(const string& key, const int def) const {
  auto opt = find(key); 
  if(opt == end()) return(def); 
  return (stoi(opt->second)); 
}
double AlgOptions::asDouble(const string& key, const double def) const {
  auto opt = find(key); 
  if(opt == end()) return(def); 
  return (stod(opt->second)); 
}


void AlgOptions::updateValue(double & v, const std::string &key) const {
    if(isPresent(key))
      v = asDouble(key);
}
void AlgOptions::updateValue(int & v, const std::string &key) const {
    if(isPresent(key))
      v = asInt(key);
}
void AlgOptions::updateValue(string & v, const std::string &key) const {
    if(isPresent(key))
      v = asString(key);
}


AlgOptions::AlgOptions(string toParse) {
  parse(toParse);
}

AlgOptions::AlgOptions() {}
