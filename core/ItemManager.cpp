//
// Created by eyraud on 18/12/18.
//

#include <ItemManager.h>
#include <iostream>

#include "ItemManager.h"

using namespace std;

ItemManager::ItemManager(Instance & instance) : ins(&instance), comms(instance),
dataAvailTimes(ins->itemSizes.size()), sourceLocation(ins->itemSizes.size(), -1) {

    vector<bool> isProduced(ins->itemSizes.size(), false);
    for(auto &p : ins->itemsProduced) {
        for (int &i: p)
            isProduced[i] = true;
    }

    for(uint i = 0; i < ins->itemSizes.size(); ++i) {
        dataAvailTimes[i].resize(ins->nbMemoryNodes, -1);
        if(! isProduced[i]) {
            // This is where I assume that all
            // input data are at location 0 at
            // the start.
            sourceLocation[i] = 0;
            dataAvailTimes[i][sourceLocation[i]] = 0;
        }
    }
}

double ItemManager::sendItemTo(int item, int dstMemNode, double currentTime) {
    if(dataAvailTimes[item][dstMemNode] < 0) {
        int src = sourceLocation[item]; // Ideally, I could choose the source location that minimizes commEndTime ?
	if (src < 0 || src >= ins->nbMemoryNodes) {
	  cerr << "sendItemTo for item " << ins->itemNames[item] << " to " << dstMemNode << " at " << currentTime << ": sourceLocation is invalid " << src << endl;
	  throw(1); 
	}
        double commEndTime = comms.newTransfer(dataAvailTimes[item][src], src, dstMemNode, ins->itemSizes[item], currentTime);
        dataAvailTimes[item][dstMemNode] = commEndTime;
        return commEndTime;
//        if(verbosity >= 4)
//            cout << "Greedy: for task " << chosenTask << " on " << idle << " node " << dst
//                 << ": item " << ins.itemNames[item] << " is on " << src << " at "
//                 << dataAvailTimes[item] << ", comm. end delay is " << commEndTime << endl;
    } else {
        return currentTime;
    }
}

void ItemManager::produceItemOn(int item, int memNode, double currentTime) {
    if(sourceLocation[item] >= 0) {
        cerr << "ItemManager: item " << item << " (" << ins->itemNames[item] << ") produced several times: was on " << sourceLocation[item] << ", produced on " << memNode << endl;
        throw(1);
    }
    //    cerr << "ItemManager: Producing item " << item << " (" << ins->itemNames[item] << ") : was on " << sourceLocation[item] << ", produced on " << memNode << endl;
    dataAvailTimes[item][memNode] = currentTime;
    sourceLocation[item] = memNode;
}





