#include "Dmdas.h"
#include "util.h"

#include <vector>
#include <set>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;


Dmdas::Dmdas(const AlgOptions& options) : GreedyAlgorithm(options), 
  ranker(options), verbosity(options.asInt("verbosity", 0)) {
  
}

double Dmdas::compute(Instance &instance, SchedAction* action) {
    // Compute ranks
  ins = &instance;
  readyTasks = ranker.makeSet(instance);

  localQueues.resize(instance.totalWorkers); 
  for(auto && tq: localQueues) {
    tq = ranker.makeSet(instance); 
  }
  assignedLoad.resize(instance.totalWorkers, 0); 
  
  return GreedyAlgorithm::compute(instance, action);
}  

// For now keep the "old" version of dmdas where the assigned load also
// counts lower priority tasks
double Dmdas::getFinishedTime(double now, int task, int w, int wType) {
  double execTime = ins->execType(wType, task);
  if(ins->isValidValue(execTime)) {
    double s = max(endTimesWorkers[w], now); 
    return (s + assignedLoad[w] 
	    + execTime); 
  } else 
    return std::numeric_limits<double>::infinity();
}

int Dmdas::getBestWorker(double now, int task) {
  double bestSoFar = std::numeric_limits<double>::infinity(); 
  int bestWorker = -1;
  int i = 0; 
  for(int wType = 0; wType < ins->nbWorkerTypes; ++wType) {
    for(int j = 0; j < ins->nbWorkers[wType]; ++j, ++i) {
      double t = getFinishedTime(now, task, i, wType); 
      if(t < bestSoFar) {
	bestSoFar = t; bestWorker = i; 
      }
    }
  }
  if(bestWorker < 0) {
    cerr << "Dmdas: no valid worker for task " << task << endl;
    throw(1); 
  }
  return bestWorker; 
}
  
void Dmdas::assignFront(double now)  {
  int task = readyTasks->front(); 
  int w = getBestWorker(now, task); 
  localQueues[w]->insert(task); 
  assignedLoad[w] += ins->execWorker(w, task); 
  readyTasks->eraseFront(); 
}

/* On Task Push does not schedule directly, to make sure
   that the decision about tasks are performed in prioriy order 
   as much as possible */ 
void Dmdas::onTaskPush(int task, double now) {
  readyTasks->insert(task);
}

int Dmdas::chooseTask(int worker, double now) {
  /* Now we assign tasks to workers */
  while(!readyTasks->empty()) 
    assignFront(now); 

  if (!localQueues[worker]->empty()) {
    int result = localQueues[worker]->front();
    localQueues[worker]->eraseFront();
    assignedLoad[worker] -= ins->execWorker(worker, result); 
    return result;
  }

  return -1; 
}
