#include "IndepZhang.h"
#include <iostream>
#include <algorithm>

using namespace std; 

void IndepZhang::updateValue(double & v, const string key, const AlgOptions& opt ) {
  if(opt.isPresent(key))
    v = opt.asDouble(key); 
}

IndepZhang::IndepZhang(const AlgOptions & options) : IndepAllocator(options) {
  updateValue(lambda, "lambda", options); 
  updateValue(beta,   "beta",   options); 
  updateValue(theta,  "theta",  options); 
  updateValue(phi,    "phi",    options);

  sortBy = options.asString("sortby", "none"); 
  rev    = options.asString("rev", "no") == "yes";
  
  needsPreciseLoads = true; 
}

IndepResult IndepZhang::compute(Instance& ins, vector<int> &taskSet, vector<double> &loads) {
  if(ins.nbWorkerTypes != 2) {
    cerr << "IndepZhang: only for two types of ressources, please." << endl;
    throw(1); 
  }

  if(sortBy == "accel")
    sort(taskSet.begin(), taskSet.end(),
	 [&](int t1, int t2) {
	   return ins.execType(0, t1)/ins.execType(1, t1)
	     > ins.execType(0, t2)/ins.execType(1, t2); 
	 });
  else if(sortBy == "min") 
    sort(taskSet.begin(), taskSet.end(),
	 [&](int t1, int t2) {
	   return min(ins.execType(0, t1),ins.execType(1, t1))
	     > min(ins.execType(0, t2),ins.execType(1, t2)); 
	 });
  else if(sortBy == "avg"){
    int m1 = ins.nbWorkers[0]; 
    int m2 = ins.nbWorkers[1]; 
    sort(taskSet.begin(), taskSet.end(),
	 [&](int t1, int t2) {
	   return m1*ins.execType(0, t1) + m2*ins.execType(1, t1)
	     > m1*ins.execType(0, t2) + m2*ins.execType(1, t2); 
	 });
  }
  
  if(rev)
    reverse(taskSet.begin(), taskSet.end()); 
  
  return computeAl5(ins, taskSet, loads); 
}

double IndepZhang::minLoad(Instance & ins, int groupNumber, int* argmin) {
  int start = 0;
  for(int g = 0; g < groupNumber; g++) start += ins.nbWorkers[g];
  double result = workerLoads[start];
  if(argmin) *argmin = start; 
  for(int j = start + 1; j < start + ins.nbWorkers[groupNumber]; j++){
    if(workerLoads[j] < result) {
      result = workerLoads[j]; 
      if(argmin) *argmin = j;
    }
  }
  return result; 
}

void IndepZhang::assignList(Instance& ins, int groupNumber, int task) {
  int w; 
  minLoad(ins, groupNumber, &w);
  workerLoads[w] += ins.execType(groupNumber, task); 
}

IndepResult IndepZhang::computeAl5(Instance& ins, vector<int> &taskSet, vector<double> &loads) {

  int largestGroup  = ins.nbWorkers[0] >= ins.nbWorkers[1]? 0 : 1; 
  int smallestGroup = 1 - largestGroup; 

  int m1 = ins.nbWorkers[largestGroup];
  int m2 = ins.nbWorkers[smallestGroup]; 
  
  workerLoads = loads;
  double minLoadG2 = minLoad(ins, smallestGroup); 
  double rule3LoadG1 = 0;
  double rule3LoadG2 = 0; // Not sure if we should include previous load in this or not. 

  IndepResult res(2); 
  
  for(int &task: taskSet) {
    double p1 = ins.execType(largestGroup, task);
    double p2 = ins.execType(smallestGroup, task);
    int target = -1; 
    // Rule 1
    if( p1 >= beta * (minLoadG2 + p2)  ) target = smallestGroup;
    // Rule 2
    else if ( p1/m1 <= theta  * (p2/m2) ) target = largestGroup;
    else if ( p1/m1 >= lambda * (p2/m2) ) target = smallestGroup;
    // Rule 3
    else if (rule3LoadG1 / m1 <= phi * rule3LoadG2 / m2) {
      target = largestGroup;
      rule3LoadG1 += p1; 
    } else {
      target = smallestGroup;
      rule3LoadG2 += p2; 
    }

    res[target].push_back(task); 
    assignList(ins, target, task);
    minLoadG2 = minLoad(ins, smallestGroup);
  }
  
  return res; 
}
