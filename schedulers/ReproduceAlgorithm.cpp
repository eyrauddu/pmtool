#include "ReproduceAlgorithm.h"
#include <vector>
#include <iostream>

using namespace std;
double ReproduceAlgorithm::compute(Instance& ins, SchedAction * action) {
  
  vector<int>* allocP = (vector<int>*) ins.extraData.get(shareKey + "[worker]");
  vector<double>* startP = (vector<double>*) ins.extraData.get(shareKey + "[start]");
  vector<double>* endP = (vector<double>*) ins.extraData.get(shareKey + "[end]");
  if(!allocP || !startP || !endP) {
    cerr << "ReproduceAlgorithm: data associated with key " << shareKey
	 << " is not complete" << endl;
    throw(1); 
  }
  auto &alloc = *allocP;
  auto & start = *startP;
  auto & end = *endP; 

  double makespan = 0; 
  
  for(int i = 0; i < ins.nbTasks; i++){
    if(alloc[i] < 0 || alloc[i] >= ins.totalWorkers) {
      cerr << "ReproduceAlgorithm: allocation of task " << ins.formatTask(i)
	   << " is not valid: " << alloc[i] << endl;
      throw(1);
    }
    if(start[i] < 0 || start[i] > end[i]) {
      cerr << "ReproduceAlgorithm: timing of task " << ins.formatTask(i)
	   << " is not valid: " << start[i] << " " << end[i] << endl;
      throw(1);
    }
    makespan = max(makespan, end[i]); 
    if(action) {
      action->onSchedule(i, alloc[i], start[i], end[i]); 
    }
  }
  return makespan; 
}

ReproduceAlgorithm::ReproduceAlgorithm(const AlgOptions & opt): shareKey(opt.asString("key")) {
  if(shareKey == "") {
    cerr << "ReproduceAlgorithm: 'key' option required" << endl;
    throw(1); 
  }
}
