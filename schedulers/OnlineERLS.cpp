//
// Created by eyraud on 31/08/18.
//

#include <cmath>
#include "OnlineERLS.h"


int OnlineERLS::assignTask(int task, double now) {
    // In ER-LS algorithm definition, we call GPUs the machine type with the less processors
    int gpuWorker = getEarliestWorker(1 - largestGroup);
    double gpuReadyTime = finishTimes[gpuWorker];
    double gpuComputeTime = ins->execType(1 - largestGroup, task);
    double cpuComputeTime = ins->execType(largestGroup, task);
    if (cpuComputeTime >= gpuReadyTime + gpuComputeTime)
        return gpuWorker;
    if (cpuComputeTime / gpuComputeTime <= threshold)
        return getEarliestWorker(largestGroup);
    else
        return gpuWorker;
}

double OnlineERLS::compute(Instance &instance, SchedAction *action) {
    largestGroup = instance.nbWorkers[0] >= instance.nbWorkers[1] ? 0 : 1;
    threshold = sqrt(instance.nbWorkers[largestGroup] / instance.nbWorkers[1-largestGroup]);
    return OnlineGeneric::compute(instance, action);
}

OnlineERLS::OnlineERLS(const AlgOptions &options) : OnlineGeneric(options) {

}
