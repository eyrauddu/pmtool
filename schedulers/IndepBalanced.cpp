//#include "IndepDP2.h"
#include "IndepBalanced.h"
#include <iostream>
#include <limits>
//#include <new>
#include <algorithm>
//#include <cmath>
#include "util.h"

/* From https://hal.inria.fr/hal-01475884 */
/* TODO: handle correctly tasks which can be executed on only one resource */
using namespace std;


/* BalancedEstimate, Algorithm 1 */
IndepBalancedEst::IndepBalancedEst(const AlgOptions& opt): IndepAllocator(opt) {
  verbosity = opt.asInt("verb_bal", verbosity); 
  // This would have worked  because the IndepAllocator is created before the ranker in IndepBasedRank
  // But unfortunately this breaks the const qualifier.
  //   if(!opt.isPresent("dosort")) 
  //      opt.insert("dosort", "no");
  if(opt.asString("dosort", "yes") != "no") {
    cerr << "IndepBalancedEst: WARNING: to obtain the behavior from the published paper, " 
	 "you need to use dosort=no option. Use at your own risk."  << endl;
  }

  needsPreciseLoads = true; 
}

// task is the local task number (from 0 to # of tasks to be scheduled in this round
// taskID is the global task number (from 0 to # of tasks in the instance)
void allocate(int task, int taskID, int resType, vector<int> &allocation, vector<double> & w, vector<double> &m, vector<int> &im, Instance & ins) {
  double len = ins.execType(resType, taskID); 
  allocation[task] = resType; 
  w[resType] += len / ins.nbWorkers[resType]; 
  if(m[resType] <= len) {
    m[resType] = len; 
    im[resType] = task; 
  }
}

void changeAllocation(int task, int newResType, vector<int> &taskSet, vector<int> &allocation, 
		      vector<double> & w, vector<double> &m, vector<int>& im, Instance & ins) {
  int taskID = taskSet[task];
  if(allocation[task] == newResType) {
    cerr << "changeAllocation: task " << taskID << " already allocated to " << newResType << endl; 
    throw(1); 
  }
  allocate(task, taskID, newResType, allocation, w, m, im, ins); 
  int otherType = 1-newResType; 
  double otherLen = ins.execType(otherType, taskID); 
  w[otherType] -= otherLen / ins.nbWorkers[otherType]; 
  if(im[otherType] == task) {
    /* This task achieved the maximum length on the other resource, 
       need to recompute the maximum length among remaining tasks */
    m[otherType] = 0; 
    im[otherType] = -1; 
    for(unsigned int i = 0; i < allocation.size(); i++){ 
      int id = taskSet[i]; 
      if( (allocation[i] == otherType) && (m[otherType] <= ins.execType(otherType, id)) ) {
	m[otherType] = ins.execType(otherType, id); 
	im[otherType] = i;
      }
    }
  }
}

double computeLambda(vector<double> & w, vector<double> &m) {
  return max(getMax(w), getMax(m));
}


/* Sort tasks in LPT order: largest first */
IndepResult allocToResultAndSort(vector<int>& allocation, vector<int> & taskSet, Instance & ins) {
  
  IndepResult res(2); 
  
  vector<vector<int> >* sortedTaskTypes = ins.getSortedByLength();
  
  vector< vector< vector<int> > > allocByTaskType(2, vector<vector<int> >(ins.nbTaskTypes)); 
  for(unsigned int i = 0; i < allocation.size(); i++) 
    allocByTaskType[allocation[i]][ins.taskTypes[taskSet[i]]].push_back(taskSet[i]); 
  
  for(int j = 0; j < ins.nbWorkerTypes; j++)
    for(int k = 0; k < ins.nbTaskTypes; k++) 
      res[j].insert(res[j].end(), allocByTaskType[j][(*sortedTaskTypes)[j][k]].begin(), allocByTaskType[j][(*sortedTaskTypes)[j][k]].end());

  return res; 
}

double computeMakespan(Instance& ins, vector<int>& allocation, vector<int> & taskSet, vector<double> &loads) {
  IndepResult tasksPerResource = allocToResultAndSort(allocation, taskSet, ins); 
  vector<double> newLoads = loads; 
  int minWorker = 0, maxWorker = 0; 

  for(int j = 0; j < ins.nbWorkerTypes; j++) {
    maxWorker = minWorker + ins.nbWorkers[j]; 
    for(int &t: tasksPerResource[j]) {
      int bestWorker = minWorker; 
      for(int k = minWorker + 1; k < maxWorker; k++) 
	if(newLoads[k] < newLoads[bestWorker])
	  bestWorker = k; 
      newLoads[bestWorker] += ins.execType(j, t); 
    }
    minWorker = maxWorker; 
  }

  return getMax(newLoads); 
}


IndepResult IndepBalancedEst::compute(Instance & ins, vector<int> &taskSet, vector<double> &loads) {

  if(ins.nbWorkerTypes != 2) {
    cerr << "IndepBalanced: only implemented for instances with 2 worker types" << endl; 
    throw(1); 
  }

  int n = taskSet.size(); 
  vector<int> allocation(n); /* allocation[i] = resource type on which task i is allocated */
  vector<double> w(2, 0); /* Compute the total load on each resource type */
  vector<double> m(2, 0); /*         the maximal task length on each resource type */
  vector<int> im(2, -1); /*          which task achieves the maximal task length */
  
  vector<int> bestAlloc; /* To remember them during the computation */
  double bestLambda; 
  vector<int> invAlloc; 

  /* First fill the load array with current loads */
  for(int j = 0; j < 2; j++) {
    int i = 0; 
    for(int k = 0; k < ins.nbWorkers[j]; i++, k++) 
      w[j] += loads[i];
  }

  for(int i = 0; i < n; i++) {
    int t = taskSet[i]; 
    if(ins.execType(0, t) < ins.execType(1, t)) 
      allocate(i, t, 0, allocation, w, m, im, ins); 
    else 
      allocate(i, t, 1, allocation, w, m, im, ins); 
  }

  int mostLoaded = 1; 
  if (w[0] > w[1]) 
    mostLoaded = 0; 
  
#define TYPE1 (1-mostLoaded)
#define TYPE2 (mostLoaded)

  bestAlloc = allocation; 
  bestLambda = computeLambda(w, m); 

  vector<int> order(n); 
  vector<double> accelFactors(n); 
  for(int i = 0; i < n; i++) { 
    order[i] = i; 
    accelFactors[i] = ins.execType(TYPE1, taskSet[i]) / ins.execType(TYPE2, taskSet[i]); 
    // TODO: handle correctly the cases where taskSet[i] cannot be executed on one resource (infinity)
  }
  strictCompare  cmp(new rankCompare(accelFactors, true), false); 
  // TODO: HERE ALSO, CAN SORT BY TASK TYPES. Not sure it's performance critical though. 
  // And doing it correctly requires to record the number of tasks of each type instead of individual tasks. 
  
  sort(order.begin(), order.end(), cmp); 
  int taskIndex = 0; 
  while(taskIndex < n && allocation[order[taskIndex]] != TYPE2)
    taskIndex++; 
  for(; taskIndex < n; taskIndex++) {
    int task = order[taskIndex]; 
    int taskID = taskSet[task]; 
    double loadType1 = ins.execType(TYPE1, taskID) / ins.nbWorkers[TYPE1]; 
    double loadType2 = ins.execType(TYPE2, taskID) / ins.nbWorkers[TYPE2]; 
    if((w[TYPE1] <= w[TYPE2]) && (w[TYPE1] + loadType1 > w[TYPE2] + loadType2))
      invAlloc = allocation; 
    changeAllocation(task, TYPE1, taskSet, allocation, w, m, im, ins); 
    double lambda = computeLambda(w, m); 
    if(lambda < bestLambda){
      bestAlloc = allocation; 
      bestLambda = lambda; 
    }
    
    if((lambda == m[TYPE1]) && accelFactors[im[TYPE1]] > 1 ) {
      /* Task im[TYPE1] is on resource TYPE1, but would be faster on TYPE2 */
      changeAllocation(im[TYPE1], TYPE2, taskSet, allocation, w, m, im, ins); 
    }
  }

  if(invAlloc.size() == 0) 
    invAlloc = allocation; 

  /* Now compute LPT schedules from invAlloc and bestAlloc, return the one with best makespan */
  if(computeMakespan(ins, invAlloc, taskSet, loads) 
     > computeMakespan(ins, bestAlloc, taskSet, loads)) 
    return allocToResultAndSort(bestAlloc, taskSet, ins); 
  else
    return allocToResultAndSort(invAlloc, taskSet, ins); 
}



/* Balanced Makespan, Algorithm 3 */
IndepBalancedMkspan::IndepBalancedMkspan(const AlgOptions& opt): IndepAllocator(opt) {
  verbosity = opt.asInt("verb_mks", verbosity); 
  if(opt.asString("dosort", "yes") != "no") {
    cerr << "IndepBalancedMkspan: WARNING: to obtain the behavior from the published paper, " 
      "you need to use dosort=no option. Use at your own risk."  << endl;
  }
  needsPreciseLoads = true; 
}


IndepResult IndepBalancedMkspan::compute(Instance & ins, vector<int> &taskSet, vector<double> &loads) {

  if(ins.nbWorkerTypes != 2) {
    cerr << "IndepBalanced: only implemented for instances with 2 worker types" << endl; 
    throw(1); 
  }

  int n = taskSet.size(); 
  vector<int> allocation(n); /* allocation[i] = resource type on which task i is allocated */
  vector<double> w(2, 0); /* Compute the total load on each resource type */
  vector<double> m(2, 0); /*         the maximal task length on each resource type */
  vector<int> im(2, -1); /*          which task achieves the maximal task length */
  
  vector<int> bestAlloc; /* To remember them during the computation */
  double bestMkspan; 

  /* First fill the load array with current loads */
  for(int j = 0; j < 2; j++) {
    int i = 0; 
    for(int k = 0; k < ins.nbWorkers[j]; i++, k++) 
      w[j] += loads[i];
  }

  for(int i = 0; i < n; i++) {
    int t = taskSet[i]; 
    if(ins.execType(0, t) < ins.execType(1, t))
      allocate(i, t, 0, allocation, w, m, im, ins); 
    else 
      allocate(i, t, 1, allocation, w, m, im, ins); 
  }

  int mostLoaded = 1; 
  if (w[0] > w[1]) 
    mostLoaded = 0; 
  
#define TYPE1 (1-mostLoaded)
#define TYPE2 (mostLoaded)

  bestAlloc = allocation; 
  bestMkspan = computeMakespan(ins, bestAlloc, taskSet, loads); 

  vector<int> order(n); 
  vector<double> accelFactors(n); 
  for(int i = 0; i < n; i++) { 
    order[i] = i; 
    accelFactors[i] = ins.execType(TYPE1, taskSet[i]) / ins.execType(TYPE2, taskSet[i]); 
    // TODO: handle correctly the cases where i cannot be executed on one resource (infinity)
  }
  // TODO: implement non-strict compare with the correct semantics
  strictCompare cmp(new rankCompare(accelFactors, true), false); 
  sort(order.begin(), order.end(), cmp); 
  int taskIndex = 0; 
  while(taskIndex < n && allocation[order[taskIndex]] != TYPE2)
    taskIndex++; 
  for(; taskIndex < n; taskIndex++) {
    int task = order[taskIndex]; 
    changeAllocation(task, TYPE1, taskSet, allocation, w, m, im, ins); 
    double mkspan = computeMakespan(ins, allocation, taskSet, loads); 
    if(mkspan < bestMkspan){
      bestAlloc = allocation; 
      bestMkspan = mkspan; 
    }
    
    if((computeLambda(w, m) == m[TYPE1]) && accelFactors[im[TYPE1]] > 1 ) {
      /* Task im[TYPE1] is on resource TYPE1, but would be faster on TYPE2 */
      changeAllocation(im[TYPE1], TYPE2, taskSet, allocation, w, m, im, ins); 
      mkspan = computeMakespan(ins, allocation, taskSet, loads); 
      if(mkspan < bestMkspan){
	bestAlloc = allocation; 
	bestMkspan = mkspan; 
      } 
    }
  }


  return allocToResultAndSort(bestAlloc, taskSet, ins); 
}
