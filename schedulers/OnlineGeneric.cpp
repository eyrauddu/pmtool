//
// Created by eyraud on 30/08/18.
//

#include <OnlineGeneric.h>
#include <iostream>

using namespace std;

double OnlineGeneric::compute(Instance &instance, SchedAction *action) {

    ins = &instance;
    // Make sure that reverse dependencies are computed.
    instance.getRevDependencies();

    readyTasks = priority_queue<taskAndReadyTime, vector<taskAndReadyTime>, greater<taskAndReadyTime>>();
    finishTimes.clear(); 
    finishTimes.resize(instance.totalWorkers, 0);
    readyTimes.clear(); 
    readyTimes.resize(instance.nbTasks, -1);
    nbScheduledPredecessors.clear(); 
    nbScheduledPredecessors.resize(instance.nbTasks, 0);

    for(int t = 0; t < instance.nbTasks; ++t) {
        if(instance.dependencies[t].empty()) {
            readyTimes[t] = 0;
            readyTasks.emplace(0, t);
        }
    }

    while(! readyTasks.empty()) {
        taskAndReadyTime next = readyTasks.top();
        readyTasks.pop();
        int worker = assignTask(next.second, next.first);
        double startTime = max(next.first, finishTimes[worker]);
        double finishTime = startTime + instance.execWorker(worker, next.second);

        if(verbosity >= 4)
	  cout << "OG @ " << next.first << " Scheduling task " << next.second << " at time " << startTime << " on worker " << worker << " until time " << finishTime << endl;
        if(action)
            action->onSchedule(next.second, worker, startTime, finishTime);
        finishTimes[worker] = finishTime;

        for(int & succ: instance.revDep[next.second]) {
            ++nbScheduledPredecessors[succ];
            readyTimes[succ] = max(readyTimes[succ], finishTime);
            if(nbScheduledPredecessors[succ] == instance.dependencies[succ].size()){
                readyTasks.emplace(readyTimes[succ], succ);
            }
        }
    }

    return getMax(finishTimes);
}

int OnlineGeneric::getEarliestWorker(int type) {
    int wIndex = 0;
    for(int typeIndex = 0; typeIndex < type; ++typeIndex)
        wIndex += ins->nbWorkers[typeIndex];

    int res = wIndex;
    for(int w = 1; w < ins->nbWorkers[type]; ++w)
        if(finishTimes[wIndex + w] < finishTimes[res])
            res = wIndex + w;
    return res;
}

int OnlineGeneric::getEarliestFinishWorker(int task, double now) {
    int res = getEarliestWorker(0);
    double best = max(now, finishTimes[res]) + ins->execType(0, task);
    for(int w = 1; w < ins->nbWorkerTypes; ++w) {
        double worker = getEarliestWorker(w);
        double endTime = max(now, finishTimes[worker]) + ins->execType(w, task);
        if(endTime < best) {
            res = worker;
            best = endTime;
        }
    }
    return res;
}

OnlineGeneric::OnlineGeneric(const AlgOptions &options) {
    verbosity = options.asInt("verbosity", 0);
}

