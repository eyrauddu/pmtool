//
// Created by eyraud on 11/12/17.
//

#include <iostream>
#include "GreedyPerType.h"


using namespace std;


GreedyPerType::GreedyPerType(const AlgOptions& options): GreedyAlgorithm(options), ranker(options) {
    stealerType = options.asInt("stealer", -1);
    stealerName = options.asString("stealerName");
    if(stealerName != "" && stealerType != -1) {
        cerr << "PreAllocatedGreedy: warning: specified both stealer and stealerName, stealer has precedence." <<endl;
    }
    spoliate = options.asString("spoliate", "yes") == "yes";
    pick = options.asString("pick", "no") == "yes";
}

double GreedyPerType::compute(Instance & instance, SchedAction* action) {
  ins = &instance;

  isStealer.resize(ins->nbWorkerTypes, false);
  if(stealerType != -1) 
    isStealer[stealerType] = true; 
  
  if(stealerName != "" && stealerType == -1) {
    int nbStealers = 0; 
    for(int i = 0; i < ins->nbWorkerTypes; i++) 
      if(ins->workerNames[i].compare(0, stealerName.size(), stealerName) == 0){
	isStealer[i] = true; ++nbStealers; 
      }
    if(nbStealers == 0) {
      cerr << "PreAllocatedGreedy: warning: could not find stealer name " << stealerName << endl;
    } else if(verbosity >= 3)
      cerr << "PreAllocatedGreedy: stealerName " << stealerName << " found " << nbStealers << endl;
  }

  queues.clear();
  for(int i = 0; i < ins->nbWorkerTypes; i++)
    queues.push_back(makeQueue());


  return GreedyAlgorithm::compute(instance, action);
}

int GreedyPerType::chooseTask(int worker, double now) {
    int wType = ins->getType(worker);
    if(queues[wType]->empty()){
        if(isStealer[wType]) {
            int bestTask = -1;
            int chosenVictim = -1;

            if(pick) {
                // Try to steal first, and then try to spoliate
                // if no task is found and spoliate is true.
                for(int t = 0; t < ins->nbWorkerTypes; t++) {
                    if(t != wType && ! queues[t]->empty()) {
                        int candidateTask = queues[t]->front();
                        // With my API I cannot look at all tasks :'(
                        if(ins->isValidType(wType, candidateTask)) {
                            double taskAF = ins->execType(t, candidateTask) / ins->execType(wType, candidateTask);
                            double bestAF = ins->execType(t, bestTask) / ins->execType(wType, bestTask);
                            if(taskAF > 1) {
                                if(bestTask == -1 || (taskAF > bestAF) ||
                                   ( (taskAF == bestAF) && queues[wType] -> compare(candidateTask, bestTask))) {
                                    bestTask = candidateTask;
                                    chosenVictim = t;
                                }
                            }
                        }
                    }
                }

                if(bestTask != -1) {
                    queues[chosenVictim]->eraseFront();
                    return bestTask;
                }
            }

            if(spoliate) {
                for(int victim = 0; victim < ins->totalWorkers; victim++) {
                    int victimType = ins->getType(victim);
                    // ins->getType not efficient, but easier to read
                    if(!isStealer[wType] && runningTasks[victim] != -1
                       && ins->isValidType(wType, runningTasks[victim])
                       && now + ins->execType(wType, runningTasks[victim]) < endTimesWorkers[victim]) {
                        int task = runningTasks[victim];
                        double taskAF = ins->execType(victimType, task)/ins->execType(wType, task);
                        double bestAF = ins->execType(victimType, bestTask)/ins->execType(wType, bestTask);
                        if(bestTask == -1
                           || (taskAF > bestAF)
                           || ((taskAF == bestAF)
                               && queues[wType]->compare(task, bestTask)))  {
                            bestTask = task;
                            chosenVictim = victim;
                        }
                    }
                }
                if(bestTask != -1) {
                    runningTasks[chosenVictim] = -1;
                    return bestTask;
                }
            }
        }
        return -1;
    }
    int result = queues[wType]->front();
    queues[wType]->eraseFront();
    return result;
}

TaskSet *GreedyPerType::makeQueue() {
    return ranker.makeSet(*ins);
}
