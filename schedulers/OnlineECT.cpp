//
// Created by eyraud on 31/08/18.
//

#include "OnlineECT.h"

int OnlineECT::assignTask(int task, double now) {
    return getEarliestFinishWorker(task, now);
}

OnlineECT::OnlineECT(const AlgOptions &options) : OnlineGeneric(options) {

}
