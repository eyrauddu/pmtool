
#include "listAlgorithm.h"
#include "util.h"
#include <set>
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;

ListAlgorithm::ListAlgorithm(const AlgOptions & options):
  GreedyAlgorithm(options), ranker(options) {

}

static const string listAlgName = "list";

double ListAlgorithm::compute(Instance& ins, SchedAction* action) {

  readyTasks = ranker.makeSet(ins);

  return GreedyAlgorithm::compute(ins, action); 
}

int ListAlgorithm::chooseTask(int worker, double now) {

  if (readyTasks->empty())
    return -1; 

  int result = readyTasks->front();
  readyTasks->eraseFront();
  return result; 
}


void ListAlgorithm::onTaskPush(int task, double now) {
  readyTasks->insert(task);
}

