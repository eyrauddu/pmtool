//
// Created by eyraud on 18/09/18.
//

#include <util.h>
#include <iostream>
#include "IndepCLB2C.h"

using namespace std;

IndepCLB2C::IndepCLB2C(const AlgOptions &opt) : IndepAllocator(opt) {
   needsPreciseLoads = true;
}


int getEarliestWorker(vector<double> loads, int from, int to) {
    int best = from;

    for(int i = from+1; i < to; ++i){
        if(loads[i] < loads[best])
            best = i;
    }
    return best;

}

IndepResult IndepCLB2C::compute(Instance &ins, std::vector<int> &taskSet, std::vector<double> &loads) {

    if(ins.nbWorkerTypes != 2) {
        cerr << "IndepCLB2C: only implemented for instances with 2 worker types" << endl;
        throw(1);
    }

    IndepResult result(2);

    if(taskSet.empty())
        return result;

    vector<double> accelFactors(taskSet.size());
    for(int i = 0; i < taskSet.size(); i++)
        accelFactors[i] = ins.execType(0, taskSet[i]) / ins.execType(1, taskSet[i]);
    rankCompare compareByAccelFactor = rankCompare(accelFactors);
    strictCompare strict(&compareByAccelFactor, true);

    int midPointWorker = ins.nbWorkers[0];

    auto tasks = new set<int, strictCompare>(strict);
    for(int i = 0; i < taskSet.size(); i++)
        tasks->insert(i);

    auto posMin = tasks->begin();
    auto posMax = tasks->end(); --posMax;
    int bestWorkerZero = getEarliestWorker(loads, 0, midPointWorker);
    int bestWorkerOne = getEarliestWorker(loads, midPointWorker, ins.totalWorkers);
    int taskZero = taskSet[*posMin];
    int taskOne = taskSet[*posMax];
    while (posMin != posMax) {
        if(loads[bestWorkerZero] + ins.execType(0, taskZero) <= loads[bestWorkerOne] + ins.execType(1, taskOne)) {
            result[0].push_back(taskZero);
            ++posMin;
            loads[bestWorkerZero] += ins.execType(0, taskZero);
            taskZero = taskSet[*posMin];
            bestWorkerZero = getEarliestWorker(loads, 0, midPointWorker);
        } else {
            result[1].push_back(taskOne);
            --posMax;
            loads[bestWorkerOne] += ins.execType(1, taskOne);
            taskOne = taskSet[*posMax];
            bestWorkerOne = getEarliestWorker(loads, midPointWorker, ins.totalWorkers);
        }
    }
    if(loads[bestWorkerZero] + ins.execType(0, taskZero) <= loads[bestWorkerOne] + ins.execType(1, taskOne)) {
        result[0].push_back(taskZero);
    } else {
        result[1].push_back(taskOne);
    }

    return result;
}
