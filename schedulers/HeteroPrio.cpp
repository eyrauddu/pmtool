#include "listAlgorithm.h"
#include "util.h"
#include "HeteroPrio.h"

#include <algorithm>
#include <iostream>
#include <limits>
#include <cmath>
using namespace std;

// TODO: test that it actually still works when used twice in a row with different instances. 

HeteroPrio::HeteroPrio(const AlgOptions & options): ranker(options) {
  // TODO: areaDistribution (requires CPLEX), show steals
  verbosity = options.asInt("verbosity", 0); 
  combinedFactor = options.asDouble("combFactor", 0.00); 
  // Factor for combined GPU view: all tasks with heterogeneity index
  // within combFactor are considered together. 
  alwaysFromFront = (options.asString("front", "no") == "yes"); 
  sortStealByAccFactor = (options.asString("ssbaf", "no") == "yes");
  stealLatest = (options.asString("stealLatest", "no") == "yes"); 
  stealIfIdle = (options.asString("stealidle", "yes") == "yes");
}

void HeteroPrio::init(Instance& ins) {

  readyQueues.clear(); 
  for(int i = 0; i < ins.nbTaskTypes; i++) 
    readyQueues.push_back(ranker.makeSet(ins)); 
  
  revDependencies = ins.getRevDependencies(); 
  
  vector<double> maxTimings(ins.nbTaskTypes, -1); 
  vector<double> minTimings(ins.nbTaskTypes, -1); 
  vector<double> aggregateTimings(ins.nbWorkerTypes, 1); 
  double avgAggregate = 0; 

  for(int j = 0; j < ins.nbTaskTypes; j++) {
    maxTimings[j] = 0;
    minTimings[j] = std::numeric_limits<double>::infinity(); 
    for(int i = 0; i < ins.nbWorkerTypes; i++)
      if(ins.nbWorkers[i] > 0) {
	if(ins.isValidValue(ins.execTimes[i][j])) {
	  maxTimings[j] = max(maxTimings[j], ins.execTimes[i][j]);
	  minTimings[j] = min(minTimings[j], ins.execTimes[i][j]);
	}
      }
  }

  int numberNonEmptyTypes = 0; 
  for(int i = 0; i < ins.nbWorkerTypes; i++) {
    if(ins.nbWorkers[i] > 0) {
      int nbValidTaskTypes = 0; 
      numberNonEmptyTypes ++; 
      for(int j = 0; j < ins.nbTaskTypes; j++) {
	/* TODO: option here to compute arithmetic average ? */
	if(ins.isValidValue(ins.execTimes[i][j])) {
	  aggregateTimings[i] *= ins.execTimes[i][j]; 
	  nbValidTaskTypes ++; 
	}
      }
      aggregateTimings[i] = pow(aggregateTimings[i], 1.0 / nbValidTaskTypes); 
      avgAggregate += aggregateTimings[i]; 
    }
  }
  avgAggregate /= numberNonEmptyTypes; 

  if(verbosity >= 2) {
    cout << "# non empty: " << numberNonEmptyTypes << ", avgAggregate" << avgAggregate << endl; 
    cout << "agTimings: " << aggregateTimings << endl; 
    cout << "maxTimings: " << maxTimings; 
    cout << "minTimings: " << minTimings; 
  }

  workers.resize(ins.nbWorkerTypes); 
  workerPollOrder.resize(ins.nbWorkerTypes); 
  int cumulWorkers = 0; 
  for(int i = 0; i < ins.nbWorkerTypes; i++) {
    workers[i].hetFactor.resize(ins.nbTaskTypes);
    workers[i].RQorder.resize(ins.nbTaskTypes);
    for(int j = 0; j < ins.nbTaskTypes; j++) {
      // Compute Het Factor
      // TODO: HetFactor based on area distribution
      if(ins.isValidValue(ins.execTimes[i][j])) {
	workers[i].hetFactor[j] = (maxTimings[j] * minTimings[j]) / (ins.execTimes[i][j] * ins.execTimes[i][j]);
      } else {
	workers[i].hetFactor[j] = 0; 
      }
      
      workers[i].RQorder[j] = j; 
     }
    strictCompare  cmp(new rankCompare(workers[i].hetFactor, false), false); 
    sort(workers[i].RQorder.begin(), workers[i].RQorder.end(), cmp); 
    workers[i].speedIndex = aggregateTimings[i]; 
    workers[i].isFast = workers[i].speedIndex <= avgAggregate; 
    workerPollOrder[i] = i; 
    workers[i].firstID = cumulWorkers; 
    workers[i].currentTasks.resize(ins.nbWorkers[i], -1); 
    workers[i].nbWorkers = ins.nbWorkers[i]; 
    cumulWorkers += ins.nbWorkers[i]; 

    if(verbosity >= 3) 
      cout << "RQOrder for wType " << i << ": " << workers[i].RQorder 
	   << " HF: " << workers[i].hetFactor << endl;

  }

  sort(workerPollOrder.begin(), workerPollOrder.end(), 
       [&] (int a, int b) {return workers[a].speedIndex < workers[b].speedIndex; }); 
  
  tasks.resize(ins.nbTasks);
  for(int i = 0; i < ins.nbTasks; i++) {
    tasks[i].duration = tasks[i].startTime = -1; 
    tasks[i].nbDep = ins.dependencies[i].size();
    if(tasks[i].nbDep == 0){
      readyQueues[ins.taskTypes[i]]->insert(i);
      //      if(action != NULL) action->onTaskPush(i);
    }
  }

}



  

HeteroPrio::currentInfo HeteroPrio::bestSteal(Instance &ins, int wType, int wLocalID, double currentTime) {

  workerInfo &stealerInfo = workers[wType]; 
 
  vector<currentInfo> candidates; 

  if(verbosity >= 4) 
    cout << "HP: time " << currentTime << " Stealing for worker " 
	 << workers[wType].firstID + wLocalID << endl; 
  
  // Candidates: tasks from slower workers, sorted by priorities
  for(int j = 0; j < ins.nbWorkerTypes; j++) 
    //    if(workers[j].speedIndex > stealerInfo.speedIndex)
      for(int i = 0; i < workers[j].nbWorkers; i++) 
	if( (workers[j].currentTasks[i] != -1)
	    && ins.isValidType(wType, workers[j].currentTasks[i]) )
	  candidates.emplace_back(workers[j].currentTasks[i], j, i);
  
  if(sortStealByAccFactor) {
  sort(candidates.begin(), candidates.end(), 
       [&] (currentInfo a, currentInfo b) {
	 return (stealerInfo.hetFactor[ins.taskTypes[a.taskID]] > stealerInfo.hetFactor[ins.taskTypes[b.taskID]]);
       }); 
  } else {
    if(stealLatest) {
      sort(candidates.begin(), candidates.end(), 
	   [this] (currentInfo a, currentInfo b) {
	     taskInfo & infoA = tasks[a.taskID]; 
	     taskInfo & infoB = tasks[b.taskID];
	     return (infoA.startTime + infoA.duration > infoB.startTime + infoB.duration);
	   });
    } else {
      
      sort(candidates.begin(), candidates.end(), 
	   [this] (currentInfo a, currentInfo b) {
	     // TODO Find a more elegant solution. This clearly prevents HetPrio to work with fifo
	     return readyQueues[0]->compare(a.taskID, b.taskID); 
	   }); 
    }
  }
  for(auto candIt = candidates.begin(); candIt != candidates.end(); candIt++) {
    currentInfo &l = *candIt; 
    taskInfo &c = tasks[l.taskID]; 
    if(c.startTime + c.duration 
       > currentTime + ins.execType(wType, l.taskID)) {

      return l;
    }
  }
  // What if nothing ?
  return currentInfo(-1, -1, -1); 
}

int HeteroPrio::performSteal(HeteroPrio::currentInfo &l) {

  if(l.taskID == -1) // Nothing to do
    return -1; 

  if(verbosity >= 4) 
    cout << "HP: stealing task " << l.taskID << " from worker " << workers[l.wType].firstID + l.wLocalID << endl; 
  
  // Remove it from the worker that currently executes it
  workerInfo &victim = workers[l.wType]; 
  victim.runningTasks --; 
  victim.currentTasks[l.wLocalID] = -1; 
  auto ev = find_if(events.begin(), events.end(), 
		    [&] (event e) {return e.taskID == l.taskID; }); 
  if(ev == events.end()) {
    cerr << "HP: could not find event to steal task" <<endl; 
    throw(-1); 
  }
  events.erase(ev); 
  return l.taskID;
}

double HeteroPrio::compute(Instance& ins, SchedAction* action) {
  init(ins); 
  int nbReadyTasks = getSum<TaskSet*, int> (readyQueues, [] (TaskSet* r) -> int { return r->size(); }); 
  double currentTime = 0; 
  while(nbReadyTasks > 0 || !events.empty() ) {
  // While queue is non empty, or some events are still here.
  
    if(verbosity >= 5) {
      cout << "HP: starting loop, time= " << currentTime << " #RDY Tasks: " << nbReadyTasks << endl;
      // if(verbosity >= 6)
      // 	for(int i = 0; i < ins.nbTaskTypes;  i++) {
      // 	  cout << "Ready queue for task type " << i << " : " << readyQueues[i] << endl;    
      // 	}
    }

    for(auto wkPollit = workerPollOrder.begin(); wkPollit != workerPollOrder.end(); wkPollit++) {
      workerInfo &w = workers[*wkPollit]; 
      while(w.runningTasks < w.nbWorkers) { 
	// At least one worker is currently idle
	int chosenTask = -1; 
	bool chosenFromFront = true; 
	int chosenQueue = -1; 
	for(auto rq = w.RQorder.begin(); rq != w.RQorder.end(); rq++) {
	  if( (! readyQueues[*rq]->empty())
	      && ins.isValidValue(ins.execTimes[*wkPollit][*rq]) ) {
	    chosenQueue = *rq; 
	    if(w.isFast) {
	      chosenTask = readyQueues[*rq]->front(); 
	      chosenFromFront = true; 
	      rq++; 
	      while(rq != w.RQorder.end() 
		    && w.hetFactor[*rq] > (1.0 - combinedFactor) * w.hetFactor[*(rq-1)]) {
		if(! readyQueues[*rq]->empty ()) {
		  int u = readyQueues[*rq]->front(); 
		  if(readyQueues[*rq]->compare(u, chosenTask)) {
		    chosenTask = u; 
		    chosenQueue = *rq;
		  }
		}
		rq++; 
	      }
	    } else {
	      // TODO: choose lowest priority task only if the worker 
	      // is slower for this task type ?
	      // No change for Cholesky, but in general it should help...
	      if(! alwaysFromFront){
		chosenTask = readyQueues[*rq]->back();
		chosenFromFront = false; 
	      } else {
		chosenTask = readyQueues[*rq]->front();
		chosenFromFront = true; 
	      }
	    }
	    break;
	  }
	}
	
	int idleLocalID = find(w.currentTasks.begin(), w.currentTasks.end(), -1) 
	  - w.currentTasks.begin(); 
	if(verbosity >= 5)
	  cout << "HP: task; wType " << *wkPollit << ", nbRun: " << w.runningTasks << " current: " << w.currentTasks << " idle=" << idleLocalID <<  " is Fast: " << w.isFast << " chosenFromFront: " << chosenFromFront << endl; 
	int t = -1; 

	if(stealIfIdle) {
	  if(chosenQueue == -1) { // TOSDO : && w.isFast ?? 
	    currentInfo l = bestSteal(ins, *wkPollit, idleLocalID, currentTime); 
	    t = performSteal(l); 
	  } else {
	    t = chosenTask; 
	  }
 	} else {
	  currentInfo l = bestSteal(ins, *wkPollit, idleLocalID, currentTime); 
	  if(chosenQueue == -1) 
	    t = performSteal(l); 
	  else if(l.taskID == -1) {
	    t = chosenTask; 
	  }	    
	  else {
	    if(w.hetFactor[ins.taskTypes[chosenTask]] >= w.hetFactor[ins.taskTypes[l.taskID]]) {
	      // Use task from ready queue
	      t = chosenTask; 
	    } else {
	      t = performSteal(l); 
	    }
	  }
	}

	if((t == chosenTask) && (chosenQueue != -1)) {
	  if(chosenFromFront)
	    readyQueues[chosenQueue]->eraseFront(); 
	  else 
	    readyQueues[chosenQueue]->eraseBack(); 
	  nbReadyTasks--; 
	}

	if(t != -1) {
	  // Schedule chosen task
	  tasks[t].startTime = currentTime; 
	  tasks[t].duration = ins.execType(*wkPollit, t);
	  if(!ins.isValidValue(tasks[t].duration)) {
	    cerr << "HP: Invalid value of selected task " << t << " on " << *wkPollit << " chosenQueue= " << chosenQueue << endl; 
	    throw(-1); 
	  }
	  double endDate = tasks[t].startTime + tasks[t].duration; 
	  events.emplace(t, endDate, *wkPollit, w.firstID + idleLocalID); 
	  w.runningTasks ++; 
	  w.currentTasks[idleLocalID] = t; 
	  if(verbosity >= 2) 
	    cout << "HP: start exec of task " << t << " type " << chosenQueue << " on worker " 
		 << w.firstID + idleLocalID << " type " << *wkPollit << " to finish at " << endDate << endl; 
	} else {
	  break;
	}
      }
    }

    // Advance time and release tasks when needed
    // TODO: it seems Suraj's code only advances one task at a time. 
    // Not sure if it is equivalent.
    auto nextEventIt = events.begin();
    if(nextEventIt == events.end()) {
      cerr << "Error: no more events" << endl; 
      throw(-1); 
    }
    currentTime = nextEventIt -> date; 
    while(nextEventIt->date == currentTime) {
      if(verbosity >= 2) 
	cout << "HP: finish exec of task " << nextEventIt->taskID << " at time " 
	     << nextEventIt->date << endl; 
      if(action != NULL) 
	action->onSchedule(nextEventIt->taskID, nextEventIt->workerID, 
			   tasks[nextEventIt->taskID].startTime, nextEventIt->date); 
      workerInfo &w = workers[nextEventIt->workerType]; 
      w.runningTasks --; 
      w.currentTasks[nextEventIt->workerID - w.firstID] = -1; 
      
      int t = nextEventIt->taskID; 
      for(auto depIt = revDependencies[t].begin(); depIt != revDependencies[t].end(); 
	  depIt++) {
	tasks[*depIt].nbDep --; 
	if(tasks[*depIt].nbDep == 0) {
	  readyQueues[ins.taskTypes[*depIt]]->insert(*depIt); 
	  nbReadyTasks++; 
	  /* if(action != NULL) 
	     action->onTaskPush(*depIt) */
	}
      }

      
      events.erase(nextEventIt); 
      nextEventIt = events.begin(); 
    }

  }
  
  return currentTime; 
}
