#include "IndepImreh.h"
#include <iostream>
#include "util.h"

using namespace std;


IndepImreh::IndepImreh(const AlgOptions& opt): IndepAllocator(opt) {
  verbosity = opt.asInt("verbosity", verbosity); 
  alpha = opt.asDouble("alpha", 1.0); 
  gamma = opt.asDouble("gamma", 1.0); 
}


// From (DOI) 10.1007/s00607-003-0011-9
// Algorithm MG(alpha, gamma) page 6


IndepResult IndepImreh::compute(Instance& instance, vector<int> &taskSet, vector<double> &loads) {
  if(instance.nbWorkerTypes != 2) {
    cerr << "IndepImreh: only implemented for instances with 2 worker types" << endl; 
    throw(1); 
  }
  if( (alpha < 0) || (alpha > 1.0)) {
    cerr << "IndepImreh: alpha value must be between 0 and 1, not " << alpha << endl; 
    throw(1); 
  }
  if( (gamma <= 0) || (gamma > 1.0)) {
    cerr << "IndepImreh: gamma value must be between 0 and 1, not " << gamma << endl; 
    throw(1); 
  }
  IndepResult result(2); // 2 because there are two resource types. 

  int Pindex, Sindex; // Notation: P is the type with the smaller number of machines
  if(instance.nbWorkers[0] > instance.nbWorkers[1]) {
    Pindex = 1; Sindex = 0; 
  } else {
    Pindex = 0; Sindex = 1; 
  }

  int k = instance.nbWorkers[Pindex]; 
  int m = instance.nbWorkers[Sindex]; 


  if(verbosity >= 4) {
    cout << "IndepImreh: called with TS=" << taskSet << " and loads=" << loads << endl; 
    cout << "     CPU times: "; 
    for(int i : taskSet) cout << instance.execType(0, i) << " "; 
    cout << endl; 
    cout << "     GPU times: "; 
    for(int i : taskSet) cout << instance.execType(1, i) << " "; 
    cout << endl; 
    
  }
  
  if(taskSet.size() == 0) 
    return result; 

  // Keep the stats of set R
  double maxP = 0; 
  double sumP = 0; 

  for(int t: taskSet) {
    double p = instance.execType(Pindex, t); 
    double s = instance.execType(Sindex, t); 
    if(!instance.isValidValue(p)) {
      result[Sindex].push_back(t); 
      continue; 
    }
    if(!instance.isValidValue(s)) {
      result[Pindex].push_back(t); 
      continue; 
    }
    
    if( (p / k) <= alpha * (s / m) ) {
      result[Pindex].push_back(t); 
      continue; 
    }
    
    double r = max((sumP+p)/k, max(maxP, p)); 
    if(r <= alpha * s) {
      result[Pindex].push_back(t); 
      maxP = max(maxP, p); 
      sumP += p; 
      continue; 
    }
    
    result[Sindex].push_back(t); 
  }
 
  if(verbosity >= 3) 
    cout << "Result of IndepImreh: " << result << endl; 
  
   if(result[0].size() + result[1].size() != taskSet.size()) {
     cerr << "IndepImreh: not the correct number of tasks in result !" << endl;
     throw(1); 
   }
  return result; 

}

