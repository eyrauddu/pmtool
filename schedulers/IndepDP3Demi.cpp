#include "IndepDP3Demi.h"
#include <iostream>
#include <limits>
#include <new>
#include <algorithm>
#include <cmath>
#include "util.h"

#ifdef WITH_CPLEX
#include <ilcplex/ilocplex.h>
ILOSTLBEGIN
#endif


using namespace std;


IndepDP3Demi::IndepDP3Demi(const AlgOptions& opt): IndepDualGeneric(opt) {
  discretizationConstant = opt.asDouble("disc", discretizationConstant);
#ifdef WITH_CPLEX
  solveWithCplex = (opt.asString("cplex", "false") == "true") || (opt.asString("cplex", "false") == "discrete");
  cplexUseDiscretizedValues = opt.asString("cplex", "false") == "discrete";
#else
  if(opt.isPresent("cplex"))
    cerr << "Warning: DP3demi: pmtool compiled without cplex support, ignoring 'cplex' option." << endl;
#endif
  
}


// returns minimum CPU load, taking into account existing loads
double IndepDP3Demi::tryGuess(Instance& instance, std::vector<int> taskSet, vector<double>& loads,
			      double target, IndepResult &result, bool getResult) {
  // CPUload(i, g) := smallest load on CPU from the first i tasks,
  //                  with at most g load on GPU
  // So need to discretize the GPU load ? Yes. Paper says with a ratio of lambda/3n
  // For all tasks in taskSet:
  //   forall g, CPUload(i, g) = min CPUload(i-1, g-T^G_i) CPUload(i-1, g) + T^C_i

  int nbCPU = instance.nbWorkers[0]; 
  int nbGPU = instance.nbWorkers[1]; 
  
  double existingCPUload = loads[0];
  double existingGPUload = loads[1]; 
  double maxGPUload = target * nbGPU - existingGPUload; 

  if(maxGPUload < 0) maxGPUload = 1; 

  double ratio = target / (discretizationConstant * taskSet.size()); 
  vector<int> discreteGPUtimings(instance.nbTaskTypes);
  vector<bool> isLongOnCPU(instance.nbTaskTypes);
  vector<bool> isLongOnGPU(instance.nbTaskTypes);
  for(int i = 0; i < instance.nbTaskTypes; i++) {
    discreteGPUtimings[i] = ceil(instance.execTimes[1][i] / ratio);
    isLongOnCPU[i] = instance.execTimes[0][i] > (target/2);
    isLongOnGPU[i] = instance.execTimes[1][i] > (target/2); 
  }
  const int N = ceil(maxGPUload / ratio);

#ifdef WITH_CPLEX
  if(solveWithCplex) {
    IloEnv env;
    IloModel model = IloModel(env);

    IloNumVarArray affect(env, taskSet.size(), 0.0, 1.0, ILOINT); // 0 means on CPU, 1 on GPU
    IloExpr nbTasksWithLargeCPUTime(env);
    IloExpr nbTasksWithLargeGPUTime(env);
    IloExpr totalCPULoad(env);
    IloExpr totalGPULoad(env); 
    for(int i = 0; i < taskSet.size(); ++i) {
      int t = taskSet[i];
      int taskType = instance.taskTypes[t];
      const double exec0 = instance.execTimes[0][taskType];
      const double exec1 = instance.execTimes[1][taskType]; 
      if(isLongOnCPU[taskType])
	nbTasksWithLargeCPUTime += (1-affect[i]);
      if(isLongOnGPU[taskType])
	nbTasksWithLargeGPUTime += affect[i];
      totalCPULoad += (1-affect[i])*exec0;
      totalGPULoad += affect[i]*(cplexUseDiscretizedValues ? discreteGPUtimings[taskType] : exec1);
      if(exec0 > target)
	model.add(affect[i] == 1);
      if(exec1 > target)
	model.add(affect[i] == 0); 
    }

    model.add(nbTasksWithLargeCPUTime <= nbCPU);
    model.add(nbTasksWithLargeGPUTime <= nbGPU);
    if(cplexUseDiscretizedValues)
      model.add(totalGPULoad <= N);
    else
      model.add(totalGPULoad <= maxGPUload); 
    model.add(IloMinimize(env, totalCPULoad)); 

    IloCplex modelCplex = IloCplex(model); 
    if(verbosity < 8) 
      modelCplex.setOut(env.getNullStream());
    modelCplex.setParam(IloCplex::Param::MIP::Tolerances::MIPGap, 0.000001);

    IloBool feasible = modelCplex.solve();
    if(! feasible)
      return std::numeric_limits<double>::infinity(); 

    double value = modelCplex.getObjValue();

    if(getResult) {
      result[0].clear();
      result[1].clear();
      
      IloNumArray affectValues(env);
      modelCplex.getValues(affectValues, affect);
      for(int i = 0; i < taskSet.size(); ++i)
	if(affectValues[i] > 0.5)
	  result[1].push_back(i);
	else
	  result[0].push_back(i); 
    }
    return value + existingCPUload; 
  }
#endif

  

  int nbJobsWithLargeCPUTime = 0;
  int nbJobsWithLargeGPUTime = 0;
  for(int & t: taskSet) {
    int taskType = instance.taskTypes[t];
    if(instance.execTimes[0][taskType] > (target / 2))
      ++ nbJobsWithLargeCPUTime;
    if(instance.execTimes[1][taskType] > (target / 2))
      ++ nbJobsWithLargeGPUTime;
  }
  
  const int maxMu = min(nbCPU, nbJobsWithLargeCPUTime);
  const int maxNu = min(nbGPU, nbJobsWithLargeGPUTime);
  const int stateSpaceSize = (N+1) * (maxMu + 1) * (maxNu + 1); 
  int length = getResult ? taskSet.size() + 1 : 1; 
  double** CPUload = new double*[length];
  if(verbosity >= 7) {
    cerr << "DP3demi: N=" << N << " maxMu= " << maxMu << " maxNu = " << maxNu << endl; 
    cerr << "DP3demi: allocating " << length << " space= " << stateSpaceSize << ", total= " << length * stateSpaceSize << endl;
  }
  CPUload[0] = new double[length * stateSpaceSize]; 
  for(int i = 1; i < length; i++) 
    CPUload[i] = CPUload[i-1] + stateSpaceSize; 

#define getTabValue(tab, l, m, k) (tab[l + (N+1)*(m) + (N+1)*(maxMu+1)*(k)])

  
  if(verbosity >= 7) 
    cout << "IndepDP3Demi: maxGLoad = " << maxGPUload << ", ratio = " << ratio << " N= " << N << " gR: " << getResult << " mL " << target << endl; 

  int index = 0; 
  for(int i = 0; i < stateSpaceSize; ++i) 
    CPUload[index][i] = 0; 
  for(int t : taskSet) {
    const int taskType = instance.taskTypes[t];
    const int nextIndex = getResult ? index+1: index;

    const double exec0 = instance.execTimes[0][taskType];
    const double exec1 = instance.execTimes[1][taskType]; 
    const int discreteGPUtime = discreteGPUtimings[taskType];
    
    const int muOffset = isLongOnCPU[taskType];
    const int nuOffset = isLongOnGPU[taskType];

    if(exec0 > target && exec1 > target) {
      delete[] CPUload[0];
      delete[] CPUload; 
      return -1; // Problem is not feasible: task t cannot be placed on any resource
    }

    if(verbosity >= 8) 
      cout << "Task " << t << " dGPUTime="<< discreteGPUtime << " exec0=" << exec0 << " muOffset="<<muOffset << " nuOffset="<<nuOffset << endl; 
    
    if((exec0 <= target) && (exec1 <= target)) { // Task t can be placed on both resources
      for(int mu = maxMu; mu >= muOffset; --mu) {
	for(int nu = maxNu; nu >= nuOffset; --nu) {
	  for(int l = N; l >= discreteGPUtime; --l) {
	    getTabValue(CPUload[nextIndex], l, mu, nu) = min(getTabValue(CPUload[index], l, mu-muOffset, nu) + exec0,
							     getTabValue(CPUload[index], l - discreteGPUtime, mu, nu-nuOffset));
	  }
	  for(int l = discreteGPUtime - 1; l >= 0; --l) {
	    getTabValue(CPUload[nextIndex], l, mu, nu) = getTabValue(CPUload[index], l, mu-muOffset, nu) + exec0;
	  }
	}
	if(nuOffset) {
	  for(int l = N; l >= 0; --l) {
	    getTabValue(CPUload[nextIndex], l, mu, 0) = getTabValue(CPUload[index], l, mu-muOffset, 0) + exec0;
	  }
	}
      }

      if(muOffset) {
	for(int nu = maxNu; nu >= nuOffset; --nu) {
	  for(int l = N; l >= discreteGPUtime; --l) {
	    getTabValue(CPUload[nextIndex], l, 0, nu) = getTabValue(CPUload[index], l - discreteGPUtime, 0, nu-nuOffset);
	  }
	  for(int l = discreteGPUtime - 1; l >= 0; --l) {
	    getTabValue(CPUload[nextIndex], l, 0, nu) = std::numeric_limits<double>::infinity(); 
	  }
	}
	if(nuOffset) {
	  for(int l = N; l >= 0; --l) {
	    getTabValue(CPUload[nextIndex], l, 0, 0) = std::numeric_limits<double>::infinity(); 
	  }
	}
      }
    } else if ((exec0 <= target) && (exec1 > target)) { // Task t can only be placed on CPUs
      for(int mu = maxMu; mu >= muOffset; --mu) {
	for(int nu = maxNu; nu >= 0; --nu) {
	  for(int l = N; l >= 0; --l) {
	    getTabValue(CPUload[nextIndex], l, mu, nu) = getTabValue(CPUload[index], l, mu-muOffset, nu) + exec0; 
	  }
	}
      }
      if(muOffset) {
	for(int nu = maxNu; nu >= 0; --nu) {
	  for(int l = N; l >= 0; --l) {
	    getTabValue(CPUload[nextIndex], l, 0, nu) = std::numeric_limits<double>::infinity();
	  }
	}
      }
      
    } else /* ((exec0 > target) && (exec1 <= target)) */ { // Task t can only be placed on GPUs
      for(int mu = maxMu; mu >= 0; --mu) {
	for(int nu = maxNu; nu >= nuOffset; --nu) {
	  for(int l = N; l >= discreteGPUtime; --l) {
	    getTabValue(CPUload[nextIndex], l, mu, nu) = getTabValue(CPUload[index], l - discreteGPUtime, mu, nu-nuOffset);
	  }
	  for(int l = discreteGPUtime - 1; l >= 0; l--) {
	    getTabValue(CPUload[nextIndex], l, mu, nu) = std::numeric_limits<double>::infinity(); 
	  }
	}
	if(nuOffset) {
	  for(int l = N; l >= 0; l--) {
	    getTabValue(CPUload[nextIndex], l, mu, 0) = std::numeric_limits<double>::infinity(); 
	  }
	}
      }
    }
    
    index = nextIndex;
    if(verbosity >= 8) { 
      cout << "DP3Demi: state after task " << t << endl;
      for(int mu = 0; mu <= maxMu; ++mu)
	for(int nu = 0; nu <= maxNu; ++nu) {
	  cout << "  ";
	  for(int l = 0; l <= N; ++l)
	    cout << " " << getTabValue(CPUload[index], l, mu, nu);
	  cout << endl;
	}
    }
    

  }

  double value = getTabValue(CPUload[index], N, maxMu, maxNu); 
  if(verbosity >= 7)
    cerr << "DP3demi: final value is " << value << endl; 
  
  int gLoad = N;
  int mu = maxMu;
  int nu = maxNu; 
  if(getResult &&   (value != std::numeric_limits<double>::infinity())) {
    
    result[0].clear();
    result[1].clear();
    
    for(; index > 0; index--) {
      const int taskType = instance.taskTypes[taskSet[index-1]];
      const double exec0 = instance.execTimes[0][taskType];
      const double exec1 = instance.execTimes[1][taskType]; 
      const int discreteGPUtime = discreteGPUtimings[taskType];
      
      const int muOffset = isLongOnCPU[taskType];
      const int nuOffset = isLongOnGPU[taskType]; 
      
      if((mu >= muOffset) && 
	 (abs(getTabValue(CPUload[index], gLoad, mu, nu) - (getTabValue(CPUload[index-1], gLoad, mu-muOffset, nu) + exec0)) <= 1e-5)) {
	mu -= muOffset; 
	result[0].push_back(taskSet[index-1]);
      }
      else {
	gLoad -= discreteGPUtimings[taskType];
	nu -= nuOffset; 
	result[1].push_back(taskSet[index-1]);
      }
    }
    double actualLoad = 0;
    for(int & t: result[0])
      actualLoad += instance.execTimes[0][instance.taskTypes[t]];

    if(abs(actualLoad-value) > 0.0001)
      cerr << "DP3Demi Warning: Difference between computed load and actual load: " << value << " " << actualLoad << endl;
    
  }
  delete[] CPUload[0]; 
  delete[] CPUload; 

  return value + existingCPUload; 
}
