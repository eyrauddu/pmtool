
#include "availSequence.h"
#include <limits>
#include <iostream>

using namespace std; 

AvailSequence::AvailSequence(double start) {
  //   Period p(); 
  seq.emplace_back(start, std::numeric_limits<double>::infinity()); /* ca marche ça ? */
}

timeSeq::iterator AvailSequence::getAvail(double from, double len, double& start) {
  timeSeq::iterator i; 
  for(i = seq.begin(); i != seq.end(); i++) {
    if(i->end >= from) break; 
  }
  if(len <= i->end - std::max(from, i->start)) {
    start = std::max(from, i->start); 
  } else {
    for(i++; i != seq.end(); i++) {
      if(len <= i->end - i->start) break;
    }
    start = i->start; 
  }
  return i; 
}

timeSeq::iterator AvailSequence::getAvailLatest(double from, double len, double latest, 
						double& start, timeSeq::iterator &i) {
  timeSeq::iterator res = i; 
  bool found = false; 
  for(; i != seq.end(); i++) 
    if(i->end >= from) break; 
  if(i->start + len > latest) {
    cerr << "getAvailLatest " << from << " " << len << " " << latest
	 << ": starting interval is too late:" << i->start + len << endl; 
    throw(150); 
  }
  if(from + len > latest) {
    cerr << "getAvailLatest " << from << " " << len << " " << latest
	 << ": earliest time is too late:" << i->start + len << endl; 
    throw(150); 
  }
  //  cerr << "gAL: " <<  std::min(i->end, latest) << " " << std::max(from, i->start) << " " << len << " " << std::min(i->end, latest) - std::max(from, i->start) << " " << (len +  std::max(from, i->start) <= std::min(i->end, latest)) << endl; 

  // This looks highly unstable: for unknown reason, if I write
  // len <= min - max, this does not detect equality correctly. 
  if(std::max(from, i->start) + len <= std::min(i->end, latest)) {
    res = i; 
    start = std::min(i->end, latest) - len;
    found = true; 
  }
  for(i++; i != seq.end(); i++) {
    if(i->start > latest) break; 
    if(i->start + len <= std::min(i->end, latest)) {
      start = std::min(i->end, latest) - len; 
      res = i; 
      found = true; 
    }
  }
  if(!found) {
    cerr << "getAvailLatest " << from << " " << len << " " << latest
	 << ": no interval found, ended at " << i->start << " " << i->end << endl; 
    display(cerr); 
    throw(44); 
  }
  return res; 
}

void AvailSequence::insertBusy(timeSeq::iterator i, double start, double len) {
  /* Assumes i != seq.end(), i->start <= start, i->end >= start + len */ 
  if((i == seq.end()) || (i->start > start + epsilon) || (i->end + epsilon < start + len)) {
    cerr << "AvailSequence::insertBusy: invalid arguments. Start " << start 
	 << ", len " << len << ", interval [" << i->start << "," << i->end << "]" << endl; 
    throw(12); 
  }
  Period orig = *i; 
  auto j = seq.erase(i); 
  if(orig.start < start) {
    // Period before(orig.start, start); 
    seq.emplace(j, orig.start, start); 
  }
  if(orig.end > start + len) {
    // Period after(start + len, orig.end); 
    seq.emplace(j, start+len, orig.end);
  }
}

void AvailSequence::insertBusy(double start, double len) {
  double actualStart; 
  timeSeq::iterator i = getAvail(start, len, actualStart); 
  insertBusy(i, actualStart, len); 
}

void AvailSequence::display(std::ostream &out) {
  for(auto it = seq.begin(); it != seq.end(); it++) {
    out << "[" << it->start << " " << it->end << "] "; 
  } 
  out << std::endl;
}
