//
// Created by eyraud on 30/08/18.
//

#include <cmath>
#include "OnlineQA.h"

using namespace std;

int OnlineQA::assignTask(int task, double now) {
    double accel = ins->execType(0, task) / ins->execType(1, task);
    return (accel <= threshold) ? getEarliestWorker(0) : getEarliestWorker(1);
}

double OnlineQA::compute(Instance &instance, SchedAction *action) {
    threshold = sqrt(instance.nbWorkers[0] / instance.nbWorkers[1]);
    return OnlineGeneric::compute(instance, action);
}

OnlineQA::OnlineQA(const AlgOptions &options) : OnlineGeneric(options) {

}
