#include "IndepDP2.h"
#include <iostream>
#include <limits>
#include <new>
#include <algorithm>
#include <cmath>
#include "util.h"

#ifdef WITH_CPLEX
#include <ilcplex/ilocplex.h>
ILOSTLBEGIN
#endif

using namespace std;


IndepDP2::IndepDP2(const AlgOptions& opt): IndepDualGeneric(opt) {
  discretizationConstant = opt.asDouble("disc", discretizationConstant); 
#ifdef WITH_CPLEX
  solveWithCplex = (opt.asString("cplex", "false") == "true") || (opt.asString("cplex", "false") == "discrete");
  cplexUseDiscretizedValues = opt.asString("cplex", "false") == "discrete";
#else
  if(opt.isPresent("cplex"))
    cerr << "Warning: DP2: pmtool compiled without cplex support, ignoring 'cplex' option." << endl;
#endif
}


// returns minimum CPU load, taking into account existing loads
double IndepDP2::tryGuess(Instance& instance, std::vector<int> taskSet, vector<double>& loads,
			  double maxlen, IndepResult &result, bool getResult) {
  // CPUload(i, g) := smallest load on CPU from the first i tasks,
  //                  with at most g load on GPU
  // So need to discretize the GPU load ? Yes. Paper says with a ratio of lambda/3n
  // For all tasks in taskSet:
  //   forall g, CPUload(i, g) = min CPUload(i-1, g-T^G_i) CPUload(i-1, g) + T^C_i


  double existingCPUload = loads[0];
  double existingGPUload = loads[1]; 
  double maxGPUload = maxlen * instance.nbWorkers[1] - existingGPUload; 
  
  if(maxGPUload < 0) maxGPUload = 1; 

  double ratio = maxlen / (discretizationConstant * taskSet.size()); 
  vector<int> discreteGPUtimings(instance.nbTaskTypes);
  for(int i = 0; i < instance.nbTaskTypes; i++) 
    discreteGPUtimings[i] = ceil(instance.execTimes[1][i] / ratio); 
  const int N = ceil(maxGPUload / ratio); 

#ifdef WITH_CPLEX
  if(solveWithCplex) {
    IloEnv env;
    IloModel model = IloModel(env);

    IloNumVarArray affect(env, taskSet.size(), 0.0, 1.0, ILOINT); // 0 means on CPU, 1 on GPU
    IloExpr totalCPULoad(env);
    IloExpr totalGPULoad(env); 
    for(int i = 0; i < taskSet.size(); ++i) {
      int t = taskSet[i];
      int taskType = instance.taskTypes[t];
      const double exec0 = instance.execTimes[0][taskType];
      const double exec1 = instance.execTimes[1][taskType]; 
      totalCPULoad += (1-affect[i])*exec0;
      totalGPULoad += affect[i]*(cplexUseDiscretizedValues ? discreteGPUtimings[taskType] : exec1);
      if(exec0 > maxlen)
	model.add(affect[i] == 1);
      if(exec1 > maxlen)
	model.add(affect[i] == 0); 
    }

    if(cplexUseDiscretizedValues)
      model.add(totalGPULoad <= N);
    else
      model.add(totalGPULoad <= maxGPUload); 
    model.add(IloMinimize(env, totalCPULoad)); 

    IloCplex modelCplex = IloCplex(model); 
    if(verbosity < 8) 
      modelCplex.setOut(env.getNullStream());
    modelCplex.setParam(IloCplex::Param::MIP::Tolerances::MIPGap, 0.0000001);
    
    
    IloBool feasible = modelCplex.solve();
    if(! feasible)
      return std::numeric_limits<double>::infinity(); 

    double value = modelCplex.getObjValue();

    if(getResult) {
      result[0].clear();
      result[1].clear();
      
      IloNumArray affectValues(env);
      modelCplex.getValues(affectValues, affect);
      for(int i = 0; i < taskSet.size(); ++i)
	if(affectValues[i] > 0.5)
	  result[1].push_back(i);
	else
	  result[0].push_back(i);

      // cout << "CPUResult: "  << result[0] << endl; 
      // cout << "GPUResult: "  << result[1] << endl; 
      
    }
    return value + existingCPUload; 
  }
#endif

  int length = getResult ? taskSet.size() + 1 : 1; 
  double** CPUload = new double*[length]; 
  CPUload[0] = new double[length * (N+1)]; 
  for(int i = 1; i < length; i++) 
    CPUload[i] = CPUload[i-1] + N+1; 
  
  
  if(verbosity >= 7) 
    cout << "TryGuess: maxGLoad = " << maxGPUload << ", ratio = " << ratio << " N= " << N << " gR: " << getResult << " mL " << maxlen << endl; 

  int index = 0; 
  for(int i = 0; i <= N; i++) 
    CPUload[index][i] = 0; 
  for(int t : taskSet) {
    int taskType = instance.taskTypes[t];
    int nextIndex = getResult ? index+1: index;

    double exec0 = instance.execTimes[0][taskType];
    double exec1 = instance.execTimes[1][taskType]; 
    int discreteGPUtime = discreteGPUtimings[taskType];
    
    // Possible optimization if needed: run this test for all
    // taskTypes (which appear in the taskSet) instead of for all
    // tasks
    if(exec0 > maxlen && exec1 > maxlen){
      delete[] CPUload[0];
      delete[] CPUload; 
      return -1; // Problem is not feasible: task t cannot be placed on any resource
    }
    if((exec0 <= maxlen) && (exec1 <= maxlen)) {
      for(int l = N; l >= discreteGPUtime; --l) {
	CPUload[nextIndex][l] = min(CPUload[index][l] + exec0, CPUload[index][l - discreteGPUtime]);
      }
      for(int l = discreteGPUtime - 1; l >= 0; --l) {
	CPUload[nextIndex][l] = CPUload[index][l] + exec0;
      }
    } else if ((exec0 <= maxlen) && (exec1 > maxlen)) {
      for(int l = N; l >= 0; --l) {
	CPUload[nextIndex][l] = CPUload[index][l] + exec0; 
      }
    } else /* ((exec0 > maxlen) && (exec1 <= maxlen)) */ {
      for(int l = N; l >= discreteGPUtime; --l) {
	CPUload[nextIndex][l] = CPUload[index][l - discreteGPUtime];
      }
      for(int l = discreteGPUtime - 1; l >= 0; l--) {
	  CPUload[nextIndex][l] = std::numeric_limits<double>::infinity(); 
      }
    }

    
    // for(int l = N; l >= 0; l--) {
    //   double newLoad = std::numeric_limits<double>::infinity(); 
    //   if( <= maxlen)
    // 	newLoad = CPUload[index][l] + instance.execTimes[0][taskType]; 
    //   if((instance.execTimes[1][taskType] <= maxlen) && (discreteGPUtimings[taskType] <= l))
    // 	newLoad = min(newLoad, CPUload[index][l - discreteGPUtimings[taskType]]); 
    //   CPUload[nextIndex][l] = newLoad; 
    // }
    index = nextIndex; 
  }

  double value = CPUload[index][N]; 
  
  int gLoad = N; 

  if(getResult && value != std::numeric_limits<double>::infinity()) {
    
    result[0].clear();
    result[1].clear();
    
    for(; index > 0; index--) {
      int taskType = instance.taskTypes[taskSet[index-1]];
      if(CPUload[index][gLoad] == CPUload[index-1][gLoad] + instance.execTimes[0][taskType])
	result[0].push_back(taskSet[index-1]); 
      else {
	gLoad -= discreteGPUtimings[taskType]; 
	result[1].push_back(taskSet[index-1]);
      }
    }
  }
  delete[] CPUload[0]; 
  delete[] CPUload; 

  return value + existingCPUload; 
}
