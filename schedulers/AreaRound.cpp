//
// Created by eyraud on 12/06/18.
//

#include <AreaBound.h>
#include <algorithm>
#include <IndepAllocator.h>
#include "AreaRound.h"
#include "util.h"
using namespace std;

AreaRound::AreaRound(const AlgOptions &options) : IndepAllocator(options), bound(AlgOptions()) {
}

IndepResult AreaRound::compute(Instance &ins, std::vector<int> &taskSet, std::vector<double> &loads) {

    //cout << "AR: called with taskSet " << taskSet << endl;

    vector<int> countTasks(ins.nbTaskTypes, 0);
    for(int & t: taskSet)
        countTasks[ins.taskTypes[t]]++;

    bound.init(ins);
    for(int t = 0; t < ins.nbTasks; ++t)
        bound.removeTask(t);
    for(int & t: taskSet)
        bound.restoreTask(t);
    bound.compute();

    vector< vector<double> > repartition = bound.getRepartition();

    vector<vector<int>> roundedRepartition(ins.nbTaskTypes, vector<int>(ins.nbWorkerTypes));
    for(int i = 0; i < ins.nbTaskTypes; i++)
        for(int j = 0; j < ins.nbWorkerTypes; j++)
            roundedRepartition[i][j] = static_cast<int>(floor(repartition[i][j]));

    for(int tIndex = 0; tIndex < ins.nbTaskTypes; tIndex++) {
        int toAssign = countTasks[tIndex] - getSum(roundedRepartition[tIndex]);
        vector<pair<double, int>> excess(ins.nbWorkerTypes);
        for(int j = 0; j < ins.nbWorkerTypes; j++)
            excess[j] = make_pair(repartition[tIndex][j] - roundedRepartition[tIndex][j], j);

        nth_element(excess.begin(), excess.begin() + toAssign, excess.end(),
                    [](pair<double, int> a, pair<double, int> b) {return (a.first > b.first);});
        for(int k = 0; k < toAssign; k++) {
            auto& v = excess[k];
            roundedRepartition[tIndex][v.second]++;
        }
    }

    IndepResult result(ins.nbWorkerTypes,IndepOneResult());

    vector<vector<int> >* sortedTaskTypes = ins.getSortedByLength();

    vector< vector< vector<int> > > allocByTaskType(ins.nbWorkerTypes, vector<vector<int> >(ins.nbTaskTypes));
    for(int & task: taskSet) {
        int taskType = ins.taskTypes[task];
        bool found = false;
        for (int j = 0; j < ins.nbWorkerTypes; ++j) {
            if (roundedRepartition[taskType][j] > 0) {
                --roundedRepartition[taskType][j];
                allocByTaskType[j][taskType].push_back(task);
                found = true;
                break;
            }
        }
        if(!found){
            cerr << "AreaRound: could not find a target for task " << task << " of type " << taskType << endl;
            throw(1);
        }
    }


    for(int j = 0; j < ins.nbWorkerTypes; j++)
        for(int k = 0; k < ins.nbTaskTypes; k++)
            result[j].insert(result[j].end(), allocByTaskType[j][(*sortedTaskTypes)[j][k]].begin(), allocByTaskType[j][(*sortedTaskTypes)[j][k]].end());

    return result;
}
