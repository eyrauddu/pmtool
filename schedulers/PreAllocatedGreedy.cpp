#include "PreAllocatedGreedy.h"
#include <iostream>

using namespace std;


intCompare* GreedyRanker::oneCmp(Instance& ins, string subRankOpt) {
    vector<double> w;
    string old(subRankOpt);
    bool reverse = isReverse(subRankOpt);
    if(subRankOpt == "alloc") {
        w = ins.computePreAllocRank(preAssign);
        return (new rankCompare(w, reverse));
    } else {
        return RankComputer::oneCmp(ins, old);
    }
}

TaskSet* GreedyRanker::makeSet(Instance & ins, vector<int> alloc) {
    preAssign = std::move(alloc);
    return RankComputer::makeSet(ins);
}

GreedyRanker::GreedyRanker(const AlgOptions &options) : RankComputer(options) {
    rankOpt = options.asString("rank", "alloc");
}


PreAllocatedGreedy::PreAllocatedGreedy(const AlgOptions& options): GreedyPerType(options), ranker(options) {
  file = options.asString("file", "");
  key = options.asString("key", "");
  if(file.empty()) {
      if(key.empty()) {
          cerr << "PreAllocatedGreedy: option 'file' or 'key' is mandatory" << endl;
          throw(1);
      }
  } else if(!key.empty()) {
      cerr << "PreAllocatedGreedy: both 'file' and 'key' present, using 'key'" << endl;
  }

}

double PreAllocatedGreedy::compute(Instance & instance, SchedAction* action) {
    preAssign.resize(instance.nbTasks);
    if(!key.empty()) {
        vector<int> *values = (vector<int> *) instance.extraData.get(key);
        if (!values) {
            cerr << "PreAllocatedGreedy: could not find shared allocation " << file
                 << ". Did you really use the share=<...> option in a previous algorithm?" << endl;
            throw (1);
        }
        for (int i = 0; i < instance.nbTasks; i++) {
            preAssign[i] = (*values)[i];
        }
    } else if(!file.empty()) {
      ifstream inputStream(file);
      if(!inputStream) {
          cerr << "PreAllocatedGreedy: cannot open " << file << endl;
          throw(1);
      }
      for(int i = 0; i < instance.nbTasks; i++) {
          inputStream >> preAssign[i] >> ws;
      }
    }

  for(int i = 0; i < instance.nbTasks; i++) {
    int val = preAssign[i];
    if(val < 0 || val >= instance.nbWorkerTypes) {
      cerr << "PreAllocatedGreedy " << file << ": wrong pre assignment for task " << i
	   << " : " << val << endl;
      throw(1);
    }
  }
  return GreedyPerType::compute(instance, action);
}

void PreAllocatedGreedy::onTaskPush(int task, double now) {
  queues[preAssign[task]]->insert(task); 
}

TaskSet *PreAllocatedGreedy::makeQueue() {
    return ranker.makeSet(*ins, preAssign);
}
