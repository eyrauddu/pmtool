
#include "GreedyAlgorithm.h"
#include "util.h"
#include <set>
#include <iostream>
#include <algorithm>

using namespace std;

GreedyAlgorithm::GreedyAlgorithm(const AlgOptions & options) {
  verbosity = options.asInt("verbosity", 0);
  doComms = options.asString("comms", "no") == "yes";
}

double GreedyAlgorithm::compute(Instance& ins, SchedAction* action) {

  if(doComms) {
    itemManager = new ItemManager(ins);
  } else {
    itemManager = NULL; 
  }
  
  int nbWorkers = ins.totalWorkers; 
  int n = ins.nbTasks; 

  vector< vector<int> > revDependencies = ins.getRevDependencies(); 

  leaveIdle = vector<bool>(nbWorkers, false); 
  endTimesWorkers = vector<double>(nbWorkers, 0); 
  nbDep = vector<int>(n, 0);
  runningTasks = vector<int>(nbWorkers, -1); 

  vector<bool> finishedTasks(n, false); 

  for(int i = 0; i < n; i++) {
    nbDep[i] = ins.dependencies[i].size();
    if(nbDep[i] == 0){
      onTaskPush(i, 0);
      if(action != NULL) action->onTaskPush(i);
    }
  }
  
  double currentTime = 0; 

  int idle = 0;
  int nbFinishedTasks = 0;

  while(nbFinishedTasks < n) {
  
    int index = 0; 
    int t = ins.getType(idle, &index); 

    if(verbosity >= 5) {
      cout << "Greedy: " << currentTime << " worker " << idle << " of type " << t << " is idle." << endl; 
    }

    int chosenTask = chooseTask(idle, currentTime); 
  
    if(chosenTask < 0) {
      if(verbosity >= 6) 
	cout << "Greedy: " << currentTime << " leaving worker " << idle << " idle." << endl; 
      leaveIdle[idle] = true; 
    }
    
    if(chosenTask >= 0) {
      // Check that all dependencies are satisfied
      for(int k : ins.dependencies[chosenTask]) 
	if(!finishedTasks[k]) {
	  cerr << "Greedy: chosenTask is " << chosenTask << " but its dependency " << k << " is not finished" << endl; 
	  throw(1); 
	}
      
      if(verbosity >= 6) 
	cout << "Greedy: " << currentTime << " chosen task is #" << chosenTask << ", " << ins.taskIDs[chosenTask] << endl; 

      double startTime = currentTime; 
      if(doComms) {
	// Compute a finish time which takes communication into account.
    	for(int &item: ins.itemsRequired[chosenTask]) {
	  int dst = ins.memoryNodes[t][index];
	  startTime = max(startTime, itemManager->sendItemTo(item, dst, currentTime));
	}
      }

      double finishTime = startTime + ins.execWorker(idle, chosenTask);
      if(verbosity >= 4) 
	cout << "Greedy: starting " << chosenTask << " of type " << ins.taskTypes[chosenTask] << " at " << startTime << " to end at " << finishTime << " on worker " << idle << " of type " << t << endl;

      endTimesWorkers[idle] = finishTime;
      runningTasks[idle] = chosenTask; 

      for(int l = 0; l < nbWorkers; l++) 
	leaveIdle[l] = false;
      
    }
    
    // Is a worker idle now ?
    idle = -1; 
    for(int j = 0; j < nbWorkers; j++) 
      if(!leaveIdle[j] && runningTasks[j] == -1) {
	idle = j; 
	break; 
      }
    
    // If idle is found, all is good, we continue with the loop. Otherwise, advance time.
    if(idle == -1) {
      // Find the next worker to finish
      double nextTime = currentTime; 
      for(int i = 0; i < nbWorkers; i++){
	if(endTimesWorkers[i] > currentTime)
	  if((idle == -1) || (endTimesWorkers[i] < nextTime)) { 
	    idle = i; nextTime = endTimesWorkers[i]; 
	  }
      }
      if(verbosity >= 6) 
	cout << "Greedy " << currentTime << " before advancing to " << nextTime << ", idle worker is" << idle << " LI: " << leaveIdle << " ETW: " << endTimesWorkers << endl; 
      if(idle == -1) {
	cerr << "Greedy: Found no eligible worker. currentTime=" << currentTime;
	cerr << "endTimesWorkers: " << endTimesWorkers << endl;
	throw(-1);
      }
      
      currentTime = max(currentTime, nextTime); 
      
      //Finish all tasks with finishTime <= currentTime
      for(int w = 0; w < nbWorkers; w++) 
	if((runningTasks[w] != -1) && (endTimesWorkers[w] <= currentTime)) {
	  int i = runningTasks[w]; 
	  if(verbosity >= 4) 
	    cout << "Greedy: Finishing task " << i << " on worker " << w << " at time " << currentTime << endl;
	  if(action != NULL) 
	    action->onSchedule(runningTasks[w], w, 
			       endTimesWorkers[w] - ins.execWorker(w, runningTasks[w]), endTimesWorkers[w]);
	  if(doComms) {
	    int index; 
	    int wType = ins.getType(w, &index); 
	    for(int& item: ins.itemsProduced[i]) {
	      int location = ins.memoryNodes[wType][index];
	      itemManager->produceItemOn(item, location, currentTime);
	    }
	  }
	  runningTasks[w] = -1; 
	  if(finishedTasks[i]) {
	    cerr << "Greedy: task " << i << " was already finished !" << endl; 
	    throw(1); 
	  }
	  finishedTasks[i] = true; 
	  ++nbFinishedTasks; 
	  for(int k: revDependencies[i]) {
	    nbDep[k]--;
	    if(verbosity >= 7) 
	      cout << "      Dependent task: " << k << " remaining dependencies: " << nbDep[k] << endl;
	    if(nbDep[k] == 0){
	      onTaskPush(k, currentTime);
	      if(action != NULL) {
		action->onTaskPush(k);
	      }
	    }
	  }
	}

      for(int l = 0; l < nbWorkers; l++) 
	leaveIdle[l] = false;
      
      /* Search for an idle worker again to give priority to lower
	 indices, not necessarily to the worker that just finished. */
      idle = -1; 
      for(int j = 0; j < nbWorkers; j++) 
	if((leaveIdle[j] == false) && runningTasks[j] == -1) {
	  idle = j; 
	  break; 
	}
    }
    
  }

  delete itemManager;
  return currentTime; 

}

