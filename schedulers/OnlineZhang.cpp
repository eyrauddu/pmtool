//
// Created by eyraud on 11/12/17.
//

#include <iostream>
#include "OnlineZhang.h"

// void OnlineZhang::updateValue(double & v, const std::string &key, const AlgOptions& opt ) {
//     if(opt.isPresent(key))
//         v = opt.asDouble(key);
// }



OnlineZhang::OnlineZhang(const AlgOptions &options) : OnlineGeneric(options) {
  options.updateValue(lambda, "lambda");
  options.updateValue(beta,   "beta");
  options.updateValue(theta,  "theta");
  options.updateValue(phi,    "phi");
}

double OnlineZhang::compute(Instance &instance, SchedAction *action) {
    if(instance.nbWorkerTypes != 2) {
        std::cerr << "OnlineZhang: only for two types of ressources, please." << std::endl;
        throw(1);
    }

    largestGroup  = instance.nbWorkers[0] >= instance.nbWorkers[1]? 0 : 1;
    smallestGroup = 1 - largestGroup;
    m1 = instance.nbWorkers[largestGroup];
    m2 = instance.nbWorkers[smallestGroup];

    ins = &instance;
    avgLoad.resize(2, 0);

    return OnlineGeneric::compute(instance, action);
}

int OnlineZhang::assignTask(int task, double now) {

    double p1 = ins->execType(largestGroup, task);
    double p2 = ins->execType(smallestGroup, task);
    int target;
    double minLoadG2 = finishTimes[getEarliestWorker(smallestGroup)]; 
    // Rule 1
    if( p1 >= beta * (minLoadG2 + p2)  ) target = smallestGroup;
    // Rule 2
    else if ( p1/m1 <= theta  * (p2/m2) ) target = largestGroup;
    else if ( p1/m1 >= lambda * (p2/m2) ) target = smallestGroup;
    // Rule 3
    else if (rule3LoadG1 / m1 <= phi * rule3LoadG2 / m2) {
      target = largestGroup;
      rule3LoadG1 += p1;
    } else {
      target = smallestGroup;
      rule3LoadG2 += p2;
    }

    avgLoad[target] += ins->execType(target, task);
    return getEarliestWorker(target); 
}

