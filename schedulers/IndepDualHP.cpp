#include "IndepDP2.h"
#include "IndepDualHP.h"
#include <iostream>
#include <limits>
#include <new>
#include <algorithm>
#include <cmath>
#include "util.h"

using namespace std;


IndepDualHP::IndepDualHP(const AlgOptions& opt): IndepAllocator(opt) {
  verbosity = opt.asInt("verb_DualHP", verbosity); 
}


// Arbitrary convention: CPU times are index 0, GPU times are index 1. Just a naming thing. 

bool IndepDualHP::tryGuess(Instance& instance, std::vector<int> taskSet, vector<double> loads, double lambda, 
			   IndepResult &result) {
  
  result[0].clear(); 
  result[1].clear(); 
  vector<double> tmploads(loads); 
  
  std::vector<int> remaining; 
  // First: assign tasks that need can not go to one resource to the other.
  for(int t: taskSet) {
    if(instance.execType(0, t) > lambda) {
      if(instance.execType(1, t) > lambda) {
	if(verbosity >= 4) 
	  cout << "IndepDualHP: guess " << lambda << " infeasible because task " << t << " too long on both resources" << endl; 
	return false; 
      } else {
	result[1].push_back(t); 
	tmploads[1] += instance.execType(1, t); 
      }
    } else {
      if(instance.execType(1, t) > lambda) {
	result[0].push_back(t); 
	tmploads[0] += instance.execType(0, t); 
      } else {
	remaining.push_back(t); 
      }
    }
  }

  // For remaining tasks: sort them by acceleration factor, fill the GPU until more than lambda load.
  auto accel = [&] (int a) {
    return (instance.execType(0, a) / instance.execType(1, a)); 
  };
  auto accelCmp = [&] (int a, int b) {
    return ( accel(a) > accel(b) ); 
  }; 
  
  int nbGPU = instance.nbWorkers[1]; 
  int nbCPU = instance.nbWorkers[0]; 
  sort(remaining.begin(), remaining.end(), accelCmp); 
  auto it = remaining.begin(); 
  while(it != remaining.end() && tmploads[1] < nbGPU * lambda) {
    result[1].push_back(*it); 
    tmploads[1] += instance.execType(1, *it);
    ++it; 
  }
  
  // Then: remaining tasks go on CPU, guess is valid iff CPU load is not exceeded.
  while(it != remaining.end()) {
    result[0].push_back(*it); 
    tmploads[0] += instance.execType(0, *it); 
    ++it;
  }

  return (tmploads[0] <= nbCPU * lambda); 

}


IndepResult IndepDualHP::compute(Instance& instance, vector<int> &taskSet, vector<double> &loads) {
  if(instance.nbWorkerTypes != 2) {
    cerr << "IndepDualHP: only implemented for instances with 2 worker types" << endl; 
    throw(1); 
  }
  IndepResult result(2); // 2 because there are two resource types. 

  if(verbosity >= 4) {
    cout << "IndepDualHP: called with TS=" << taskSet << " and loads=" << loads << endl; 
    cout << "     CPU times: "; 
    for(int i : taskSet) cout << instance.execType(0, i) << " "; 
    cout << endl; 
    cout << "     GPU times: "; 
    for(int i : taskSet) cout << instance.execType(1, i) << " "; 
    cout << endl; 
    
  }
    
  
  if(taskSet.size() == 0) 
    return result; 

  double minload = min(loads[0], loads[1]); 
  loads[0] -= minload; 
  loads[1] -= minload; 
  

  double low = lowerBoundTwoResource(instance, taskSet, loads[0], loads[1]); 
  double up = std::numeric_limits<double>::infinity(); 
  if(verbosity >= 4) 
    cout << "IndepDualHP: lowerBound= " << low << endl; 
  
  // TODO: optim for the case where GPUarea <= min execution time on CPU: result = all on GPU !

  double target; 
  // Then, dichotomy, as usual
  while(abs(up - low) > epsilon*low) {
    if(up != std::numeric_limits<double>::infinity())
      target = (up + low) / 2; 
    else 
      target = 1.15*low; 
    
    if(verbosity >= 4) 
      cout << "IndepDualHP: trying " << target << endl; 
    
    if(tryGuess(instance, taskSet, loads, target, result)) {
      if(verbosity >= 4) 
	cout << "IndepDualHP: feasible " << target << endl; 
      up = target; 
    }
    else  {
      if(verbosity >= 4) 
	cout << "IndepDualHP: non feasible " << target << endl; 
      low = target; 
    }
  }
  
  if(verbosity >= 3) 
    cout << "Result of IndepDualHP: " << result << endl; 
  
   if(result[0].size() + result[1].size() != taskSet.size()) {
     cerr << "IndepDualHP: not the correct number of tasks in result !" << endl;
     throw(1); 
   }
  return result; 

}

