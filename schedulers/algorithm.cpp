#include "algorithm.h"
#include <iostream>
#include <fstream>
#ifdef WITH_CPLEX
#include "AreaBound.h"
#endif

using namespace std;

RankComputer::RankComputer(const AlgOptions & options) {
  rankOpt = options.asString("rank", "min"); 
}

bool RankComputer::isReverse(string &arg) {
  if(arg[0] == '#') {
    arg.erase(0, 1); 
    return true; 
  }
  return false; 
}

intCompare* RankComputer::oneCmp(Instance& ins, string subRankOpt) {
  vector<double> w; 
  bool reverse = isReverse(subRankOpt);

  if(subRankOpt == "heft")
    w = ins.computeHEFTRank();
  else if(subRankOpt == "min")
    w = ins.computeMinRank();
  else if(subRankOpt == "unit")
    w = ins.computeUnitRank();
#ifdef WITH_CPLEX
  else if(subRankOpt == "area") {
    vector<vector<double> >* repartition; 
    if(! ins.extraData.hasValue("area[repart]")) {
      AlgOptions o; 
      o.insert("share", "area"); 
      AreaBound bound(o); 
      ((ModifiableBound*)(& bound))->compute(ins); 
    }
    repartition = ( vector<vector<double>>*) ins.extraData.get("area[repart]");
    vector<double> wbarType(ins.nbTaskTypes, 0); 
    vector<double> wbar(ins.nbTasks, 0); 
    for(int i = 0; i < ins.nbTaskTypes; i++) 
      for(int j = 0; j < ins.nbWorkerTypes; j++) 
	if(ins.isValidValue(ins.execTimes[j][i]))
	  wbarType[i] += (*repartition)[i][j] * ins.execTimes[j][i]; 
    for(int i = 0; i < ins.nbTasks; i++) 
      wbar[i] = wbarType[ins.taskTypes[i]]; 
    w = ins.computeRanks(wbar); 
  }
#endif
  else if(subRankOpt == "none") {
    w = vector<double>(ins.nbTasks, 1); 
  }
  else if(subRankOpt.substr(0, 4) == "ext@") {
    string filename = subRankOpt.substr(4); 
    ifstream inp(filename); 
    if(!inp) {
      cerr << "Rank " << subRankOpt << " : cannot open file " << filename << endl;
      throw(1); 
    }
    w.resize(ins.nbTasks); 
    for(int i = 0; i < ins.nbTasks; i++) {
      inp >> w[i] >> ws; 
    }
    //    reverse = true;  
    // Those files give lower values to higher priority tasks
  } else if(subRankOpt.substr(0, 4) == "int@") {
    string key = subRankOpt.substr(4); 
    w =  * ((vector<double>*) ins.extraData.get(key)); 
    //    reverse = true; 
  }
  else {
    cerr << "RankComputer: unknown sub rank " << subRankOpt << endl; 
    throw(-1); 
  }
  return (new rankCompare(w, reverse));
}

TaskSet* RankComputer::makeSet(Instance& ins) {
  if(rankOpt == "fifo") {
    return (new FifoSet(ins)); 
  } else {
    intCompare* cmp; 
    if(rankOpt.substr(0, 4) == "lex@") {
      vector<string> opts = split(rankOpt.substr(4), '+'); 
      vector<intCompare*> cmps; 
      for(string & o: opts) {
	cmps.push_back(oneCmp(ins, o)); 
      }
      cmp = new lexCompare(cmps); 
    }
    else 
      cmp = oneCmp(ins, rankOpt); 
    return (new RankSet(ins, cmp)); 
  }
}

RankSet::RankSet(Instance& ins, intCompare* cmp): 
  rankCmp(cmp, false), set(rankCmp) {}

void RankSet::insert(int task) {
  auto r = set.insert(task); 
  if(!r.second) {
    cerr << "Task " << task <<" already present in set" << endl; 
    throw(-1); 
  }
}

void RankSet::erase(int task) {
  auto it = set.find(task); 
  if(it == set.end()) {
    cerr << "RankSet: task " << task << "not present in set " << set << endl; 
    throw(1); 
  }
  set.erase(it); 
}

bool RankSet::compare(int taskA, int taskB) {
  return rankCmp(taskA, taskB); 
}

int RankSet::size() {
  return set.size();
}

int RankSet::front() {
  return *(set.begin()); 
}

int RankSet::back() {
  return *(prev(set.end())); 
}

void RankSet::eraseFront() {
  if(set.empty()) {
    cerr << "RankSet: empty in eraseFront ! " << endl; 
    throw(1); 
  }
  set.erase(set.begin()); 
}

void RankSet::eraseBack() {
  if(set.empty()) {
    cerr << "RankSet: empty in eraseBack ! " << endl; 
    throw(1); 
  }
  set.erase(prev(set.end())); 
}

FifoSet::FifoSet(Instance& ins) : cnt(0), queue(), order(ins.nbTasks, -1) {}
void FifoSet::insert(int task) { 
  queue.push_front(task); 
  order[task] = ++cnt; 
}
void FifoSet::erase(int task) {
  auto it = queue.begin(); 
  while(it != queue.end() && *it != task) ++it; 
  if(it == queue.end()){
    cerr << "FifoSet: task " << task << "not present in queue. " << endl; 
    throw(1); 
  }
  queue.erase(it); 
}
bool FifoSet::compare(int taskA, int taskB) {
  return order[taskA] < order[taskB]; 
}
int FifoSet::size() { return queue.size(); }
int FifoSet::front() {return queue.front(); }
int FifoSet::back() {return queue.back(); }
void FifoSet::eraseFront() { queue.pop_front(); }
void FifoSet::eraseBack() { queue.pop_back(); }
