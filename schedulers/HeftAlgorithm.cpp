#include "HeftAlgorithm.h"
#include "util.h"
#include <limits>
#include <queue>
#include <iostream>

using namespace std;

HeftAlgorithm::HeftAlgorithm(const AlgOptions &options): ranker(options) {
  verbosity = options.asInt("verbosity", 0); 
}


static const string heftAlgName = "heft";


double HeftAlgorithm::compute(Instance& ins, SchedAction *action) {
  int nbWorkers = getSum(ins.nbWorkers); 
  int n = ins.nbTasks; 
  vector< vector<int> > revDependencies = ins.getRevDependencies(); 
  if(verbosity >= 5) {
    for(int i = 0; i < n; i++) 
      cout << revDependencies[i] << endl;
  }
  

  //rankCompare rc(rank); // NOT reversed because priority_queue returns LARGEST elements first.
  TaskSet* queue = ranker.makeSet(ins);;
  vector<AvailSequence> workerAvailability(nbWorkers); 
  vector<double> endtimesTasks(n, -1);
  vector<int> nbDep(n, 0);
  int i, j, k, t;


  for(i = 0; i < n; i++) {
    nbDep[i] = ins.dependencies[i].size();
    if(nbDep[i] == 0){
      queue->insert(i);
      if(action != NULL) action->onTaskPush(i);
    }
  }

  while(! queue->empty()) {
    i = queue->front();
    queue->eraseFront();

    if(verbosity >= 4) 
      cout << "HEFT: considering task " << i << endl; 

    double depTime = 0;
    for(int& d: ins.dependencies[i])
      depTime = std::max(depTime, endtimesTasks[d]);

    double bestTime = std::numeric_limits<double>::infinity();
    double bestStart = -1; 
    int bestK = 0; int bestT = 0; 
    k = 0;
    timeSeq::iterator bestPeriod; // Should belond to workerAvailability[bestK]
      for(t = 0; t < ins.nbWorkerTypes; t++) {
	for(j = 0; j < ins.nbWorkers[t]; j++, k++) {
	  double e = -1; double l = ins.execType(t, i); 
	  if(ins.isValidValue(l)) {
	    timeSeq::iterator p = workerAvailability[k].getAvail(depTime, l, e); 
	    if(e + l < bestTime){
	      bestK = k;
	      bestTime = e + l;
	      bestStart = e;
	      bestPeriod = p; 
	      bestT = t; 
	    }
	  }
	}
      }
      
    double startTime = bestStart; 
    if(verbosity >= 3) 
      cout << "HEFT: start task " << i << " of type " << ins.taskTypes[i] << " at " << startTime << " to end at " << bestTime << " on worker " << bestK << " of type " <<  bestT << endl;

    if(action != NULL) {
      action->onSchedule(i, bestK, startTime, bestTime);
    }
    workerAvailability[bestK].insertBusy(bestPeriod, startTime, ins.execType(bestT, i));

    endtimesTasks[i] = bestTime;
    for(int &d: revDependencies[i]) {
      nbDep[d]--;
      if(nbDep[d] == 0){
	queue->insert(d);
	if(action != NULL) action->onTaskPush(d);
      }
    }
  }




  double res = getMax(endtimesTasks); 
  return(res);

}
