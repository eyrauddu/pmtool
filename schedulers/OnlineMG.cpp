//
// Created by eyraud on 11/12/17.
//

#include <iostream>
#include <algorithm> // for max
#include "OnlineMG.h"

using namespace std; 

OnlineMG::OnlineMG(const AlgOptions& options) : OnlineGeneric(options) {
  options.updateValue(alpha, "alpha");
  options.updateValue(gamma, "gamma");
}


double OnlineMG::compute(Instance &instance, SchedAction *action) {
    if(instance.nbWorkerTypes != 2) {
        std::cerr << "OnlineMG: only for two types of ressources, please." << std::endl;
        throw(1);
    }

    largestGroup  = instance.nbWorkers[0] >= instance.nbWorkers[1]? 0 : 1;
    smallestGroup = 1 - largestGroup;
    m1 = instance.nbWorkers[largestGroup];
    m2 = instance.nbWorkers[smallestGroup];
    
    maxRTime = 0;
    sumRTime = 0; 
    
    return OnlineGeneric::compute(instance, action);
}

int OnlineMG::assignTask(int task, double now) {

    double p1 = ins->execType(largestGroup, task);
    double p2 = ins->execType(smallestGroup, task);
    int  target;
    if(p2/m2 <= gamma * (p1/m1)) {
      target = smallestGroup; 
    } else {
      double newMax = max(maxRTime, p2);
      double newSum = sumRTime + p2;
      if(max(newMax, newSum / m2) <= alpha * p1) {
	target = smallestGroup;
	maxRTime = newMax; sumRTime = newSum; 
      } else {
	target = largestGroup; 
      }
    }
    return getEarliestWorker(target); 
}

