#include "IndepBased.h"
#include "GreedyAlgorithm.h"
#include "util.h"
#include "IndepDP2.h"
#include "IndepDP3Demi.h"
#include "IndepAccel.h"
#include "IndepDualHP.h"
#include "IndepImreh.h"
#include "IndepBalanced.h"
#include "IndepZhang.h"
#include <IndepCLB2C.h>
#include <IndepMinMin.h>

#include <vector>
#include <set>
#include <string>
#include <iostream>
#include <algorithm>

#ifdef WITH_CPLEX
#include <AreaRound.h>
#endif

using namespace std;

//typedef set<int, rankCompare> rankSet;

IndepBased::IndepBased(const AlgOptions& options) : GreedyAlgorithm(options) {
  string indepName = options.asString("indep", "dualhp"); 
  if(indepName == "dp2") 
    indep = new IndepDP2(options);
  if(indepName == "dp3demi") 
    indep = new IndepDP3Demi(options);
  if(indepName == "dualhp") 
    indep = new IndepDualHP(options);
  if(indepName == "accel") {
    indep = new IndepAccel(options); 
    /*    if(style != "strict") {
      cout << "Warning: IndepAccel only valid with 'strict' style, style ignored" << endl; 
      style = "strict";
      }*/
  }
  if(indepName == "imreh") 
    indep = new IndepImreh(options); 
  if(indepName == "balest")
    indep = new IndepBalancedEst(options); 
  if(indepName == "balmks")
    indep = new IndepBalancedMkspan(options);
  if(indepName == "zhang")
    indep = new IndepZhang(options);
  if(indepName == "clb2c")
      indep = new IndepCLB2C(options);
  if(indepName == "minmin")
      indep = new IndepMinMin(options);
#ifdef WITH_CPLEX
  if(indepName == "round")
    indep = new AreaRound(options);
#endif

  if(!indep) {
    cerr << "Unknown indep algorithm " << indepName << endl; 
    throw(1); 
  }
  
}

string IndepBased::name() {
  return "indep";
}

double IndepBased::compute(Instance &instance, SchedAction* action) {
  ins = &instance; 
  allocated.resize(instance.nbWorkerTypes); 
  unassignedTasks.clear();
  hasChanged = true; 
  return GreedyAlgorithm::compute(instance, action); 
}

void IndepBased::allocate(double now) {
  for(auto&& type : allocated) {
    unassignedTasks.insert(unassignedTasks.end(), type.begin(), type.end()); 
    type.clear();
  }

  vector<double> scheduledLoad(ins->nbWorkerTypes, 0); 
  if(indep->needsPreciseLoads) {
    scheduledLoad.resize(ins->totalWorkers, 0); 
    int j = 0; 
    for(int t = 0; t < ins->nbWorkerTypes; t++) {
      for(int k = 0; k < ins->nbWorkers[t]; k++, j++) {
	scheduledLoad[j] = std::max(0.0, endTimesWorkers[j] - now); 
      }
    }
  } else {
    int j = 0; 
    for(int t = 0; t < ins->nbWorkerTypes; t++) {
      for(int k = 0; k < ins->nbWorkers[t]; k++, j++) {
	scheduledLoad[t] += std::max(0.0, endTimesWorkers[j] - now); 
      }
    }
  }

  if(verbosity >= 1) {
    cout << "Indep: allocating at time " << now << endl;
    if(verbosity >= 3)
        cout << "Indep: calling with TS=" << unassignedTasks << " and loads=" << scheduledLoad << endl;
  }
  IndepResult result = indep->compute(*ins, unassignedTasks, scheduledLoad);
  if(verbosity >= 3)
    cout << "Result of Allocation: " << result << endl;
  for(int i = 0; i < ins->nbWorkerTypes; i++) {
    allocated[i].swap(result[i]); 
  }

  unassignedTasks.clear(); 
  hasChanged = false; 
}

void IndepBased::onTaskPush(int task, double now) {
  unassignedTasks.push_back(task); 
  hasChanged = true; 
}

void IndepBased::chosenTask(int task, int wType, IndepOneResult::iterator it) {
  if(*it != task) {
    cerr << "Indep2: inconsistency in chosenTask: " << task << " vs " << *it << endl; 
    throw(-1); 
  }
  allocated[wType].erase(it); 
}

int IndepBased::chooseTask(int worker, double now) {
  if(hasChanged) maybeAllocate(now); 
  int wType = ins->getType(worker); 
  if(verbosity >= 5) {
    cout << "Choosing task for " << worker << ", allocated = " << allocated[wType] << endl;
    cout << "  now = " << now << ", ETW =  " << endTimesWorkers << endl; 
  }
  auto taskIt = allocated[wType].begin(); 
  /*    if((taskIt == allocated[wType].end()) && hasChanged) {
  // Worker type wType is idle, call alloc->compute
  allocate(now); 
  taskIt = allocated[wType].begin(); 
  }*/
  if(taskIt == allocated[wType].end()) return -1; 
  int task = *taskIt;
  chosenTask(task, wType, taskIt); 
  return task; 
}


class IndepBasedRank: public IndepBased {
protected:
  RankComputer ranker; 
  TaskSet* set; 
  bool dosort = true; 
public: IndepBasedRank(const AlgOptions& options): 
  IndepBased(options), ranker(options), dosort(options.asString("dosort", "yes") != "no") { }
  virtual void allocate (double now) {
    IndepBased::allocate(now); 
    if(dosort) {
      for(int i = 0; i < ins->nbWorkerTypes; i++) {
	// allocated[i].sort([&] (int a, int b) { return set->compare(a, b); } ); 
	sort(allocated[i].begin(), allocated[i].end(),
	     [&] (int a, int b) { return set->compare(a, b); } ); 
      }
    }
  }
  virtual double compute(Instance &instance, SchedAction* action) {
    set = ranker.makeSet(instance); 
    return IndepBased::compute(instance, action); 
  }
  virtual void onTaskPush(int task) {
    set->insert(task);
      IndepBased::onTaskPush(task, 0);
  }
};

  
class IndepBasedIdle: public IndepBasedRank {
public:
  IndepBasedIdle(const AlgOptions &options): IndepBasedRank(options) {
  }
  virtual void maybeAllocate(double now) {
    // Call allocate if at least one worker is idle
    int j; 
    for(j = 0; j < ins->totalWorkers; j++) {
      if((runningTasks[j] == -1) && 
	(leaveIdle[j] == false) && 
	(allocated[ins->getType(j)].empty()) )
       break;
   }
   if(j < ins->totalWorkers)
     allocate(now); 
  }
};

class IndepBasedProgress : public IndepBasedRank {
protected: 
  double proportion; 
  int nbAssignedTasks; 
  int nbScheduledTasks; 
public:
  IndepBasedProgress(const AlgOptions &options): IndepBasedRank(options) {
    proportion = options.asDouble("proportion", 0.0);
  }
  virtual double compute(Instance &ins, SchedAction* action) {
    nbScheduledTasks = 0;
    nbAssignedTasks = 0;
    return IndepBasedRank::compute(ins, action); 
  }
  virtual void allocate(double now) {
    IndepBasedRank::allocate(now); 
    nbScheduledTasks = 0; 
    nbAssignedTasks = 0; 
    for(int i = 0; i < ins->nbWorkerTypes; i++)
      nbAssignedTasks += allocated[i].size();     
  }
  virtual void maybeAllocate(double now) {
    if(verbosity >= 6)
      cerr << "IBProgress: allocate ? scheduled: " << nbScheduledTasks
	   << " assigned: " << nbAssignedTasks
	   << " proportion: " << proportion << endl; 
	
    if(nbScheduledTasks >= nbAssignedTasks * proportion){
      allocate(now); 
    }
  }  
  virtual void chosenTask(int task, int wType, IndepOneResult::iterator it) {
    IndepBasedRank::chosenTask(task, wType, it); 
    nbScheduledTasks++; 
  }
};

class IndepBasedLevelStrict: public IndepBased {
protected: 
  bool do_sort; 
public:
  IndepBasedLevelStrict(const AlgOptions& options) : 
    IndepBased(options), do_sort(!options.isPresent("nosort")) {}
  virtual void maybeAllocate(double now) {
    bool allEmpty = true; 
    for(auto && v: allocated) 
      if(!v.empty()) {
	allEmpty = false; break; 
      }
    if(allEmpty) {
      for(auto && v : endTimesWorkers)
	if(v > now) {
	  allEmpty = false; break; 
	}
    }
    if(allEmpty) 
      allocate(now); 
  }
  virtual void allocate(double now) {
    IndepBased::allocate(now); 
    if(do_sort) {
      for(int i = 0; i < ins->nbWorkerTypes; i++) 
	// allocated[i].sort([&](int a, int b) { 
	//     return ins->execType(i, a) > ins->execType(i, b); 
	//   }); 
	sort(allocated[i].begin(), allocated[i].end(), [&](int a, int b) { 
	    return ins->execType(i, a) > ins->execType(i, b); 
	  }); 
    }
  }
};

class IndepBasedLevel: public IndepBasedRank {
public:
  IndepBasedLevel(const AlgOptions& options) : IndepBasedRank(options) {}
  virtual void maybeAllocate(double now) {
    bool allEmpty = true; 
    for(auto && v: allocated) {
      if(!v.empty()) {
	allEmpty = false; 
	break; 
      }
    }
    if(allEmpty) 
      allocate(now); 
  }
};



IndepBased* makeIndep(const AlgOptions& options) {
  string style = options.asString("style", "onidle"); 
  if(style == "onidle") 
    return(new IndepBasedIdle(options));
  if(style == "onprogress")
    return(new IndepBasedProgress(options));
  if(style == "strict") 
    return(new IndepBasedLevelStrict(options)); 
  if(style == "level")
    return(new IndepBasedLevel(options)); 

  cerr << "Indep2: Unknown style " << style << endl; 
  throw(1);  
}
