#include "IndepDP2.h"
#include <iostream>
#include <limits>
#include <new>
#include <algorithm>
#include <cmath>
#include "util.h"

#include "IndepAccel.h"

using namespace std;

IndepAccel::IndepAccel(const AlgOptions& opt): IndepAllocator(opt) {
  verbosity = opt.asInt("verb_DP2", verbosity); 
}

//Limitation: CPU times should be index 0, GPU times index 1. TODO generalize at some point
const int IA_CPU = 0; 
const int IA_GPU = 1; 

double gpuTime(Instance& ins, int t) {
  return ins.execType(IA_GPU, t); 
}
double cpuTime(Instance& ins, int t) {
  return ins.execType(IA_CPU, t);
}

bool IndepAccel::tryGuess(Instance& ins, std::vector<int> taskSet, double target, IndepResult& result)  {

  if(verbosity >= 6) 
    cout << "IndepAccel, target = " << target << endl; 

  // Sets G* go on GPU, sets Cµ* go on CPU
  // Sets "1" contain long tasks than may disturb the schedule, we
  // should have at most m (or k) of them
  // Sets "2" are the short ones
  vector<int> setG1, setG2, setC1, setC2; 
  for(auto t: taskSet) {
    if(ins.execType(IA_CPU, t) <= target)
      setC2.push_back(t); 
    else 
      setG1.push_back(t); 
  }

    auto surfaceCmp = [&] (int a, int b) {
    return ( (cpuTime(ins, a) - gpuTime(ins, a)) > 
	     (cpuTime(ins, b) - gpuTime(ins, b))    );
  }; 
  sort(setC2.begin(), setC2.end(), surfaceCmp); 

  // Tasks with CPU time > target can not go on CPU, they go at the end of G1,
  ///   and are sorted by decreasing GPU time. Line 131 is where we forbid them to move
  
  sort(setG1.begin(), setG1.end(), [&] (int a, int b) {
      if(cpuTime(ins, a) > target) {
	if(cpuTime(ins, b) > target) {
	  return gpuTime(ins, a) > gpuTime(ins, b); 
	} else {
	  return false; 
	}
      } else if(cpuTime(ins, b) > target) {
	return true; 
      } else {
	return surfaceCmp(a, b); 
      }
    }); 
  if(verbosity >= 7) 
    cout << "IndepAccel: start. C1; " << setC1 << " C2: " << setC2 << " G1: " << setG1 << " G2: " << setG2 << endl;

  // Tasks in G1 with GPU time small enough go to G2: they do not disturb the schedule
  // We find them at the end of G1 because of how we sorted G1. 
  if(!setG1.empty()) {
    auto it = setG1.end() - 1; 
    while (gpuTime(ins, *it) < target / 2.0) {
      setG2.push_back(*it); 
      if(it == setG1.begin()) break; 
      it--; 
    }
    
    if(gpuTime(ins, *it) >= target / 2.0) it++; 
    setG1.erase(it, setG1.end());
  }

  if(((int) setG1.size()) > ins.nbWorkers[IA_CPU] + ins.nbWorkers[IA_GPU]) {
    if(verbosity >= 6)
      cout << "IndepAccel: G1 set too big, not feasible. "<< endl; 
    return false; 
  }
  


  int nbCPU = ins.nbWorkers[IA_CPU], nbGPU = ins.nbWorkers[IA_GPU]; 
  double CPUload = 0, GPUload = 0; 
  for(int i: setG1) 
    GPUload += gpuTime(ins, i);
  for(int i: setG2)
    GPUload += gpuTime(ins, i);
  for(int i: setC2) 
    CPUload += cpuTime(ins, i); 
  
  if(verbosity >= 7) 
    cout << "IndepAccel: start. CPUload= " << CPUload << " C1; " << setC1 << " C2: " << setC2 << " G1: " << setG1 << " G2: " << setG2 << endl;

  auto iterC2 = setC2.begin(); 
  auto iterG1 = setG1.begin(); 

  int iterations = 0; 


  while( (iterations <= nbCPU) && ((GPUload > nbGPU * target + target/2.0) || (CPUload > nbCPU * target + target/2.0)) ) {
    if(verbosity >= 7) {
    cout << "IndepAccel: iteration "<< iterations << "CPUload= " << CPUload << " GPUload= " << GPUload << endl;
    cout << "           C1: " << setC1 << endl;
    cout << "           C2: " << setC2 << endl;
    cout << "           G1: " << setG1 << endl;
    cout << "           G2: " << setG2 << endl;
    }
    bool changed = false;
    while ((GPUload <= nbGPU * target) && (iterC2 != setC2.end())) {
      setG2.push_back(*iterC2); 
      GPUload += gpuTime(ins, *iterC2); 
      CPUload -= cpuTime(ins, *iterC2); 
      iterC2++;
      changed = true; 
    }
    // Here is where we forbid tasks which were moved at the end of G1 to be reassigned
    if( (((int) setC1.size()) < nbCPU) && (iterG1 != setG1.end()) && (cpuTime(ins, *iterG1) <= target) ) {
      setC1.push_back(*iterG1); 
      GPUload -= gpuTime(ins, *iterG1); 
      CPUload += cpuTime(ins, *iterG1); 
      iterG1++;
      changed = true; 
    }
    if(!changed)
      break;
    iterations++; 
  }
  
  if(CPUload > nbCPU * target + target/2.0) {
    if(verbosity >= 6) 
      cout << "IndepAccel: CPU load too big " << CPUload << " C1; " << setC1 << " C2: " << setC2 << " G1: " << setG1 << " G2: " << setG2 << endl;
    return false; 
  }

  if((int) (setG1.end() - iterG1) > nbGPU) {
    if(verbosity >= 6) 
      cout << "IndepAccel: Set G1 too big " << CPUload << " C1; " << setC1 << " C2: " << setC2 << " G1: " << setG1 << " G2: " << setG2 << "" << endl;
    return false; 
  }

  result[0].clear();
  result[1].clear(); 

  result[0].insert(result[0].end(), setC1.begin(), setC1.end()); 
  result[0].insert(result[0].end(), iterC2, setC2.end()); 
  result[1].insert(result[1].end(), iterG1, setG1.end()); 
  result[1].insert(result[1].end(), setG2.begin(), setG2.end()); 
  
  if(verbosity>= 6) 
    cout << "IndepAccel: feasible, result = " << result << endl; 

  return true; 
}


IndepResult IndepAccel::compute(Instance& instance, vector<int> &taskSet, vector<double> &loads) {

  if(instance.nbWorkerTypes != 2) {
    cerr << "IndepAccel: only implemented for instances with 2 worker types" << endl; 
    throw(1); 
  }
  if(loads[0] != 0 || loads[1] != 0) {
    cerr << "IndepAccel: only implemented for 0 previous load" << endl; 
    throw(1); 
  }

  if(verbosity >= 4) {
    cout << "IndepAccel: called with TS=" << taskSet << " and loads=" << loads << endl; 
    cout << "     CPU times: "; 
    for(int i : taskSet) cout << instance.execType(0, i) << " "; 
    cout << endl; 
    cout << "     GPU times: "; 
    for(int i : taskSet) cout << instance.execType(1, i) << " "; 
    cout << endl; 
    
  }


  IndepResult result(2);
  if(taskSet.size() == 0) 
    return result; 
  

  double low = lowerBoundTwoResource(instance, taskSet); 
  double up = std::numeric_limits<double>::infinity(); 

  if(verbosity >= 6)
    cerr << "IndepAccel: Lower bound is " << low << endl;
  
 double target; 
   while(abs(up - low) > epsilon*low) {
    if(up != std::numeric_limits<double>::infinity())
      target = (up + low) / 2; 
    else 
      target = 1.15*low; 
    if(tryGuess(instance, taskSet, target, result)) {
      up = target; 
    } else {
      low = target; 
    }
   }
   
   if(verbosity >= 4) 
     cout << "Result of IndepAccel: " << result << endl; 
   if(result[0].size() + result[1].size() != taskSet.size()) {
     cerr << "IndepAccel: not the correct number of tasks in result !" << endl;
     throw(1); 
   }
     


   return result; 
}
