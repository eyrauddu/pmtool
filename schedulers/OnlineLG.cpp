//
// Created by eyraud on 11/12/17.
//

#include <iostream>
#include "OnlineLG.h"

OnlineLG::OnlineLG(const AlgOptions& options) : OnlineGeneric(options) {
  
}


double OnlineLG::compute(Instance &instance, SchedAction *action) {
    if(instance.nbWorkerTypes != 2) {
        std::cerr << "OnlineLG: only for two types of ressources, please." << std::endl;
        throw(1);
    }
    return OnlineGeneric::compute(instance, action);
}

int OnlineLG::assignTask(int task, double now) {

    double p1 = ins->execType(0, task);
    double p2 = ins->execType(1, task);
    int target = p1/ins->nbWorkers[0] <= p2/ins->nbWorkers[1] ? 0 : 1;
    return getEarliestWorker(target); 
}

