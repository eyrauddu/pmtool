//
// Created by eyraud on 25/07/18.
//

#include <iostream>
#include <limits>
#include "TrueHeteroPrio.h"
using namespace std;


bool TrueHeteroPrio::isBetterSteal(int taskA, double endA, int taskB, double endB, int wType) {
  double scoreA, scoreB;
  if(stealType == AF) {
    scoreA = ins->execType(1-wType, taskA) / ins->execType(wType, taskA);
    scoreB = ins->execType(1-wType, taskB) / ins->execType(wType, taskB);
    if (scoreA == scoreB) {
      scoreA = priorities[taskA];
      scoreB = priorities[taskB];
    }
  }
  else if (stealType == LATEST) {
    scoreA = endA;
    scoreB = endB;
  }
  else { /* Steal by priority */
    scoreA = priorities[taskA];
    scoreB = priorities[taskB];
  }
  return scoreA > scoreB; 
}

int TrueHeteroPrio::chooseTask(int worker, double now) {
    int wType = ins->getType(worker);

    bool chosenFromQueue = false; 
    int resultTask = -1; 
    auto positionInQueue = taskSet->begin();
    if(! taskSet->empty()) {
      if (wType == 0) {
	positionInQueue = taskSet->end();
	--positionInQueue;
      }
      chosenFromQueue = true; 
      resultTask = *positionInQueue;
    }

    bool unfavorable = false;
    if (resultTask != -1)
      unfavorable = ins->execType(wType, resultTask) > ins->execType(1-wType, resultTask); 
    
    bool otherTypeHasAnIdleProc = false; 
    int chosenVictim = -1;
    if(resultTask == -1 || unfavorable) {
        int bestTask = -1;

        // Steal: copy/paste from GreedyPerType
	int victim = wType == 1 ? 0 : ins->nbWorkers[0];
	for(int i = 0; i < ins->nbWorkers[1-wType]; ++i,++victim){
	  int& task = runningTasks[victim]; 
	  if(task < 0)
            otherTypeHasAnIdleProc = true;
	  else if(ins->isValidType(wType, task)
		  && now + ins->execType(wType, task) < endTimesWorkers[victim]) {
	    if(bestTask == -1 || isBetterSteal(task, endTimesWorkers[victim], bestTask, endTimesWorkers[chosenVictim], wType)) {
	      bestTask = task;
	      chosenVictim = victim;
	    }
	  }	    
	}

	if(bestTask != -1) {
	  resultTask = bestTask;
	  chosenFromQueue = false;
	  unfavorable = false; 
	}
    }


    if(resultTask == -1)
      return -1; 

    if(otherTypeHasAnIdleProc && unfavorable) 
      // Stay idle if another processor could do better than me
      return -1;
    
    if(chosenFromQueue)
      taskSet->erase(positionInQueue);
    else if(chosenVictim != -1)
      runningTasks[chosenVictim] = -1;

    return resultTask; 
}

void TrueHeteroPrio::onTaskPush(int task, double now) {
    taskSet->insert(task);
}

double TrueHeteroPrio::compute(Instance &ins, SchedAction *action) {
    if(ins.nbWorkerTypes != 2) {
        cerr << "TrueHeteroPrio: only works with two worker types, not " << ins.nbWorkerTypes << endl;
        throw(1);
    }
    vector<double> accelFactors(ins.nbTasks);
    for(int i = 0; i < ins.nbTasks; i++)
        accelFactors[i] = ins.execType(0, i) / ins.execType(1, i);
    rankCompare compareByAccelFactor(accelFactors, true);
    if(rankString == "heft")
      priorities = ins.computeHEFTRank();
    else if(rankString == "min")
      priorities = ins.computeMinRank();
    else if(rankString == "unit")
      priorities = ins.computeUnitRank();
    else if(rankString == "none") {
      priorities = vector<double>(ins.nbTasks, 1); 
    } else {
      cerr << "TrueHP: Unknown rank " << rankString << endl;
      throw(1); 
    }
    rankCompare compareByPriorities(priorities, true);
    vector<intCompare*> list;
    list.push_back(&compareByAccelFactor);
    list.push_back(&compareByPriorities);
    lexCompare combined(list);
    strictCompare strict(&combined, true);
    taskSet = new set<int, strictCompare>(strict);
    this->ins = &ins;
    
    return GreedyAlgorithm::compute(ins, action);
}

TrueHeteroPrio::TrueHeteroPrio(const AlgOptions &options) : GreedyAlgorithm(options) {
    string steal = options.asString("steal", "latest");
    if(steal == "latest")
        stealType = LATEST;
    else if(steal == "prio")
        stealType = PRIO;
    else if(steal == "af")
        stealType = AF;
    else {
       cerr << "TrueHP: unknown steal type " << steal << ", defaulting to latest" << endl;
       stealType = LATEST;
    }
    rankString = options.asString("rank", "min");
    
}

