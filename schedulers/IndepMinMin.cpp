
// Created by eyraud on 25/03/19

#include "IndepMinMin.h"
#include <iostream>
#include <set>
#include "util.h"

using namespace std; 

IndepMinMin::IndepMinMin(const AlgOptions& options) : IndepAllocator(options) {
  needsPreciseLoads = true; 
}

int IndepMinMin::getEarliestWorker(vector<double> &loads, int type) {
  int best = workerIndices[type];
  int to = workerIndices[type+1]; 
    for(int i = workerIndices[type]; i < to; ++i){
        if(loads[i] < loads[best])
            best = i;
    }
    return best;
}

double IndepMinMin::endTime(vector<double> &loads, int workerType, int task) {
  return loads[bestWorkers[workerType]] + ins->execType(workerType, task); 
}

IndepResult IndepMinMin::compute(Instance & ins, vector<int> &taskSet, vector<double> &loads) {

  this->ins = & ins; 

  workerIndices.resize(ins.nbWorkerTypes + 1);
  bestWorkers.resize(ins.nbWorkerTypes); 
  workerIndices[0] = 0; 
  for(int i = 0; i < ins.nbWorkerTypes; ++i) {
    workerIndices[i+1] = workerIndices[i] + ins.nbWorkers[i];
    bestWorkers[i] = getEarliestWorker(loads, i);
  }
  
  IndepResult result(ins.nbWorkerTypes);

  set<int> tasks(taskSet.begin(), taskSet.end()); 
  int nbTasks = taskSet.size(); 

  while(nbTasks > 0) {
    int bestWorkerType = -1;
    int bestTask = -1;
    for(int  t: tasks) {
      int bestWorkerTypeForThisTask = 0; 
      for(int j = 1; j < ins.nbWorkerTypes; ++j) {
	if(endTime(loads, j, t) < endTime(loads, bestWorkerTypeForThisTask, t))
	  bestWorkerTypeForThisTask = j;
      }
      if(bestTask < 0 ||
	 endTime(loads, bestWorkerTypeForThisTask, t) < endTime(loads, bestWorkerType, bestTask)) {
	bestTask = t;
	bestWorkerType = bestWorkerTypeForThisTask; 
      }
    }

    result[bestWorkerType].push_back(bestTask);
    loads[bestWorkers[bestWorkerType]] += ins.execType(bestWorkerType, bestTask);
    bestWorkers[bestWorkerType] = getEarliestWorker(loads, bestWorkerType);
    
    nbTasks -= 1; 
    tasks.erase(bestTask); 
    
  }
  
  return(result); 
}


// Possibilities
//   As an IndepAllocator, close to CLB2C
//
//   O(n^2m') m' = # worker types
//   Until done: O(n)
//     find best worker for each type of worker O(m)
//     Find smallest ready task for each type of worker O(nm')   =>O(m'(1+update))
//     Assign.
//       Keep ordered sets of tasks, one for each worker type ? Updates seem costly, but worst case is still better
//       Keep best worker for each type ? Avoids recomputation. 


// What about MaxMin ? 
