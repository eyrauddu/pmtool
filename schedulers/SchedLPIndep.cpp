//
// Created by eyraud on 01/06/18.
//

#include <ilcplex/ilocplex.h>
#include "SchedLPIndep.h"

using namespace std;

SchedLPIndep::SchedLPIndep(AlgOptions opt) : GreedyAlgorithm(opt) {
    limit = opt.asDouble("limit", 0);
    gap = opt.asDouble("gap", -1);
}

double SchedLPIndep::compute(Instance &ins, SchedAction *action) {

    // Compute Pre Assign
    vector<int> countTasks(ins.nbTaskTypes, 0);
    for(auto & t: ins.taskTypes) countTasks[t] ++;
    IloEnv env;
    IloModel model(env);

    IloNumVar load(env, 0.0, IloInfinity, ILOFLOAT);
    IloArray<IloNumVarArray> nbTaskonWorker(env, ins.nbTaskTypes);

    // Quantity constraints
    for(int i = 0; i < ins.nbTaskTypes; i++) {
        nbTaskonWorker[i] = IloNumVarArray(env, ins.totalWorkers, 0.0, IloInfinity, ILOINT);
        model.add(IloSum(nbTaskonWorker[i]) == countTasks[i]);
    }
    // Load constraints
    int workerIndex = 0;
    for(int wType = 0; wType < ins.nbWorkerTypes; wType++) {
        for (int i = 0; i < ins.nbWorkers[wType]; i++) {
            IloExpr e(env);
            for (int taskType = 0; taskType < ins.nbTaskTypes; taskType++) {
                double v = ins.execTimes[wType][taskType];
                if (ins.isValidValue(v)) {
                    e += nbTaskonWorker[taskType][workerIndex] * v;
                } else {
                    model.add(nbTaskonWorker[taskType][workerIndex] == 0);
                }
            }
            model.add(e <= load);
            e.end();
            workerIndex++;
        }
    }
    // objective function & param
    model.add(IloMinimize(env, load));

    IloCplex solver(model);
    if(verbosity <= 3)
        solver.setOut(env.getNullStream());
    if(limit > 0)
        solver.setParam(IloCplex::TiLim, limit);
    if(gap > 0)
        solver.setParam(IloCplex::EpGap, gap);

    IloBool b = solver.solve();
    if(verbosity >= 5) {
        cerr << "SchedLPIndep solve status: " << b << " " << solver.getStatus() << " " << solver.getCplexStatus() << endl;
    }
    double bestBound = solver.getBestObjValue();
    if(verbosity >= 1) {
        double intSol = solver.getObjValue();
        cerr << "SchedLPIndep: integer solution: " << intSol << " best bound: " << bestBound<< " gap:" << (intSol - bestBound)/bestBound << endl;
    }


    vector<vector<int > > result(ins.nbTaskTypes, vector<int>(ins.totalWorkers, -1));
    for (int i = 0; i < ins.nbTaskTypes; i++) {
        IloNumArray values(env);
        solver.getValues(values, nbTaskonWorker[i]);
        for(int j = 0; j < ins.totalWorkers; j++)
            result[i][j] = static_cast<int>(round(values[j]));
        if(verbosity >= 2) {
            cout << "Indep: values for task types " << i << ": " << result[i] << endl;
        }
    }
    preAssign.resize(ins.totalWorkers, vector<int>());
    for(int task = 0; task < ins.nbTasks; task++) {
        bool found = false;
        for(int worker = 0; worker < ins.totalWorkers; worker++) {
            if (result[ins.taskTypes[task]][worker] > 0) {
                preAssign[worker].push_back(task);
                result[ins.taskTypes[task]][worker]--;
                found = true;
                break;
            }
        }
        if(!found) {
            cerr << "SchedLPIndep: could not find assignment for task " << task << endl;
            throw(1);
        }

    }

    return GreedyAlgorithm::compute(ins, action);
}

int SchedLPIndep::chooseTask(int worker, double now) {
//    cout << "SchedLPIndep: worker " << worker << " idle at time " << now << endl;
    if(preAssign[worker].empty()) return -1;
    int result = preAssign[worker].back();
    if(result < 0) {
        cout << "SchedLPIndep: wrong task number " << result << endl;
        throw(1);
    }
    preAssign[worker].pop_back();
    return result;
}

void SchedLPIndep::onTaskPush(int task, double now) {
}
