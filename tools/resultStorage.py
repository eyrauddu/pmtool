import csv
import sys
from collections import namedtuple, OrderedDict
import subprocess
import shutil
from datetime import datetime
import os
import pickle



dateformat = "%Y-%m-%dT%H:%M:%S"

algorithmFields = ["isBound", "alg", "params"]
resultFields = ["mkspan", "time", "date"]
Algorithm = namedtuple("Algorithm", algorithmFields)
Result = namedtuple("Result", resultFields)

def _makeAlgorithm(row):
    return Algorithm(**{k:row[k] for k in algorithmFields})

def _makeResult(row):
    return Result(**{k:row[k] for k in resultFields})

def makeAlgorithm(name, isBound=False, **args):
    params = OrderedDict(**{k:v for (k, v) in sorted(args.items(), key=lambda c: c[0]) })
    return Algorithm(alg = name, isBound = isBound, params = ":".join('='.join(t) for t in p.items()))

def makeAlgorithms(name, isBound=False, **args):
    params = [{}]
    for (k,v) in sorted(args.items(), key=lambda c: c[0]):
        params = [OrderedDict(d, **{k:x}) for x in v for d in params]

    return [ Algorithm(alg = name, isBound = isBound, params = ":".join('='.join(t) for t in p.items())) for p in params ]
    
def MakeStorage(instanceParameters, getInstanceFile, getPlatformFile, commandArgs):
    class Storage: 
        instanceFields = ["platform"] + instanceParameters
        questionFields = instanceFields + algorithmFields
        recordFields = instanceFields + algorithmFields + resultFields

        Instance  = namedtuple("Instance", instanceFields)
        Question  = namedtuple("Question", questionFields)
        Record = namedtuple("Record", recordFields)

        @classmethod
        def _makeInstance(cls, row):
            return cls.Instance(**{k:row[k] for k in cls.instanceFields})
        @classmethod
        def _makeQuestion(cls, row): 
            return cls.Question(**{k:row[k] for k in cls.questionFields})
        @classmethod
        def _makeRecord(cls, row): 
            return cls.Record(**{k:row[k] for k in cls.recordFields})

        @classmethod
        def makeInstances(cls, **kwargs):
            result = [{}]
            for k,v in kwargs.items():
                result = [dict(d, **{k:x}) for x in v for d in result]
                
            return list(map(cls._makeInstance, result))

        @classmethod
        def allCombinations(cls, instances, algorithms):
            return (cls.Question(**i._asdict(), **a._asdict()) for i in instances for a in algorithms)


        
        @classmethod
        def convertRow(cls, row):
            row["date"] = datetime.strptime(row["date"], dateformat)
            row["isBound"] = True if row["isBound"] == "True" else False

        @classmethod
        def parseRow(cls, row):
            cls.convertRow(row)
            return (cls._makeQuestion(row), _makeResult(row))

        @classmethod
        def parseRowToRecord(cls, row):
            cls.convertRow(row)
            return cls._makeRecord(row)


        def __init__(self, filename):
            self.filename = filename

        @classmethod
        def getFilename(cls, i):
            return getInstanceFile(i)

        def readFile(self):
            allData = {}
            try: 
                with open(self.filename, 'r') as fd:
                    for row in csv.DictReader(fd, fieldnames = self.recordFields):
                        (question, result) = self.parseRow(row)
                        if question in allData: 
                            allData[question].append(result)
                        else:
                            allData[question] = [result]
            except OSError:
                pass
            return allData

        def readRecords(self):
            with open(self.filename, 'r') as fd:
                for row in csv.DictReader(fd, fieldnames = self.recordFields):
                    record = self.parseRowToRecord(row)
                    yield record

        def _writeRecord(self, csvWriter, record):
            d = record._asdict()
            d["date"] = d["date"].strftime(dateformat)
            csvWriter.writerow(d)

        def writeRecords(self, results, overwrite=False):
            with open(self.filename, 'w' if overwrite else 'a') as fd:
                w = csv.DictWriter(fd, fieldnames = self.recordFields)
                for r in results:
                    self._writeRecord(w, r)

        @classmethod
        def hasUpToDateAnswer(cls, q, data):
            if q in data:
                lastAnswer = max(data[q], key=lambda r:r.date)
                src = cls.getFilename(q)
                try: 
                    srcTime = datetime.fromtimestamp(os.path.getmtime(src))
                except OSError as e:
                    print("Instance source file %s is not readable: %s\n" % (src, e), file=sys.stderr)
                    exit
                return lastAnswer.date >= srcTime
            else:
                return False

        @classmethod
        def hasValidAnswer(cls, q, data):
            return q in data

        def updateFile(self, questions, pred = lambda q, data: not Storage.hasValidAnswer(q, data)):
            data = self.readFile()
            toRun = [ q for q in questions if pred(q, data)]
            results = self.runMany(toRun)

        # Keep only the last answer for each question
        def cleanupFile(self, toBeRemoved = lambda x: False):
            data = self.readFile()
            toWrite = [self.Record(*q, *max(data[q], key=lambda r:r.date)) for q in data ]
            self.writeRecords((r for r in toWrite if not toBeRemoved(r)), overwrite=True)

        def runMany(self, questions):
            separated = [ (self._makeInstance(q._asdict()), _makeAlgorithm(q._asdict())) for q in questions ]

            ## Was a good idea, but pmtool has too many memory problems to open so many instances at once.
            ## Get list of instances
            # allInstances = set(i for (i,a) in separated)
            ## Group instances which require the same algorithms
            # instancesPerAlgList = {}
            # for i in allInstances:
            #     algs = tuple(a for (j, a) in separated if i == j)
            #     if algs in instancesPerAlgList:
            #         instancesPerAlgList[algs].append(i)
            #     else:
            #         instancesPerAlgList[algs] = [i]

            # results = results + sum((self.runOne(instances, algs) for (algs, instances) in instancesPerAlgList.items()), [])

            groupedByInstances = {}
            for (i, a) in separated:
                if i in groupedByInstances:
                    groupedByInstances[i].append(a)
                else:
                    groupedByInstances[i] = [a]

            nbInstances = len(groupedByInstances)
            results = sum( (self.runOne(i, algs, progress = (1+index)/nbInstances)
                            for (index, (i, algs)) in enumerate(groupedByInstances.items())), [])
            
            return results


        def runOne(self, instance, algs, progress=None):
            with open(self.filename, 'a') as output:

                writer = csv.DictWriter(output, fieldnames = self.recordFields)

                filename = self.getFilename(instance)
                args = []
                args.extend(commandArgs)
                args.extend(["--output-raw", "--no-header"])
                args.extend(['-p', getPlatformFile(instance.platform)])
                args.append(filename)
                args.extend(sum([[("-b" if a.isBound else "-a"),
                                  a.alg+(":"+a.params if a.params else "")] for a in algs], []))
                print("Running: " + (" [%.1f%%] " % (progress*100) if progress else "") + " ".join(args))
                with subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True) as child:
                    reader = csv.reader(child.stdout, delimiter=' ')
                    result = []
                    for row in reader:
                        algNameAndParams=row[3].split(":", maxsplit=1)
                        assert row[0] == filename
                        rec = self.Record(*list(instance), True if row[2] == 'True' else False, algNameAndParams[0],
                                          algNameAndParams[1] if len(algNameAndParams) >= 2 else "",
                                          row[4], row[5], datetime.now().strftime(dateformat))
                        writer.writerow(rec._asdict())
                        output.flush()
                        result.append(rec)
                    returncode = child.wait()
                    if returncode != 0:
                        print("Wrong return code %d for command:\n  %s\n" % (returncode, " ".join(args)), file=sys.stderr)
                        print(child.stderr, file=sys.stderr)
                        raise Exception
            return result


        def printRecords(self, records, file=""):
            if file:
                fd = open(file, "w")
            else:
                fd = sys.stdout
            recordsWithParamDicts = [ r._replace(params=OrderedDict(eq.split('=') for eq in r.params.split(":")) if r.params else {}) for r in records ]
            allParams = frozenset.union(*(frozenset(r.params.keys()) for r in recordsWithParamDicts))
            print(" ".join(instanceParameters),
                  "platform isBound algorithm",
                  " ".join(allParams), "makespan time date", file=fd)
            for r in recordsWithParamDicts:
                print(" ".join(getattr(r, p) if getattr(r, p) else "NA" for p in instanceParameters),
                      "%s" % (r.platform),
                      "%s %s" % (r.isBound, r.alg),
                      " ".join(r.params[k] if k in r.params else "NA" for k in allParams),
                      "%s %s %s" % (r.mkspan, r.time, r.date.strftime(dateformat)), file = fd)

        def displayFileContents(self, pred = lambda x: True, **kwargs):
            data = self.readRecords()
            self.printRecords(filter(pred, data), **kwargs)

        def getRecordsFromDatFile(self, file, questions):
            knownFields = [("m", "m"), ("k", "k"), ("isBound", "isBound"),
                           ("algorithm", "alg")] + [(p, p) for p in instanceParameters]
            resultFields = [ ("makespan", "mkspan"), ("time", "time"), ("date", "date") ]
            newQuestions = [(q._replace(params=dict(eq.split('=') for eq in q.params.split(":")) if q.params else {}), q) for q in questions]
            result = []
            with open(file, "r") as fd:
                reader = csv.DictReader(fd, delimiter=' ')
                for row in reader:
                    self.convertRow(row)
                    known = {p:row[p] for (p,z) in knownFields + resultFields}
                    for (p, z) in knownFields + resultFields:
                        del row[p]
                    row = {k:v for (k, v) in row.items() if v != "NA"}
                    for (q, origQ) in newQuestions:
                        if all(getattr(q, z) == known[p] for (p, z) in knownFields) and row == q.params:
                            result.append(self.Record(**origQ._asdict(), mkspan=known["makespan"], time=known["time"],
                                                      date = known["date"]))
                            break
            return result

    return Storage
    
            

        
