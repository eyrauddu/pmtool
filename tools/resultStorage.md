# resultStorage: storing results from pmtool runs

**Purpose:** Make it easier to save the results of running pmtool with
  many scheduling algorithms on many instances.
  
**Usage:** This is a Python class that should be used in a Python
  script. 

The main component is the `MakeStorage` function, which returns a
`Storage` class. `MakeStorage` has 4 parameters:

+ `instanceParameters` is a list of strings, which indicate the names
  of the parameters which describe the instances. The `Instance` class
  of the resulting `Storage` is a named tuple whose field names are
  those provided in `instanceParameters` in addition to the `platform`
  field which describes the platform. 
+ `getInstanceFile` is a function which, given an `Instance` object,
  shoud return the name of the `.rec` file which contains the instance
+ `getPlatformFile` is a function which, given a platform name as a
  string, should return the name of the corresponding platform file. 
+ `commandArgs` is a list of strings indicating how to invoke
  `pmtool`. The first string should be a path to the `pmtool`
  executable, and the next ones should be default arguments for all
  invocations.

Once we get a `Storage` class, we can create an instance by providing
it with a filename which indicates where to store the results. 


The code uses several concepts, all described by named tuples: 

+ An `Instance` specifies a pmtool problem instance, with an instance
  and a platform file
+ An `Algorithm` specifies a pmtool algorithm, with three fields:
  `alg` is the name of the algorithm, `isBound` is a boolean
  indicating whether this is a bound or an algorithm, `params`
  contains the param string of the algorithm, with the
  `key1=value1:key2=value2` syntax of pmtool.
+ A `Question` is the concatenation of an `Instance` and an
  `Algorithm`
+ A `Record` is a `Question` with an answer, i.e. with the following
  additional fields: `mkspan` and `time` provide the result from
  pmtool, `date` indicates the date at which the computation was
  done. 
  
The code provides helper functions to generate `Algorithm`s and
`Instance`s: 

+ `makeAlgorithm(name, isBound=False, **args)` combines all named
  args as parameters for the algorithm (the values should be strings)
+ `makeAlgorithms(name, isBound=False, **args)` in which the values of
  named args should be iterables containing string; this function
  returns a list of `Algorithm` objects, representing all possible
  combinations of parameters, just like the `key1=v1,v2:key2=v3,v4`
  syntax from pmtool. Examples: 

```python 
makeAlgorithms("indep", indep=["dualhp", "balest"], rank=["heft", "min"]) 
makeAlgorithms("heft", rank=["heft", "min", "none", "unit"]) 
```

+ `Storage.makeInstances` works the same as `makeAlgorithms`. 
+ `Storage.allCombinations(instances, algs)` generates all possible
`Question`s from the given lists of instances and algorithms. 


The most useful functions of the `Storage` class are:

+ `updateFile(questions)` updates the results file by running all
  questions which have no valid answer. An answer is valid if there is
  at least one, and the most recent answer is at least as recent as
  the corresponding instance file. TODO: There is still no test for
  the platform file. `updateFile` takes an optional `pred` argument
  which can change this definition of "valid answer".
+ `cleanupFile()` keeps only the latest answer for each question in
  the file. In addition, it accepts an optional `toBeRemoved`
  predicate which given a record specifies if it should be removed or
  not. 
+ `displayFileContents` provides a human (and R) readable output of
  the contents of the storage. It accepts a `file` optional argument
  to specify the output file (default is standard output), and a
  `pred` argument which given a record specifies if it should be
  displayed. 
  

## Usage example

The `cholesky` archive from the
[Graph Market](http://starpu.gforge.inria.fr/market/) contains a set
of `.rec` files from StarPU, all corresponding to a cholesky execution
for different number of tiles and for different platforms. The file
for size `N` is stored at `NxN/tasks.rec` file. The code to create the
corresponding storage is: 

```python 
import resultStorage
Ns   = [10, 20, 30, 40, 50, 60, 70, 90, 100, 110]
platforms = ["attila", "idgraf", "sirocco", "mirage"]
def getInstanceFile(i):
    return "./" + i.N + "x" + i.N + "/tasks.rec"
def getPlatformFile(p):
    return "./"+p+".platform"
DagStore = resultStorage.MakeStorage(["N"], getInstanceFile, getPlatformFile, ["/path/to/pmtool", "-t", "0.1"])
store = DagStore("./cholesky.raw")
```

Then, the following code can be used to populate the store with all
results: 

```python
instances = store.makeInstances("cholesky", N = map(str, Ns), platform = platforms)

algorithms = resultStorage.makeAlgorithms("heft", ("rank", ["heft", "min", "none", "unit", "area"]))
algorithms += resultStorage.makeAlgorithms("dmdas", ("rank", ["heft", "min", "none", "unit", "area"]))
algorithms += resultStorage.makeAlgorithms("hetprio", ("rank",["heft", "min", "none", "unit", "area"]))
algorithms += resultStorage.makeAlgorithms("hetprio", ("rank",["heft", "min", "none", "unit", "area"]), ("ssbaf", ["yes"]), ("stealidle", ["yes"]))
algorithms.append(resultStorage.Algorithm(alg = "ect", isBound=False, params=""))
algorithms.append(resultStorage.Algorithm(alg = "area", isBound=True, params="hybrid=no"))

if __name__ == "__main__":
    store.updateFile(store.allCombinations(instances, algorithms))
    store.displayFileContents(file="./cholesky.dat")
```
