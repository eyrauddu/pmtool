#include <time.h>
#include <vector>
#include <set>
#include <string>
#include <iostream>
#include <chrono>

#include <thread>
#include <mutex>

#include "instance.h"
#include "ProgramOptions.h"
#include "algorithm.h"
#include "schedAction.h"
#include "ReproduceAlgorithm.h"

using namespace std;


class AlgResult {
public:
  double makespan;
  double milliseconds;
  string name; 
  string key; // Is this better ? Not super clean, but well.
              /// Might even be randomly generated, why not.
};

void computeAlg(ProgramOptions& progOpt, Instance* ins,
		fullAlg& algAndOpt, AlgResult* result) {
  Algorithm* alg = algAndOpt.first;
  auto opts = algAndOpt.second;

  ActionSequence seq; 
  result->key = progOpt.buildName(opts, true);
  result->name = progOpt.buildName(opts, false);
  
  InternalShare saveResult(result->key, ins); 
  seq.add(&saveResult); 
  
  ExportSchedule* localExport = NULL;
  if(opts.isPresent("save")) {
    localExport = new ExportToFile(opts.asString("save"), ins, true);
    seq.add(localExport);
  }
  
  InternalShare* share = NULL; 
  if(opts.isPresent("share")) {
    share = new InternalShare(opts.asString("share"), ins); 
    seq.add(share); 
  }

  ExportAlloc* exportAlloc = NULL; 
  if(opts.isPresent("export")) {
    exportAlloc = new ExportAlloc(opts.asString("export"), ins,
				  progOpt.useSubmitOrder, progOpt.outputTypeInExport, progOpt.workerOrderInExport);
    seq.add(exportAlloc); 
  }
      
  ExportBubble * exportBubble = NULL; 
  if(opts.isPresent("bubble")) {
    exportBubble = new ExportBubble(opts.asString("bubble"), ins, opts.asInt("bubbleType")); 
    seq.add(exportBubble); 
  }
      
  auto start = chrono::steady_clock::now(); 
	
  try {
    result->makespan = alg->compute(*ins, &seq);
    auto time = chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - start);
    result->milliseconds = time.count()/ 1000.0; 

    //cout << input_file << " " << name << " " << result << " " << time.count();
	  
    // if(bestAlgorithm == NULL || bestAlgorithmValue > result) {
    //   bestAlgorithm = &(*it);
    //   bestAlgorithmValue = result;
    //   isBestSoFar = true; 
    //   if(bestExport){
    // 	if(bestSchedule) free(bestSchedule); 
    // 	bestSchedule = bestExport;
    //   }
    // }
	  
	  
  } catch (int e) {
    auto time = chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - start);
    result->makespan = -1;
    result->milliseconds = time.count() / 1000.0; 
  }
      
  if(localExport) free(localExport);
  if(exportAlloc) free(exportAlloc); 
  if(share) {
    share->finish(); 
    free(share); 
  }
  
  if(exportBubble) {
    exportBubble->finish(); 
    free(exportBubble); 
  }

  saveResult.finish(); 
}


void displayAlgResult(ProgramOptions& progOpt, Instance* instance,
		      fullAlg& algAndOpt, AlgResult& result) {

  static bool firstFile = true; 
  
  cout << instance->inputFile;
  if(instance->platformFile != "")
    cout << " " << instance->platformFile;
  else
    cout << " " << "NA" ; 
  cout << " False " << result.name;
  if(result.makespan > 0) 
    cout << " " << result.makespan;
  else
    cout << " " << "NA";
  cout << " " << result.milliseconds; 

  if(progOpt.saveFile != ""
     || progOpt.repartitionFile != "") {
    // Need to reproduce this run to record the new data and/or save to disk
    ActionSequence seq;
    ExportSchedule* globalExport = NULL;
    UtilAnalysis* utilAnalyser = NULL;
    
    if(progOpt.saveFile != "") {
      globalExport = new ExportToFile(progOpt.saveFile, instance,
				      firstFile, result.key, progOpt.useSubmitOrder);
      seq.add(globalExport);
    }
    
    if(progOpt.repartitionFile != "") {
      utilAnalyser = new UtilAnalysis(instance, progOpt.repartitionFile);
      seq.add(utilAnalyser);
    }

    AlgOptions opt;
    opt["key"] = result.key; 
    ReproduceAlgorithm rep(opt);
    rep.compute(*instance, &seq);
  
    if(utilAnalyser){
      if(instance->nbWorkerTypes == 2) {
	std::vector<std::vector<int> > r = utilAnalyser->getRepartition();
	for(int i = 0; i < (int) r.size(); i++) {
	  double usage = 0.0;
	  double otherUsage = 0.0;
	  for(int j = 0; j < (int) r[i].size(); j++){
	    usage += r[i][j] * instance->execTimes[i][j];
	    otherUsage += r[i][j] * instance->execTimes[1-i][j];
	  }
	  cout << " " << usage << " " << (i == 0 ? usage / otherUsage : otherUsage / usage);
	}
      } else {
	cout << " " << "NA" << " " << "NA";
	cout << " " << "NA" << " " << "NA";
      }
      string prefix = instance->inputFile + " ";
      if(instance->platformFile != "")
	prefix += instance->platformFile + " "; 
      utilAnalyser->write(prefix + result.name);
      free(utilAnalyser); 
    }

    if(globalExport) free(globalExport);
  }
  cout << endl;
  firstFile = false; 
}

mutex controlMutex;

void computeThread(int idx, ProgramOptions* progOpt, Instance* ins,
		   vector<AlgResult>* results, vector<bool>* started) {
  bool finished = false;
  unsigned nbAlgs = started->size(); 
  while(! finished) {
    // First find some work to do
    int i = 0; 
    {
      lock_guard<mutex> lock(controlMutex); 
      for(i = 0; i < nbAlgs; i++)
	if(!started->at(i)){
	  (*started)[i] = true;
	  break;
	}
    }
    
    if(i == nbAlgs) {
      // No work found, exit.
      finished = true;
      break;
    }
    
    computeAlg(*progOpt, ins, progOpt->algs[i], &(results->at(i)));
  }
}

int main(int argc, char** argv) {

  ProgramOptions progOpt;
  progOpt.parse(argc, argv); 

  if((progOpt.outputNamesRaw == 0) && (!progOpt.noHeader)) {
    // Output header
    cout << "input " << "platform " << "isBound " << "algorithm ";
    for(string s: progOpt.optionKeys) 
      cout << s << " "; 
    cout << "mkspan" << " " << "time"; 
    if( (progOpt.repartitionFile != "")){
      cout << " CPU.used CPU.af GPU.used GPU.af "; 
    }
    cout  << endl;
  }

  if(progOpt.platformFiles.size() == 0)
    progOpt.platformFiles.push_back("");

  if(progOpt.nbThreads < 0) {
    progOpt.nbThreads = (int) thread::hardware_concurrency();
    if(progOpt.nbThreads == 0)
      progOpt.nbThreads = 1;
    if(progOpt.verbosity >= 1)
      cerr << "Using " << progOpt.nbThreads << " parallel threads." << endl;
  }
  
  if(progOpt.outputBestFile != "") {
    ofstream save(progOpt.outputBestFile);
    save.close(); 
  }
  if(progOpt.saveFile != "") {
    ofstream save(progOpt.saveFile);
    save.close(); 
  }
  if(progOpt.repartitionFile != "") {
    ofstream save(progOpt.repartitionFile);
    save.close(); 
  }
  
  for(string &input_file: progOpt.inputFiles) {
    for(string &platform_file : progOpt.platformFiles) {
      // Read instance
      Instance* instance;
      size_t dotPosition = input_file.rfind('.'); 
      auto start = chrono::steady_clock::now(); 
      if(progOpt.verbosity >= 4) 
	cerr << "Reading input file '" << input_file << "' with platform file '" << platform_file << "'..."; 
      if( (dotPosition != string::npos
	   && input_file.compare(dotPosition, string::npos, ".rec") == 0)
	  || platform_file != "") {
	if(platform_file == "") 
	  instance = new RecFileInstance(input_file, progOpt.useSubmitOrder, progOpt.appendTags);
	else 
	  instance = new RecFileInstance(input_file, platform_file, progOpt.useSubmitOrder, progOpt.appendTags);
      }
      else
	instance = new Instance(input_file, progOpt.convertIndices);
    
      if(progOpt.noDependencies)
	instance->removeDependencies();
      if(progOpt.optRevDep) 
	instance->revertDependencies();

      if(progOpt.mergeTolerance > 0) {
	instance->autoMerge(progOpt.mergeTolerance);
      }
      auto time = chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - start);
      if(progOpt.verbosity >= 4) 
	cerr << " done (" << time.count()/1000.0 << " ms)." << endl; 
      instance->display(progOpt.verbosity);

      double bestBoundValue = 0; 
      fullBound* bestBound = NULL; 
    
      // Iterate through bound list, execute all
      for(auto it = progOpt.bounds.begin(); it < progOpt.bounds.end(); it++) {
	auto opts = it->second;
	string name = progOpt.buildName(opts, false);
	auto start = chrono::steady_clock::now(); 
      
	try {
	  double result = it->first->compute(*instance);
	  if(!bestBound || bestBoundValue < result) {
	    bestBoundValue = result;
	    bestBound = &(*it); 
	  }
	  auto time = chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - start);
	  //	auto duration = 
	  cout << input_file << " " << (instance->platformFile != "" ? instance->platformFile: "NA");
	  cout << " True " << name << " " << result << " " << time.count() / 1000.0;
	} catch (int e) {
	  auto time = chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - start);
	  cout << input_file << " " << (instance->platformFile != "" ? instance->platformFile: "NA");
	  cout << " True " << name << " " << "NA" << " " <<
	    time.count() / 1000.0;
	}
      
	if(progOpt.repartitionFile != "") {
	  cout << " " << "NA" << " " << "NA";
	  cout << " " << "NA" << " " << "NA";
	}

	cout << endl; 

      }

      vector<AlgResult> results(progOpt.algs.size()); 
      
      if(progOpt.nbThreads > 1) {
	// Test and warn if nbThreads > #available threads
	thread ths[progOpt.nbThreads];
	vector<bool> started(progOpt.algs.size(), false); 
	for(int k = 0; k < progOpt.nbThreads; k++) {
	  ths[k] = thread(computeThread, k, &progOpt, instance,
			  &results, &started); 
	}
	for (auto& th : ths) th.join();
	for(uint i = 0; i < progOpt.algs.size(); i++) {
	  displayAlgResult(progOpt, instance, progOpt.algs[i], results[i]);
	}
      } else {
	for(uint i = 0; i < progOpt.algs.size(); i++) {
	  computeAlg(progOpt, instance, progOpt.algs[i], &results[i]); 
	  displayAlgResult(progOpt, instance, progOpt.algs[i], results[i]);
	}
      }


      if(progOpt.outputBest) {
	double bestAlgorithmValue = std::numeric_limits<double>::infinity();
	int bestAlgIdx = -1; 
	for(uint idx = 0; idx < progOpt.algs.size(); idx++) {
	  if( results[idx].makespan > 0
	      && (bestAlgIdx == -1
		  || results[idx].makespan < bestAlgorithmValue) ) {
	    bestAlgIdx = idx;
	    bestAlgorithmValue = results[idx].makespan; 
	  }
	}
	
	if(bestBound != NULL)
	  cerr << "Best bound:    " << bestBoundValue << " with " << progOpt.buildName(bestBound->second, true) << endl;
	if(bestAlgIdx > 0) 
	  cerr << "Best schedule: " << bestAlgorithmValue << " with " << progOpt.buildName(progOpt.algs[bestAlgIdx].second,
                                                                                       true) << endl;
	if(bestBound != NULL && bestAlgIdx > 0) 
	  cerr << "Gap:           " << (bestAlgorithmValue - bestBoundValue) / bestBoundValue << endl; 

	if(progOpt.outputBestFile != "" && bestAlgIdx > 0) {
	  string name = instance->inputFile;
	  if(instance->platformFile != "")
	    name += ":" + instance->platformFile; 
	  ExportToFile exp(progOpt.outputBestFile, instance, false, name);
	  // TODO: allow $p / $i in file names to replace with platform / instance 
	  ExportAlloc alloc(progOpt.outputBestFile + ".rec", instance, progOpt.useSubmitOrder);
	  ActionSequence seq;
	  seq.add(&exp);
	  seq.add(&alloc);
	  AlgOptions opt;
	  opt.insert("key", results[bestAlgIdx].key);
	  ReproduceAlgorithm alg(opt);
	  alg.compute(*instance, &seq);
	  
	}
      }
    }
  }
  return(0);
  
}
