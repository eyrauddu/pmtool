#ifndef CRITICALPATH_H
#define CRITICALPATH_H

#include "algorithm.h"

class CriticalPath : public Bound {


 public:
  CriticalPath(const AlgOptions & options); 

  double compute(Instance& ins); 
  std::string name() { return "cp"; }; 
    
};

#endif
