#ifndef INDEPDUALGENERIC_H
#define INDEPDUALGENERIC_H

#include "IndepAllocator.h"
#include "instance.h"
#include <vector>

extern double lowerBoundTwoResource(Instance& ins, std::vector<int> taskSet, 
				    double CPUload = 0, double GPUload = 0); 

class IndepDualGeneric : public IndepAllocator {

 protected: 
  virtual double tryGuess(Instance &, std::vector<int> taskSet, std::vector<double>& loads,
			  double maxlen, IndepResult & result, bool getResult) = 0; 
  double epsilon = 0.01; 
  
 public: 
  IndepDualGeneric(const AlgOptions& opt); 
  IndepResult compute(Instance &, std::vector<int> &taskSet, std::vector<double> &loads); 
  
};




#endif
