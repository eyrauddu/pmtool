#ifndef REPRODUCEALGORITHM_H
#define REPRODUCEALGORITHM_H
#include <string>

#include "algorithm.h"

class ReproduceAlgorithm : public Algorithm {

  std::string shareKey; 
  
 public:
  ReproduceAlgorithm(const AlgOptions& options); 
  double compute(Instance& ins, SchedAction* action);

};



#endif
