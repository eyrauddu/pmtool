#ifndef HYBRIDBOUND_H
#define HYBRIDBOUND_H
#include <map>
#include "algorithm.h"
#include <string>

class HybridBound : public Bound {

  ModifiableBound *alg; 
  int verbosity; 
  int timeLimit; 
  bool doHybrid; 
  std::string shareKey; 
  
 public: 
  HybridBound(ModifiableBound *_alg, const AlgOptions &options); 
  double compute(Instance&); 
  std::string name(); 
  

};

#endif
