
#ifndef SYNCHROMAP_H
#define SYNCHROMAP_H

#include <map>
#include <string>
#include <future>
#include <mutex>

class SynchroMap  {
  std::map<std::string, std::pair< std::promise<void*>*, std::shared_future<void*> > > dict;
  std::mutex mtx;  // Need to protect my map. 

  void link(const std::string & key); 
  
 public:
  SynchroMap(); 
  // Inserts a value in the map
  void insert(const std::string & key, void* value);
  // Replaces an already present value -- should only be called for a key which "hasValue()",
  // otherwise use insert() directly
  void replace(const std::string & key, void* value);
  // gets a value. If not already present, blocks until the value has been pushed.
  // This gets dangerous if user misbehaves: leads to deadlock since no one will ever wake me up.
  // Solving it would require bounds & algs to declare what keys they will write to. 
  void* get(const std::string & key);

  bool isPresent(const std::string & key); 
  bool hasValue(const std::string & key); 
};



#endif
