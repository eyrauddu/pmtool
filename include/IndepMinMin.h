//
// Created by eyraud on 25/03/19.
// Based on A Comparison of Eleven Static Heuristics for
// Mapping a Class of Independent Tasks onto
// Heterogeneous Distributed Computing Systems
// doi:10.1006/jpdc.2000.1714


#ifndef PMTOOL_MINMIN_H
#define PMTOOL_MINMIN_H


#include "IndepAllocator.h"

class IndepMinMin : public IndepAllocator {
private:
    
    Instance* ins;
    std::vector<int> workerIndices;
    std::vector<int> bestWorkers; /* length = nb worker types */

    int getEarliestWorker(std::vector<double> &loads, int type);
    inline double endTime(std::vector<double> &loads, int workerType, int task);
public:
    IndepResult compute(Instance &ins, std::vector<int> &taskSet, std::vector<double> &loads) override;
    IndepMinMin(const AlgOptions &options);
};


#endif //PMTOOL_MINMIN_H
