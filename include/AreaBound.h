#ifndef AREABOUND_H
#define AREABOUND_H

#include <ilcplex/ilocplex.h>
#include <map>

#include "algorithm.h"
#include "HybridBound.h"

typedef IloArray<IloNumVarArray> NumVarMatrix;

class AreaBound : public ModifiableBound {

  int verbosity;
  std::string share; 

  Instance *ins; 
  IloEnv env; 
  //IloIntArray countTasks; 
  IloModel indepModel;
  IloRangeArray qtityConstraints;  
  IloCplex indepCplex; 
  NumVarMatrix wType;

  unsigned int nbWorkerTypes; 
  unsigned int nbTaskTypes; 
  std::vector<int> countTasks; 

  void clear(); 


 public:
  AreaBound(const AlgOptions & options); 

  void init(Instance& ins); 
  double compute(); 
  void finalize();
  std::vector<std::vector<double>> getRepartition();
  void removeTask(int taskID); 
  void restoreTask(int taskID);
  std::string name(); 
    
};

#endif
