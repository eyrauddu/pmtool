#ifndef INDEPALLOCATOR_H
#define INDEPALLOCATOR_H
#include <vector>
#include <list>

#include "instance.h"
#include "algoptions.h"

typedef std::vector<int> IndepOneResult; 
typedef std::vector< IndepOneResult > IndepResult;
// res[i] contains all tasks assigned to resource type i

class IndepAllocator {


 protected: 
  int verbosity; 

 public: 
 IndepAllocator(const AlgOptions & opt): verbosity(opt.asInt("verbosity", 0)) {}
  virtual IndepResult compute(Instance & ins, std::vector<int> &taskSet, std::vector<double> &loads) =  0; 

  bool needsPreciseLoads = false;  


};




#endif
