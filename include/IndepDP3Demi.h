#ifndef INDEPDP3DEMI_H
#define INDEPDP3DEMI_H

#include "IndepDualGeneric.h"
#include "instance.h"
#include <vector>

class IndepDP3Demi : public IndepDualGeneric {

 protected: 
  double tryGuess(Instance &, std::vector<int> taskSet, std::vector<double>& loads,
		  double target, IndepResult & result, bool getResult); 
  double discretizationConstant = 3.0;
#ifdef WITH_CPLEX
  bool solveWithCplex;
  bool cplexUseDiscretizedValues;
#endif
  
 public: 
  IndepDP3Demi(const AlgOptions& opt); 
  
};




#endif
