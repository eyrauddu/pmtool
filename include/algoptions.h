#ifndef ALGOPTIONS_H
#define ALGOPTIONS_H

#include <string>
#include <map>
#include <vector>

class AlgOptions : public std::map<std::string, std::string> {

 public:
  void insert(const std::string &key, const std::string &value); 
  void remove(const std::string &key); 

  bool isPresent(const std::string& key) const; 
  std::string asString(const std::string &key, const std::string& def = "") const; 
  int asInt(const std::string &key, const int def = 0) const ; 
  double asDouble(const std::string &key, const double def = 0.0) const; 
  void updateValue(double & v, const std::string &key) const;
  void updateValue(int & v, const std::string &key) const;
  void updateValue(std::string & v, const std::string &key) const;


  
  std::string parseWithName(const std::string &line); 
  std::vector<AlgOptions> multiParseWithName(const std::string & line, std::string & name); 
  void parse(const std::string &line); 
  void include(const std::string &opt); 
  std::string serialize() const;

    explicit AlgOptions(std::string toParse);

    AlgOptions();
};



#endif
