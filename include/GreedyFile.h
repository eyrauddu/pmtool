#ifndef GREEDYFILE_H
#define GREEDYFILE_H
#include "algorithm.h"
#include "instance.h"
#include "GreedyAlgorithm.h"

#include <vector>
#include <string>

class GreedyRanker : public RankComputer {
 protected:
  intCompare* oneCmp(Instance& ins, std::string subRankOpt); 
  std::vector<int> preAssign; 
 public: 
 GreedyRanker(const AlgOptions & options) : RankComputer(options) {}
  TaskSet* makeSet(Instance & ins, std::vector<int> alloc); 
};

class GreedyFile : public GreedyAlgorithm {

 protected: 
  std::string file; 
  std::vector<TaskSet*> queues; 
  std::vector<int> preAssign; 
  GreedyRanker ranker; 
  Instance* ins; 
  int stealerType; 
  bool spoliate; 
  bool pick; 
  std::string stealerName; 

  std::vector<bool> isStealer; 

 public: 
  GreedyFile(const AlgOptions & options); 
  double compute(Instance & instance, SchedAction* action); 
  int chooseTask(int worker, double now); 
  void onTaskPush(int task); 
  std::string name(); 
};


#endif
