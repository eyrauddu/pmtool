//
// Created by eyraud on 31/08/18.
//

#ifndef PMTOOL_ONLINEECT_H
#define PMTOOL_ONLINEECT_H


#include "OnlineGeneric.h"

class OnlineECT : public OnlineGeneric {
public:
    OnlineECT(const AlgOptions &options);

public:
    int assignTask(int task, double now) override;

};


#endif //PMTOOL_ONLINEECT_H
