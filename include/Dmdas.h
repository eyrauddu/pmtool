#ifndef DMDAS_H
#define DMDAS_H

#include "instance.h"
#include "algorithm.h"
#include "algoptions.h"
#include "GreedyAlgorithm.h"

class Dmdas : public GreedyAlgorithm {

 protected: 
  RankComputer ranker; 
  int verbosity; 
  std::vector<TaskSet*> localQueues; 
  std::vector<double> assignedLoad; 
  TaskSet* readyTasks;

  Instance* ins; 
  
  double getFinishedTime(double now, int task, int w, int wType);
  int getBestWorker(double now, int task);
  void assignFront(double now); 

  
 public:
  Dmdas(const AlgOptions& options);
  double compute(Instance &ins, SchedAction* action) override;
  int chooseTask(int worker, double now) override; 
  void onTaskPush(int task, double now) override;


};

#endif
