#ifndef INTERVALBOUND_H
#define INTERVALBOUND_H

#include <ilcplex/ilocplex.h>
#include "algorithm.h"

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<NumVarMatrix>  NumVarMatrix3D;

class IntervalBound : public Bound {
  
  IloEnv env; 
  Instance* instance; 

  int verbosity; 
  int limit; 
  double gap; 
  int nbIntervalsStart; 
  int nbIntervalsEnd; 
  bool addPaths; 
  std::string shareName; 
  std::string saveName; 

  IloModel model; 

  unsigned int nbIntervals;
  unsigned int nbWorkerTypes;
  unsigned int nbTasks;

  NumVarMatrix wType;
  NumVarMatrix3D useByInterval;
  IloNumVar load; 
  IloNumVarArray startTime; 
  IloExprArray execTime; 
 
  std::vector<int> critPath; 
  std::string weight;

  void clear(); 
  IloExpr getIntervalVar(int l); 

 public: 
  IntervalBound(const AlgOptions & options); 
  double compute(Instance & instance); 
  std::string name(); 

};

#endif 
