#ifndef LISTALGORITHM_H
#define LISTALGORITHM_H
#include <map>
#include <string>
#include <vector>

#include "instance.h"
#include "algorithm.h"
#include "algoptions.h"
#include "GreedyAlgorithm.h"

class ListAlgorithm : public GreedyAlgorithm {

 protected:
  RankComputer ranker;
  TaskSet* readyTasks;
 public: 
  ListAlgorithm(const AlgOptions &options);
  double compute(Instance& ins, SchedAction* action) override;
  int chooseTask(int worker, double now) override; 
  void onTaskPush(int task, double now) override;

};

#endif
