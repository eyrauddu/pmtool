#ifndef INDEPBASED2_H
#define INDEPBASED2_H

#include "instance.h"
#include "algorithm.h"
#include "algoptions.h"
#include "IndepAllocator.h"
#include "GreedyAlgorithm.h"

#include <list>

class IndepBased : public GreedyAlgorithm {

 protected: 
  IndepAllocator* indep; 
  //  std::string style = ""; 
  //  double proportion; 
  // std::string indepName; 
  bool hasChanged;  
  std::vector<int> unassignedTasks; 
  std::vector< IndepOneResult > allocated; // Could be lists ?
  Instance* ins; 

 public:
  IndepBased(const AlgOptions& options);
  virtual void allocate(double now); 
  virtual double compute(Instance &ins, SchedAction* action); 
  virtual std::string name();
  virtual void onTaskPush(int task, double now);
  virtual void chosenTask(int task, int wType, IndepOneResult::iterator it); 
  virtual int chooseTask(int worker, double now); 
  virtual void maybeAllocate(double now) = 0; 

};


IndepBased* makeIndep(const AlgOptions& options); 

#endif
