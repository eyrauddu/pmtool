#ifndef DEPBOUND_H
#define DEPBOUND_H

#include <iostream>
#include <ilcplex/ilocplex.h>
#include <map>
#include <vector>

#include "algorithm.h"
#include "HybridBound.h"

typedef IloArray<IloNumVarArray> NumVarMatrix;

class DepBound : public ModifiableBound {

 protected: 

  Instance *instance; 
  IloEnv env; 

  int verbosity; 
  //  int firstlimit; 
  std::string mode;
  std::string shareKey;
  // True if we should use the rounding from https://arxiv.org/abs/1912.03088
  bool bVersion;
  
  unsigned int nbWorkerTypes;
  unsigned int nbTasks;
  IloModel idc; 
  IloCplex idcCplex; 
  NumVarMatrix wType; 
  IloNumVar load; 
  IloNumVarArray startTime; 
  std::vector<bool> isRemoved;
  IloExprArray execTime; 
  IloRangeArray onlyOnce; 
  IloObjective objective; 

  bool outputInteger; 
  bool integerSolution; 
  
  void clear();


 public: 
  DepBound(const AlgOptions & options); 
  
  void init(Instance& ins);
  double compute(); 
  void finalize(); 
  void removeTask(int taskID); 
  void restoreTask(int taskID);
  std::string name() {return "depbound"; }
  double tryHard(int timeLimit); 
    

  std::vector<int> getAllocTasks(); 

};




#endif
