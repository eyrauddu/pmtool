#ifndef INDEPBALANCED_H
#define INDEPBALANCED_H

#include "IndepAllocator.h"
#include "instance.h"
#include <vector>

class IndepBalancedEst : public IndepAllocator {

 protected: 
 public: 
  IndepBalancedEst(const AlgOptions& opt); 
  IndepResult compute(Instance &, std::vector<int> &taskSet, std::vector<double> &loads); 
  
};

class IndepBalancedMkspan : public IndepAllocator {

 protected: 
 public: 
  IndepBalancedMkspan(const AlgOptions& opt); 
  IndepResult compute(Instance &, std::vector<int> &taskSet, std::vector<double> &loads); 
  
};




#endif
