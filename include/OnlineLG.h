//
// Created by eyraud on 11/12/17.
//

#ifndef PMTOOL_ONLINELG_H
#define PMTOOL_ONLINELG_H

#include "algorithm.h"
#include "OnlineGeneric.h"

/* Online implementation of the algorithm from
 * "Scheduling Problems on Two Sets of Identical Machines"

   Only works with two types of ressources

 */

class OnlineLG : public OnlineGeneric {

public:
    explicit OnlineLG(const AlgOptions &options);

    double compute(Instance& ins, SchedAction* action) override;
    int assignTask(int task, double now) override;
};


#endif //PMTOOL_ONLINEZHANG_H
