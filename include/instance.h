#ifndef INSTANCE_H
#define INSTANCE_H

#include <vector>
#include <string>
#include <limits>
#include "SynchroMap.h"
#include <unordered_map>
#include <mutex>

class Instance {
public:
  std::vector<int> nbWorkers;
    std::vector<std::vector<int>> workerIDs;
    std::vector< std::vector<double> > execTimes;
  std::vector< std::vector<int> > dependencies;
  bool isIndependent;
  std::vector<int> taskTypes;
  unsigned int nbTasks;
  unsigned int totalWorkers;
  unsigned int nbTaskTypes;
  unsigned int nbWorkerTypes;

  SynchroMap extraData; 
  std::mutex mtx; 
  
  // Those are optional, only printed if available. 
  std::vector<std::string> workerNames;
  std::vector<std::string> taskTypeNames;
  std::vector<std::string> taskIDs; 

  // Information about communications
  // Invariants: each task type uses the same data sizes
  // 
  // Need to identify data items, give them sizes (maybe they have types ?)
  // tasks depend on data items, produce items
  // EACH ITEM IS PRODUCED ONCE ! Converted from *PU data by versioning
  // Need location information about initial data 
  // [data.rec only gives MPI owners, and coordinates]
  // Need information about which worker is in which memory node
  // Need bandwidth information
  // [Available in StarPU, but not yet in .rec format]

  // Questions: I think I cannot handle commutative stuff

  // Reading data: 
  // Each task has Handles and Modes
  // Following a topological ordering, create a new version each time a task writes a handle
  // Tasks depend on the last version of the Handle

  // Starting state: 
  // constant bandwidth as command-line argument [Hmm. Not clean, though. ]
  // Memory node = worker type [otherwise I am not sure how to handle it in schedulers]
  // Initial data is all on CPUs
  // No item type, I don't think it's needed, store items directly


  // If necessary: remove the itemNames, that's a lot of strings. 
  // itemSizes repeats a lot of information, but not more than if it was itemTypes + typeSizes, 
  // and less indirection...
  std::vector<int> itemSizes;
  std::vector<std::string> itemNames;
  std::vector<std::vector<int> > itemsRequired; 
  // itemsRequired[i][j] = index of jth item that task i requires
  std::vector<std::vector<int> > itemsProduced;
  // itemsProduced[i][j] = index of jth item that task i produces

  std::vector<std::vector<int>> memoryNodes;
  int nbMemoryNodes = -1;
  // This vector works like workerIDs: length is nbWorkerTypes, memoryNodes[t] specifies
  // the memory nodes of all workers of type t

  std::vector<std::vector<double>> memoryNodeBW;
  std::vector<std::vector<double>> memoryNodeLat;

  std::string inputFile;
  std::string platformFile;
  
  // Computed afterwards
 public: 
  std::vector< std::vector<int> > revDep; 
  std::vector<int> topOrder; 
  std::vector< std::vector<int> > ancestors;
  // ancestors[i][j] == 1 iff there exists a path from j to i
  
  void doConvertIndices(); 
  Instance();
 public: 
  Instance(const std::string input_file, int convertIndices);
  int getType(int m, int* index = NULL); 

  inline bool isValidValue(double x) {
    return( (x != std::numeric_limits<double>::infinity())
	    && (x > 0) );
  }

  inline double execType(int wType, int task) {
    return execTimes[wType][taskTypes[task]]; 
  }

  inline bool isValidType(int wType, int task) {
    return isValidValue(execType(wType, task));
  }

  inline double execWorker(int w, int task) {
    return execType(getType(w), task); 
  }

  inline bool isValidWorker(int w, int task) {
    return isValidType(getType(w), task); 
  }

  void display(int verbosity);
  std::string formatTask(int task); 
  void autoMerge(double tolerance = 0.10); 
  void mergeWorkerTypes(std::vector<int> indicesToMerge);
  /* Accessors for static analysis */
  std::vector<int> getTopOrder(); 
  std::vector< std::vector<int> > getAncestors(); 
  std::vector< std::vector<int> > getRevDependencies();
  std::vector<double> computeRanks(std::vector<double> wbar, int to = -1, int from = -1);
  std::vector<double> computeHEFTRank();
  std::vector<double> computeMinRank();
  std::vector<double> computeUnitRank();
  std::vector<double> computePreAllocRank(std::vector<int> alloc);
  std::vector<double> computeLongestPath(const std::vector<double> &weights, bool display=false, std::vector<int>* storeCP=NULL, int to = -1, int from = -1); 
  std::vector<double> computeCriticalPath(bool display, std::vector<int>* storeCP);
// result[i] contains task types sorted by their execution time on resource type i, longest first
  std::vector<std::vector<int> >* sortedByLength;
    std::vector<std::vector<int> >* getSortedByLength();

  void removeDependencies(); 
  void revertDependencies(); 

 protected:
  void updateTopOrder(std::vector<int>& computed, int i, int &l);
  void computeTopOrder(); 
  void computeAncestors(); 
  void computeRevDependencies();
  int  getWorkerWithWorkerID(int workerid);
};

class CholeskyInstance : public Instance {
  CholeskyInstance(int n, int nbCPU, int nbGPU, double execTimesCPU[], double execTimesGPU[]);
};

class RecFileInstance : public Instance {
public:
    RecFileInstance(const std::string inputFile, bool useSubmitOrder = false, bool appendTags = true);
    RecFileInstance(const std::string inputFile, const std::string platformFile, bool useSubmitOrder = false, bool appendTags = true);
protected:
    void readFromFile(const std::string inputFile, std::unordered_map<std::string, std::vector<double>> timings,
                          bool useSubmitOrder, bool appendTags);

};



#endif
