#ifndef PROGRAMOPTIONS_H
#define PROGRAMOPTIONS_H
#include "algoptions.h"
#include "algorithm.h"

#include <vector>
#include <set>
#include <string>

typedef std::pair<Algorithm*, AlgOptions> fullAlg;
typedef std::pair<Bound*, AlgOptions> fullBound;

class ProgramOptions {
 private:
  void displayAlgList();
  void displayBoundList();
  template <class A, typename Func> 
    void insertComputable(std::vector< std::pair<A, AlgOptions> > & elems,
			  std::string description, Func func); 
  void insertAlg(std::string description);
  void insertBound(std::string description); 

 
 public:
  ProgramOptions();
  void parse(int argc, char** argv); 
  void usage();
  std::string buildName(AlgOptions opts, bool forceRaw);

  std::vector<std::string> inputFiles;
  std::vector<std::string> platformFiles; 
  int convertIndices = 1;
  AlgOptions globalOptions;
  int verbosity = 0;
  std::string saveFile;
  bool noDependencies = false;
  std::string repartitionFile = "";
  double mergeTolerance = 0.01;
  bool outputNamesRaw = 0; 
  bool noHeader = false; 
  bool optRevDep = false; 
  bool outputBest;
  std::string outputBestFile; 
  int nbThreads;
  bool appendTags;
  bool useSubmitOrder;
  bool outputTypeInExport;
  bool workerOrderInExport; 

  std::set<std::string> optionKeys; 
  std::vector<fullAlg> algs;
  std::vector<fullBound> bounds;

  
  
};




#endif
