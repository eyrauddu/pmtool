#ifndef COMSEQUENCE_H
#define COMSEQUENCE_H
#include <vector>
#include "availSequence.h"
#include "instance.h"

// Implements a not so greedy way of handling communication resources
// New transfers can only start after any ongoing transfer with which they are incompatible. 
// Interference is handled with availSequence

class CommSequence {
 protected: 

  Instance* ins; 
  int nbWT; 

  // This handles contention. 
  // 1st approx: each transfer starts ASAP on its source & dest resource
  // Transfer end is when both are done.
  // 2nd approx: transfers are preemptible (malleable when BW values are heterogeneous ?)
  // ending time = max availability, on other resources it starts ALAP ?
  // Yes, but then how does it interact with incompatibility ?

  // Easy way: no incompatibility, only contention. With
  // availSequences. Transfers start ALAP.  LastMile model ? With
  // homogeneous BW to start. So transfers use 2 resources, and use
  // them at 100%.

  std::vector<AvailSequence> outgoing; 
  std::vector<AvailSequence> incoming; 

 public: 
  CommSequence(Instance& ins); 
  
  // Returns ending time
  // If provided, goal means: do not finish earlier than this time. 
  // schedule the transfer, trying to end at "goal" 
  // if "goal" is too early, end as soon as possible
  // In all cases, schedule all parts of the transfer to finish at the same time, ALAP. 
  double newTransfer(double now, int source, int dest, int size, double goal = -1.0); 

};




#endif
