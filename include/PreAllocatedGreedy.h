#ifndef GREEDYFILE_H
#define GREEDYFILE_H
#include "algorithm.h"
#include "instance.h"
#include "GreedyPerType.h"

#include <vector>
#include <string>

class GreedyRanker : public RankComputer {
protected:
    intCompare* oneCmp(Instance& ins, std::string subRankOpt) override;
    std::vector<int> preAssign;
public:

    explicit GreedyRanker(const AlgOptions & options);
    TaskSet* makeSet(Instance & ins, std::vector<int> alloc);
};


class PreAllocatedGreedy : public GreedyPerType {

  std::string file; 
  std::vector<int> preAssign;
    GreedyRanker ranker;

protected:
    TaskSet* makeQueue();
 public:
    explicit PreAllocatedGreedy(const AlgOptions & options);
    double compute(Instance & instance, SchedAction* action) override;
    void onTaskPush(int task, double now) override;

    std::string key;
};


#endif
