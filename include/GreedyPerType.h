//
// Created by eyraud on 11/12/17.
//

#ifndef PMTOOL_GREEDYPERTYPE_H
#define PMTOOL_GREEDYPERTYPE_H
#include "algorithm.h"
#include "instance.h"
#include "GreedyAlgorithm.h"

#include <vector>
#include <string>


class GreedyPerType : public GreedyAlgorithm {

    RankComputer ranker;
    int stealerType;
    bool spoliate;
    bool pick;
    std::string stealerName;
    
    std::vector<bool> isStealer; 
    
 protected:
    Instance* ins;
    std::vector<TaskSet*> queues;
    virtual TaskSet* makeQueue();
public:
    explicit GreedyPerType(const AlgOptions & options);

    virtual double compute(Instance & instance, SchedAction* action) override;
    int chooseTask(int worker, double now) override;
};


#endif //PMTOOL_GREEDYPERTYPE_H
