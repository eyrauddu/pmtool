#ifndef AREASTART_H
#define AREASTART_H

#include <iostream>
#include <ilcplex/ilocplex.h>
#include <map>
#include <vector>

#include "algorithm.h"
#include "DepBound.h"
#include "AreaBound.h"

typedef IloArray<IloNumVarArray> NumVarMatrix;

class AreaStart : public DepBound {

 protected: 
  DepBound area; 
  std::vector < std::vector<int> > ancestors; 
  std::string save; 

 public: 
  AreaStart(const AlgOptions & options); 
  
  void init(Instance& ins);
  //  double compute(); 
  std::string name() {return "areastart"; }
  double tryHard(int timeLimit); 
    


};




#endif
