#ifndef SCHEDACTION_H
#define SCHEDACTION_H

#include <fstream>
#include <sstream>
#include <vector>
#include "instance.h"

#define SCHEDACTION_NONE -5

class SchedAction {
public: 
  virtual void onSchedule(int task, int worker, double startTime, double endTime) { }
  virtual void onTaskPush(int task) { }
};


class ActionSequence : public SchedAction {
  std::vector<SchedAction*> actions;
 public: 
  ActionSequence() {}
  void add(SchedAction *a); 
  void remove(SchedAction* a); 
  void onSchedule(int i, int w, double s, double f); 
  void onTaskPush(int t); 
};

class ExportSchedule : public SchedAction {
 protected: 
  std::ostream* output;
  Instance* instance; 
  std::string name;
  bool submitOrder;
  void outputHeader(); 
public: 
  ExportSchedule(std::ostream *stream, Instance* ins, bool header = false, std::string name ="");
  ExportSchedule(Instance* ins, std::string name = ""); 
  void onSchedule(int i, int w, double s, double f); 
  void changeName(std::string newName); 
};
class ExportToFile : public ExportSchedule {
  std::ofstream* f; 
 public:
  ExportToFile(std::string filename, Instance* ins, bool header = false, std::string name ="", bool submitOrder = false); 
  ~ExportToFile(); 
};
class ExportToString: public ExportSchedule {
  std::ostringstream* f;
 public:
  ExportToString(Instance* ins, bool header = false, std::string name ="");
  std::string getResult(); 
};



class ExportAlloc : public SchedAction {
  std::ofstream output;
  Instance* instance;
  bool submitOrder;
  bool outputType;
  bool workerOrder; 
  std::vector<int> workerTaskCount; 
public: 
  ExportAlloc(std::string filename, Instance* ins, bool submitOrder = false, bool outputType = false, bool workerOrder = false);
  void onSchedule(int i, int w, double s, double f); 
  ~ExportAlloc(); 
};


class UtilAnalysis : public SchedAction {
  Instance* ins; 
  std::vector<std::vector<int> > repartition; 
  std::ofstream output; 
 public: 
  UtilAnalysis(Instance* _ins, std::string saveFile);
  ~UtilAnalysis();
  void onSchedule(int i, int w, double s, double f); 
  std::vector<std::vector<int> > getRepartition(); 
  void reset();
  void write(std::string prefix); 
};

class InternalShare : public SchedAction {
 protected: 
  std::string shareName; 
  Instance* instance; 
  std::vector<int> allocation; 
  std::vector<int> worker; 
  std::vector<double> startTimes; 
  std::vector<double> endTimes; 
  
 public: 
  InternalShare(std::string shareName, Instance* ins); 
  void onSchedule(int i, int w, double s, double f); 
  void finish(); 
};

class ExportBubble: public InternalShare {
  std::ofstream output; 
  int bubbleTaskType; 
 public: 
  ExportBubble(std::string fileName, Instance* ins, int bubbleTaskType); 
  void finish(); 
};


#endif
