#ifndef ITERDEPBOUND_H
#define ITERDEPBOUND_H

#include "DepBound.h"
#include <vector>
#include <string>

class IterDepBound : public DepBound {

 protected: 
  std::vector < std::vector<int> > ancestors; 
  std::vector<int> topOrder; 
 public:
 IterDepBound(AlgOptions options) : DepBound(options) {}
  
  void findViolatedArea(std::vector< IloNumArray> weights, 
			IloNumArray startTimes, 
			int& source, int& dest); 

  void init(Instance& ins); 
  double compute();
  std::string name() {return "iter"; }; 

};


#endif
