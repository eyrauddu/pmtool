//
// Created by eyraud on 11/12/17.
//

#ifndef PMTOOL_ONLINEMG_H
#define PMTOOL_ONLINEMG_H

#include "algorithm.h"
#include "OnlineGeneric.h"

/* Online implementation of the algorithm from
 * "Scheduling Problems on Two Sets of Identical Machines"

   Only works with two types of ressources

 */

class OnlineMG : public OnlineGeneric {

 protected:
  double maxRTime;
  double sumRTime;
  int largestGroup;
  int smallestGroup;
  int m1, m2; 
  double alpha = 1.00;
  double gamma = 1.00;
  
public:
    explicit OnlineMG(const AlgOptions &options);

    double compute(Instance& ins, SchedAction* action) override;
    int assignTask(int task, double now) override;
};


#endif //PMTOOL_ONLINEMG_H
