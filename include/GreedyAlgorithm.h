#ifndef GREEDYALGORITHM_H
#define GREEDYALGORITHM_H
#include <map>
#include <string>
#include <vector>

#include "instance.h"
#include "algorithm.h"
#include "algoptions.h"
#include "ItemManager.h"

class GreedyAlgorithm : public Algorithm {

 protected:

  bool doComms;
  int verbosity; 
  virtual int chooseTask(int worker, double now) = 0; 
  virtual void onTaskPush(int task, double now) = 0;
  std::vector<int> nbDep; 
  std::vector<double> endTimesWorkers; 
  std::vector<int> runningTasks; 
  std::vector<bool> leaveIdle;

  ItemManager* itemManager;

 public: 
  GreedyAlgorithm(const AlgOptions &options);
  double compute(Instance& ins, SchedAction* action);
};

#endif
