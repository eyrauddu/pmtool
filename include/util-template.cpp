#include <limits>
#include <list>

template  <class T>
T getVal(std::istream &input) {
  T x; 
  input >> x; 
  if(!input){
    input.clear(); 
    char inf[4]; 
    input.get(inf, 4); 
    if(input.gcount() == 3 && inf[0] == 'i' && inf[1] == 'n' && inf[2] == 'f') 
      x = std::numeric_limits<T>::infinity(); 
    else 
      throw( -1 ); 
  }
  return x; 
}

template <class T>
int readArray(std::istream &input, std::vector<T> &v) {
  int i = 0; char c; 
  T x; bool keepgoing = true; 
  if(!readThisChar(input, '[')) return(-1); 
  if(!readChar(input, c)) return(-1); 
  if(c == ']') return 0;  
  input.putback(c); 
  x = getVal<T>(input);
  v.push_back(x); i++;
  while(keepgoing) {
    if(!readChar(input, c)) return(-1); 
    if(c == ']')
      keepgoing = false; 
    else if (c == ',') {
      x = getVal<T>(input); 
      v.push_back(x); i++;
    } else
      throw(-1); 
  }
  return i; 
}

// template int readArray(std::istream&, std::vector<int>&);
// template int readArray(std::istream&, std::vector<double>&);

template<class T> 
std::ostream& operator<<(std::ostream& out, std::vector<T> v) {
  out << "["; 
  for(auto it = v.begin(); it != v.end(); it++) 
    out << *it << " "; 
  out << "]"; 
  return out; 
}

template<class T> 
std::ostream& operator<<(std::ostream& out, std::list<T> v) {
  out << "["; 
  for(auto it = v.begin(); it != v.end(); it++) 
    out << *it << " "; 
  out << "]"; 
  return out; 
}

template<class T> 
std::ostream& operator<<(std::ostream& out, std::set<T> v) {
  out << "["; 
  for(auto it = v.begin(); it != v.end(); it++) 
    out << *it << " "; 
  out << "]"; 
  return out; 
}
template<class T, class U> 
std::ostream& operator<<(std::ostream& out, std::set<T, U> v) {
  out << "["; 
  for(auto it = v.begin(); it != v.end(); it++) 
    out << *it << " "; 
  out << "]"; 
  return out; 
}

template <class T>
T getMax(std::vector<T> v) {
  if(v.size() == 0) return -1; 
  T m = v[0];
  for(auto i = v.begin(); i != v.end(); i++) {
    if(*i > m) m = *i; 
  }
  return m;
}

template <class T, class R, typename Func> 
T getMax(std::vector<T> v, Func func) {
  if(v.size() == 0) return -1; 
  T best = v[0]; 
  R bestScore = func(best); 
  for(auto &x: v) {
    R score = func(x); 
    if(score > bestScore){
      best = x; 
      bestScore = score;
    }
  }
  return best; 
}

template <class T> T getSum(std::vector<T> v){
  return getSum<T, T>(v, [] (T a) { return a; });
}

template <class T, class R, typename Func> R getSum(std::vector<T> v, Func func) {
  R r = 0; 
  for(auto it = v.begin(); it != v.end(); it++)
    r += func(*it); 
  return r;
}


struct identity {
    template<typename U>
    constexpr auto operator()(U&& v) const noexcept
        -> decltype(std::forward<U>(v))
    {
        return std::forward<U>(v);
    }
};
