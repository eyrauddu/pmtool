#ifndef INDEPDP2_H
#define INDEPDP2_H

#include "IndepDualGeneric.h"
#include "instance.h"
#include <vector>

class IndepDP2 : public IndepDualGeneric {

 protected: 
  double tryGuess(Instance &, std::vector<int> taskSet, std::vector<double>& loads,
		  double maxlen, IndepResult & result, bool getResult); 
  double discretizationConstant = 3.0; 
#ifdef WITH_CPLEX
  bool solveWithCplex;
  bool cplexUseDiscretizedValues;
#endif
  
 public: 
  IndepDP2(const AlgOptions& opt); 
  
};




#endif
