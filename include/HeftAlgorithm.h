#ifndef HEFTALGORITHM_H
#define HEFTALGORITHM_H
#include <map>
#include <string>
#include <vector>

#include "instance.h"
#include "algorithm.h"
#include "availSequence.h"

class HeftAlgorithm : public Algorithm {

 protected:
  RankComputer ranker; 
  int verbosity; 

 public: 
  // List of options:
  // Rank
  // 
  HeftAlgorithm(const AlgOptions &options);
  double compute(Instance& ins, SchedAction* action);
};

#endif
