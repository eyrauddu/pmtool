//
// Created by eyraud on 18/09/18.
//

#ifndef PMTOOL_INDEPCLB2C_H
#define PMTOOL_INDEPCLB2C_H


#include "IndepAllocator.h"

class IndepCLB2C : public IndepAllocator {

public:
    IndepCLB2C(const AlgOptions &opt);

    IndepResult compute(Instance &ins, std::vector<int> &taskSet, std::vector<double> &loads) override;
};


#endif //PMTOOL_INDEPCLB2C_H
