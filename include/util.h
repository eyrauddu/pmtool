#ifndef UTIL_H
#define UTIL_H
#include <vector>
#include <set>
#include <istream>
#include <ostream>

// Ocaml spec: returns 0 if similar, -1 if a is smaller than b, 1 if a is larger than b
//   Specifically, we are comparing priorities, so larger priority should mean "to be 
//   done before"
class intCompare {
 public: 
  virtual int operator() (int a, int b) = 0; 
};

// This is responsible for strict ordering (so no "similar" for
// different ints), and for sticking to the "order" of the underlying
// container. largeFirst = true means this container outputs the large
// items first.
class strictCompare {
 protected: 
  intCompare* cmp; 
  bool largeFirst; 
 public: 
  strictCompare(intCompare* _cmp, bool largeFirst = false); 
  bool operator() (int a, int b); 
};

class rankCompare : public intCompare {
protected:
  std::vector<double> r;
  bool rev; 
  rankCompare() {}
public: 
  rankCompare(std::vector<double> rank, bool reverse = false); 
  int operator() (int a, int b);
};

class lexCompare : public intCompare {
 protected: 
  std::vector<intCompare*> cmps; 
 public: 
  lexCompare(std::vector<intCompare*> _cmps); 
  int operator() (int a, int b); 
};

bool readChar(std::istream &input, char &c); 

bool readThisChar(std::istream &input, char c); 

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems); 

std::vector<std::string> split(const std::string &s, char delim); 





template <class T> int readArray(std::istream &input, std::vector<T> &v); 

template<class T>  std::ostream& operator<<(std::ostream& out, std::vector<T> v); 
template<class T>  std::ostream& operator<<(std::ostream& out, std::set<T> v); 

template<class T, class U> 
  std::ostream& operator<<(std::ostream& out, std::set<T, U> v); 

template <class T> T getMax(std::vector<T> v); 
//template <class T> T getSum(std::vector<T> v); 

template < class T> T getSum(std::vector<T> v); 
template < class T, class R, typename Func> R getSum(std::vector<T> v, Func func); 


#include "util-template.cpp"

#endif
