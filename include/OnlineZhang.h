//
// Created by eyraud on 11/12/17.
//

#ifndef PMTOOL_ONLINEZHANG_H
#define PMTOOL_ONLINEZHANG_H

#include "algorithm.h"
#include "OnlineGeneric.h"

/* Online implementation of the algorithm from
 * "ONLINE SCHEDULING OF MIXED CPU-GPU JOBS"
   DOI: 10.1142/S0129054114500312

   Only works with two types of ressources

   Does not change an allocation decision once taken, every choice is made on task push
 */

class OnlineZhang : public OnlineGeneric {

    // Options
    double lambda = 1.69;
  double beta = 0.80;
  double theta = 1.04;
  double phi = 0.64;
  //void updateValue(double&v, const std::string &key, const AlgOptions& opt);

    // Runtime values
    int largestGroup;
    int smallestGroup;
    int m1, m2;
    double rule3LoadG1;
    double rule3LoadG2;
    std::vector<double> avgLoad;

public:
    explicit OnlineZhang(const AlgOptions &options);

    double compute(Instance& ins, SchedAction* action) override;
    int assignTask(int task, double now) override;
};


#endif //PMTOOL_ONLINEZHANG_H
