#ifndef INDEPZHANG_H
#define INDEPZHANG_H

#include "IndepAllocator.h"
#include <string>


/* Algorithms from the online paper "ONLINE SCHEDULING OF MIXED CPU-GPU JOBS"
   DOI: 10.1142/S0129054114500312
   
   Only works with two types of ressources, m and k

   Automatically choose between Al2 if m == k, Al3 if m==1 or k==1, 
   Al5 in the general case. 
 */


class IndepZhang : public IndepAllocator {
  
  
 protected: 
  double lambda = 1.69;
  double beta = 0.80;
  double theta = 1.04;
  double phi = 0.64; 
  void updateValue(double&v, const std::string key, const AlgOptions& opt); 

  std::string sortBy;
  int rev; 

  std::vector<double> workerLoads; 

  
 public:
  IndepZhang(const AlgOptions& options); 
  IndepResult compute(Instance& ins, std::vector<int> &taskSet, std::vector<double> &loads);
  
  std::string name() {
    return "zhang";
  }
 protected:
  void assignList(Instance& ins, int groupNumber, int task);
  double minLoad(Instance& ins, int groupNumber, int* argmin = NULL); 
  
  // IndepResult computeAl2(Instance& ins, std::vector<int> taskSet, std::vector<double> loads);
  // IndepResult computeAl3(Instance& ins, std::vector<int> taskSet, std::vector<double> loads);
  IndepResult computeAl5(Instance& ins, std::vector<int> &taskSet, std::vector<double> &loads);
  
  
  
};




#endif
