#ifndef INDEPIMREH_H
#define INDEPIMREH_H

#include "IndepAllocator.h"
#include "instance.h"
#include <vector>


class IndepImreh : public IndepAllocator {

 protected: 
  double alpha, gamma; 
  
 public: 
  IndepImreh(const AlgOptions& opt); 
  IndepResult compute(Instance &, std::vector<int> &taskSet, std::vector<double> &loads); 
  
};




#endif
