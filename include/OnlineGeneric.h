//
// Created by eyraud on 30/08/18.
//

#ifndef PMTOOL_ONLINEGENERIC_H
#define PMTOOL_ONLINEGENERIC_H


#include <queue>
#include "algorithm.h"

typedef std::pair<double, int> taskAndReadyTime;

class OnlineGeneric : public Algorithm {
public:
    OnlineGeneric(const AlgOptions &options);

    double compute(Instance &instance, SchedAction *action) override;
    virtual int assignTask(int task, double now) = 0;

protected:
    Instance* ins;

    std::priority_queue<taskAndReadyTime, std::vector<taskAndReadyTime>, std::greater<taskAndReadyTime>> readyTasks;
    std::vector<double> finishTimes;
    std::vector<double> readyTimes;
    std::vector<int> nbScheduledPredecessors;

    int getEarliestWorker(int type);
    int getEarliestFinishWorker(int task, double now);

    int verbosity;
};


#endif //PMTOOL_ONLINEGENERIC_H
