#ifndef INDEPACCEL_H
#define INDEPACCEL_H
#include "IndepAllocator.h"
#include "instance.h"
#include <vector>



class IndepAccel : public IndepAllocator {

 protected: 
  bool tryGuess(Instance &, std::vector<int> taskSet, double target, IndepResult & result); 
  double epsilon = 0.01; 
  
 public: 
  IndepAccel(const AlgOptions& opt); 
  IndepResult compute(Instance &, std::vector<int> &taskSet, std::vector<double> &loads); 
  
};


#endif
