#ifndef INDEPDUALHP_H
#define INDEPDUALHP_H

#include "IndepAllocator.h"
#include "instance.h"
#include <vector>


class IndepDualHP : public IndepAllocator {

 protected: 
  bool tryGuess(Instance &, std::vector<int> taskSet, std::vector<double> loads, double lambda, 
		  IndepResult & result); 
  double epsilon = 0.001; 
  
 public: 
  IndepDualHP(const AlgOptions& opt); 
  IndepResult compute(Instance &, std::vector<int> &taskSet, std::vector<double> &loads); 
  
};




#endif
