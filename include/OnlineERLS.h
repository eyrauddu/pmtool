//
// Created by eyraud on 31/08/18.
//

#ifndef PMTOOL_ONLINEERLS_H
#define PMTOOL_ONLINEERLS_H


#include "OnlineGeneric.h"

class OnlineERLS : public OnlineGeneric {
public:
    double compute(Instance &instance, SchedAction *action) override;

    OnlineERLS(const AlgOptions &options);

    int assignTask(int task, double now) override;
protected:
    int largestGroup;
    double threshold;
};


#endif //PMTOOL_ONLINEERLS_H
