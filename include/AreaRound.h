//
// Created by eyraud on 12/06/18.
//

#ifndef PMTOOL_AREAROUND_H
#define PMTOOL_AREAROUND_H

#include "AreaBound.h"
#include "IndepAllocator.h"

// Implements the rounding strategy introduced in https://doi.org/10.1016/j.jpdc.2015.07.002
// Can be downloaded at http://www.engr.colostate.edu/~ktarplee/pdf/jpdc_makespan.pdf

class AreaRound : public IndepAllocator {

public:
    AreaRound(const AlgOptions &options);
    IndepResult compute(Instance &ins, std::vector<int> &taskSet, std::vector<double> &loads) override;
    AreaBound bound;
};


#endif //PMTOOL_AREAROUND_H
