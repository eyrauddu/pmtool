#ifndef ALGORITHM_H
#define ALGORITHM_H
#include "instance.h"
#include "schedAction.h"
#include "algoptions.h"
#include "util.h"
#include <vector>
#include <string>
#include <deque>
#include <set>

class Algorithm {
 public: 
  virtual double compute(Instance& instance, SchedAction* action) = 0;
};

class TaskSet {
 public: 
  virtual void insert(int task) = 0; 
  virtual void erase(int task) = 0; 
  virtual bool compare(int taskA, int taskB) = 0; 
  virtual int size() = 0; 
  virtual bool empty() {return size() == 0;} 
  virtual int front() = 0; 
  virtual int back() = 0; 
  virtual void eraseFront() = 0; 
  virtual void eraseBack() = 0;
};

class RankSet: public TaskSet {
 protected: 
  strictCompare rankCmp;
  std::set<int, strictCompare> set; 
 public: 
  RankSet(Instance& ins, intCompare* cmp); 
  void insert(int task) ; 
  void erase(int task);
  bool compare(int taskA, int taskB);
  int size(); 
  int front(); 
  int back(); 
  void eraseFront(); 
  void eraseBack();
};

class FifoSet : public TaskSet {
 protected:
  int cnt; 
  std::deque<int> queue; 
  std::vector<int> order; 
 public: 
  FifoSet(Instance& ins); 
  void insert(int task); 
  void erase(int task);
  bool compare(int taskA, int taskB); 
  int size();
  int front(); 
  int back(); 
  void eraseFront(); 
  void eraseBack();  
};

class RankComputer {
 protected: 
  std::string rankOpt; 
  bool isReverse(std::string& arg); 
  virtual intCompare* oneCmp(Instance& ins, std::string subRankOpt); 
 public: 
  virtual TaskSet* makeSet(Instance& ins); 
  RankComputer(const AlgOptions &options);
};


class Bound {
 public:
  virtual double compute(Instance& instance)= 0; 
  virtual std::string name() = 0;
};

class ModifiableBound : public Bound {  
 public: 
  virtual void init(Instance& ins) = 0; 
  virtual double compute() = 0; 
  virtual void finalize() { }; 
  virtual void removeTask(int taskID) = 0; 
  virtual void restoreTask(int taskID) = 0;
  virtual double tryHard(int timeLimit) { return compute(); }
  double compute(Instance & ins) override {
    init(ins);
    double res = compute();
    finalize();
    return res; 
  }
};

#endif
