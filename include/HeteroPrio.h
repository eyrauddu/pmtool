#ifndef HETEROPRIO_H
#define HETEROPRIO_H
#include <map>
#include <string>
#include <vector>
#include <set>

#include "instance.h"
#include "algorithm.h"
#include "util.h"
typedef std::set<int, rankCompare> rankSet;

class HeteroPrio : public Algorithm {

  struct taskInfo {
    int nbDep; 
    /*    double readyTime; */
    double startTime; 
    double duration; 
    
  };
  struct workerInfo {
    // std::vector<int> queue; 
    int nbWorkers; 
    int firstID; 
    std::vector<int> currentTasks; 
    int runningTasks; 
    std::vector<int> RQorder; 
    std::vector<double> hetFactor; 
    bool isFast; 
    double speedIndex; /* lower means faster */
  };

  struct event {
    int taskID; 
    double date; 
    int workerType; 
    int workerID; 
  event(int t, double d, int wt, int wi): 
    taskID(t), date(d), workerType(wt), workerID(wi) {}
  };

  struct evCmp {
    bool operator() (const event& a, const event&b) {
      if(a.date == b.date) 
	return (a.taskID < b.taskID); 
      else return (a.date < b.date); 
    }
  };

  struct currentInfo {
    int taskID; 
    int wType; 
    int wLocalID; 
  currentInfo(int t, int wt, int wl): 
    taskID(t), wType(wt), wLocalID(wl) {}
  };


 protected:
  RankComputer ranker; 
  int verbosity; 
  double combinedFactor; 
  bool alwaysFromFront; 
  bool sortStealByAccFactor; 
  bool stealLatest; 
  bool stealIfIdle; 

  //  std::vector<double> priorities; 
  std::vector<taskInfo> tasks; 
  std::vector<workerInfo> workers; 
  std::vector<TaskSet*> readyQueues; 
  std::vector<int> workerPollOrder; /* they are sorted by speedIndex */
  std::set<event, evCmp> events; 

  std::vector< std::vector<int> > revDependencies;

  void init(Instance& ins); 
  currentInfo bestSteal(Instance &ins, int wType, int wLocalID, double currentTime); 
  int performSteal(currentInfo &l); 

 public: 
  HeteroPrio(const AlgOptions & options);
  double compute(Instance& ins, SchedAction* action);
};

#endif
