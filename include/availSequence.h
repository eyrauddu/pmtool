#ifndef AVAILSEQUENCE_H
#define AVAILSEQUENCE_H

#include <vector>
#include <ostream>

class Period {
 public: 
  double start; 
  double end; 
  Period() {} 
  Period(double s, double e) : start(s), end(e) {}
}; 
typedef std::vector<Period> timeSeq;


class AvailSequence {

  static constexpr double epsilon = 1e-9;
  
  /* Guarantee: seq always contains an infinitely long period (end = -1) at the end */
  timeSeq seq; 

 public: 
  
  AvailSequence(double start = 0); 
  timeSeq::iterator getAvail(double from, double len, double& start); 
  timeSeq::iterator getAvailLatest(double from, double len, double latest, 
				   double& start, timeSeq::iterator &i);
  timeSeq::iterator getAvailLatest(double from, double len, double latest, 
				   double& start) {
    timeSeq::iterator r = seq.begin(); 
    return getAvailLatest(from, len, latest, start, r); 
  }

  void insertBusy(timeSeq::iterator, double start, double len);
  void insertBusy(double start, double len);
  void display(std::ostream&); 
  
};




#endif
