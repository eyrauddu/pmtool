//
// Created by eyraud on 01/06/18.
//

#ifndef PMTOOL_SCHEDLPINDEP_H
#define PMTOOL_SCHEDLPINDEP_H


#include "algorithm.h"
#include "GreedyAlgorithm.h"
#include "PreAllocatedGreedy.h"

class SchedLPIndep: public GreedyAlgorithm {

    std::vector<std::vector<int> > preAssign; // preAssign[worker] is the vector of tasks assigned to worker

public:
    SchedLPIndep(AlgOptions opt);
    double compute(Instance &ins, SchedAction *action) override;

private:
    double limit;
    double gap;

    int chooseTask(int worker, double now) override;

    void onTaskPush(int task, double now) override;
};


#endif //PMTOOL_SCHEDLPINDEP_H
