//
// Created by eyraud on 30/08/18.
//

#ifndef PMTOOL_ONLINEQA_H
#define PMTOOL_ONLINEQA_H


#include "OnlineGeneric.h"

class OnlineQA : public OnlineGeneric {
public:
    OnlineQA(const AlgOptions &options);

    double compute(Instance &instance, SchedAction *action) override;
    int assignTask(int task, double now) override;
protected:
    double threshold;
};


#endif //PMTOOL_ONLINEQA_H
