//
// Created by eyraud on 18/12/18.
//

#ifndef PMTOOL_ITEMMANAGER_H
#define PMTOOL_ITEMMANAGER_H


#include "CommSequence.h"

class ItemManager {

    Instance* ins;

    std::vector<std::vector<double>> dataAvailTimes;
    // dataAvailTimes[i][n] is the availability of item i on memory node n
    // -1 if item is not present.

    std::vector<int> sourceLocation;
    // Redondant info to avoid recomputing it: sourceLocation[i] is the
    // index of the memory node on which i was produced


public:
    CommSequence comms;
    ItemManager(Instance &instance);

    double sendItemTo(int item, int dstMemNode, double currentTime);
    void produceItemOn(int item, int memNode, double currentTime);
};


#endif //PMTOOL_ITEMMANAGER_H
