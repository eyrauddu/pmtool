//
// Created by eyraud on 25/07/18.
//

#ifndef PMTOOL_TRUEHETEROPRIO_H
#define PMTOOL_TRUEHETEROPRIO_H


#include "GreedyAlgorithm.h"
typedef enum {LATEST, AF, PRIO} StealType;

class TrueHeteroPrio : public GreedyAlgorithm {
private:
    std::set<int, strictCompare> *taskSet;
    Instance* ins;
    std::vector<double> priorities;
    StealType stealType = LATEST;
    std::string rankString = "min";
    // bool stealIfNonEmpty = false;
    bool isBetterSteal(int taskA, double endA, int taskB, double endB, int wType);

 public:
    double compute(Instance &ins, SchedAction *action) override;
    TrueHeteroPrio(const AlgOptions &options);

    int chooseTask(int worker, double now) override;
    void onTaskPush(int task, double now) override;
};


#endif //PMTOOL_TRUEHETEROPRIO_H
