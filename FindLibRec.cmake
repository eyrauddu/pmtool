# This module finds librec.
#
# User can give LIBREC_INSTALL_DIR as a hint stored in the cmake cache.
#
# It sets the following variables:
#  LIBREC_FOUND              - Set to false, or undefined, if librec isn't found.
#  LIBREC_INCLUDE_DIRS       - include directory
#  LIBREC_LIBRARIES          - library files

set(LIBREC_INSTALL_DIR "" CACHE PATH "Librec install directory")

# Include dir
find_path(LIBREC_INCLUDE_DIR
  NAMES rec.h
  HINTS ${LIBREC_INSTALL_DIR}/include
  PATHS ENV C_INCLUDE_PATH
  ENV C_PLUS_INCLUDE_PATH
  ENV INCLUDE_PATH
)

# Finally the library itself
find_library(LIBREC_LIBRARY
  NAMES rec librec
  HINTS ${LIBREC_INSTALL_DIR}/lib
  PATHS ENV LIBRARY_PATH
  ENV LD_LIBRARY_PATH
)

message(STATUS "librec library: ${LIBREC_LIBRARY}")

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBREC_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LibRec  DEFAULT_MSG
                                  LIBREC_LIBRARY LIBREC_INCLUDE_DIR)

mark_as_advanced(LIBREC_INCLUDE_DIR LIBREC_LIBRARY )
if(LIBREC_FOUND)
  set(LIBREC_LIBRARIES ${LIBREC_LIBRARY} )
  set(LIBREC_INCLUDE_DIRS ${LIBREC_INCLUDE_DIR} )
endif()


