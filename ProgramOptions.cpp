#include "ProgramOptions.h"
#include <getopt.h>
#include <iostream>
#include <OnlineZhang.h>
#include <TrueHeteroPrio.h>
#include <OnlineQA.h>
#include <OnlineECT.h>
#include <OnlineERLS.h>
#include <OnlineLG.h>
#include <OnlineMG.h>

#include "algorithm.h"
#include "listAlgorithm.h"
#include "HeftAlgorithm.h"
#include "HybridBound.h"
#include "HeteroPrio.h"
#include "IndepBased.h"
#include "Dmdas.h"
#include "PreAllocatedGreedy.h"
// #include "MaxArea.h"
#include "ReproduceAlgorithm.h"


#ifdef WITH_CPLEX
#include "CriticalPath.h"
#include "AreaBound.h"
#include "DepBound.h"
#include "IterDepBound.h"
#include "AreaStart.h"
#include "IntervalBound.h"
#include "SchedLPIndep.h"
#endif

using namespace std; 

static const int opt_no_convert = 10;
static const int opt_no_dependencies = 11;
static const int opt_no_header = 12;
static const int opt_rev_dep = 13;
static const int opt_best = 14;
static const int opt_thread = 16;
static const int opt_tags = 17;
static const int opt_submit = 18;
static const int opt_export_type = 19;
static const int opt_export_order = 20;


static struct option long_options[] = {
  {"verbose",   required_argument,       0,  'v' },
  {"opt",  required_argument, 0, 'o'},
  {"no-convert",   no_argument, 0,  opt_no_convert},
  {"no-dep", no_argument, 0, opt_no_dependencies},
  {"no-header", no_argument, 0, opt_no_header},
  {"alg", required_argument, 0, 'a'},
  {"bound", required_argument, 0, 'b'},
  {"save", required_argument, 0, 's'},
  {"repartition", required_argument, 0, 'r'},
  {"tolerance", required_argument, 0, 't'},
  {"output-raw", no_argument, 0, 'w'},
  {"rev-dep", no_argument, 0, opt_rev_dep},
  {"platform", required_argument, 0, 'p'},
  {"help", no_argument, 0, 'h'},
  {"default", optional_argument, 0, 'd'},
  {"best",    optional_argument, 0, opt_best},
  {"threads", optional_argument, 0, opt_thread},
  {"use-tags", no_argument, 0, opt_tags},
  {"submit-order", no_argument, 0, opt_submit},
  {"export-type", no_argument, 0, opt_export_type},
  {"export-order", no_argument, 0, opt_export_order},
  {0,         0,                 0,  0 }
};

static const char* optstring = "v:o:a:b:s:r:t:wp:hd:";

ProgramOptions::ProgramOptions() {
  convertIndices = 1;
  verbosity = 0;
  noDependencies = false;
  repartitionFile = "";
  mergeTolerance = 0.01;
  outputNamesRaw = 0; 
  noHeader = false; 
  optRevDep = false;
  outputBest = false;
  nbThreads = 1;
  useSubmitOrder = false;
  appendTags = false;
  outputTypeInExport = false;
  workerOrderInExport = false;
}

void ProgramOptions::parse(int argc, char** argv) {
  
  // Read command-line parameters
  int longindex = 0;
  int opt = getopt_long(argc, argv, optstring, long_options, & longindex);
  string s_optarg;
  while(opt != -1) {
    if(optarg)
      s_optarg = string(optarg);
    switch(opt) {
    case 'v': 
      globalOptions.insert("verbosity", s_optarg);
      verbosity = stoi(s_optarg);
      break;
    case 'a':
      insertAlg(s_optarg); 
      break;
    case 'b':
      insertBound(s_optarg); 
      break;
    case 'o': 
      globalOptions.parse(s_optarg); 
      break; 
    case 's':
      saveFile = s_optarg;
      break;
    case 'r':
      repartitionFile = s_optarg;
      break;
    case opt_no_convert:
      convertIndices = 0;
      break;
    case opt_no_dependencies:
      noDependencies = true;
      break;
    case opt_no_header:
      noHeader=true; 
      break;
    case 't':
      mergeTolerance = atof(optarg);
      break;
    case 'w': 
      outputNamesRaw = 1; 
      break; 
    case 'p': 
      platformFiles.push_back(s_optarg); 
      break; 
    case opt_rev_dep:
      optRevDep = true;
    case 'd':
      if(!optarg || s_optarg == "normal") {
	insertBound("dep");
	insertAlg("heft:rank=min,heft");
	insertAlg("hetprio:rank=min,heft");
          insertAlg("hlp:rank=min,heft,alloc");
      }
      else if(s_optarg == "fast") {
	insertBound("area");
	insertAlg("heft:rank=min");
	insertAlg("hetprio:rank=min");
      }
      else if(s_optarg =="slow") {
	insertBound("dep");
	insertBound("dep:hybrid=no:share=depNoHybrid");
	insertBound("interval:pre=6:post=6:gap=0.05:share=ival");
	insertAlg("heft:rank=min,heft");
	insertAlg("hetprio:rank=min,heft");
	if(globalOptions.isPresent("stealerName")) {
	  insertAlg("greedy:key=ival[alloc]:rank=alloc");
	  insertAlg("greedy:ley=depNoHybrid[alloc]:rank=alloc");
	} else {
	  insertAlg("greedy:key=ival[alloc]:rank=alloc:stealerName=cuda");
	  insertAlg("greedy:key=depNoHybrid[alloc]:rank=alloc:stealerName=cuda");
	}
      } else {
	cerr << "Unknown default specification: '" << s_optarg <<"'. Valid values: 'slow', 'normal', 'fast'" << endl; 
      }
      break;
    case opt_best:
      outputBest = true;
      if(optarg)
	outputBestFile = s_optarg; 
      break ;
    case opt_thread:
      if(optarg)
	nbThreads = stoi(s_optarg);
      else
	nbThreads = -1;
      break;
    case opt_tags:
      appendTags = true;
      break;
    case opt_submit:
      useSubmitOrder = true;
      break;
    case opt_export_type:
      outputTypeInExport = true;
      break; 
    case opt_export_order:
      workerOrderInExport = true;
      break; 
    case '?':
    case 'h':
      usage(); 
    default:
      break;
    }
    opt = getopt_long(argc, argv, optstring, long_options, & longindex);
  }
  for(int a = optind; a < argc; a++) {
    inputFiles.push_back(string(argv[a]));
  }
}


string ProgramOptions::buildName(AlgOptions opts, bool forceRaw) {
    string result;
    if(forceRaw || outputNamesRaw) {
        result = opts.serialize();
    } else {
        result = opts.asString("name");
        for(string s: optionKeys) {
            result = result.append(" ").append(opts.asString(s, "NA"));
        }
    }
    return result;
}

void ProgramOptions::usage() {
  fprintf(stderr, "Usage: pmtool [INPUT FILES] [OPTIONS] \n\nCommon options:\n");
  fprintf(stderr, "  -a <alg:opt=val:...> \t\t\t Use algorithm <alg> with option <opt> set to <val>\n");
  fprintf(stderr, "  -b <bound:opt=val:...> \t\t Use bound <bound> with option <opt> set to <val>\n");
  fprintf(stderr, "  -o <opt=val> \t\t\t\t Apply option <opt> to all following algs and bounds\n");
  fprintf(stderr, "  -d <type> --default=<type> \t\t Use default lists of bounds and algorithms, where <type> is one of 'slow', 'normal', 'fast'.\n");
  fprintf(stderr, "  -p platformFile \t\t\t Use the given platform file\n");
  fprintf(stderr, "  -v <level> --verbose=<level> \t\t Verbosity level\n");
  fprintf(stderr, "  -s <saveFile> --save=<saveFile> \t Provide a file to save the schedules\n");
  fprintf(stderr, "  -t <tol> --tolerance=<tol> \t\t Tolerance for merging resources into the same type.\n");
  fprintf(stderr, "  --no-header \t\t\t\t Does not output a header\n");
  fprintf(stderr, "  --threads=<nb> \t\t\t Use <nb> threads in parallel for algorithms\n");
  
  fprintf(stderr, "\n"); 
  displayAlgList(); 
  fprintf(stderr, "\n"); 
  displayBoundList(); 
  fprintf(stderr, "\nFor more detail, see the README.md file.\n");
  exit(1); 
}

void ProgramOptions::displayAlgList() {
  cerr << "Algorithms available:" << endl;
  cerr << "list" << "\t" << "HEFT-list algorithm" <<endl;
  cerr << "    " << "\t" << "Options: rank" << endl;
  cerr << "heft" << "\t" << "HEFT-strict algorithm" <<endl;
  cerr << "    " << "\t" << "Options: rank" << endl;
  cerr << "hetprio" << "\t" << "HeteroPrio algorithm" <<endl;
  cerr << "    " << "\t" << "Options: rank, combFactor" << endl;
  cerr << "indep" << "\t" << "Sucessive calls to independent tasks alg" << endl;
  cerr << "    " << "\t" << "Options: indep, verbosity, rank, style" << endl;
  cerr << "dmdas" << "\t" << "Somewhat close to *PU original dmdas" << endl;
  cerr << "    " << "\t" << "Options: verbosity, rank" << endl;
  cerr << "zhang" << "\t" << "Online implementation of Zhang" << endl;
  cerr << "    " << "\t" << "Options: verbosity, rank" << endl;
}

void ProgramOptions::displayBoundList() {
  cerr << "Bounds available:" << endl;
#ifdef WITH_CPLEX
  cerr << "  area \t\t area bound, hybridized" << endl;
  cerr << "  cp   \t\t critical path" << endl; 
  cerr << "  dep  \t\t global area bound with dependencies, hybridized" << endl;
  cerr << "  iterdep \t global area bound with dependencies, hybridized + iteratively adding local area bounds" << endl;
  cerr << "  mixed \t compute time to end for all tasks, and area bounds for the beginning of the graph" << endl;
  cerr << "  interval \t same as dep bound, with intervals along a critical path to strengthen the area constraint" << endl;
#else
  cerr << endl << "To be able to use bounds, you have to compile with CPLEX."
       << endl << "Provide the -DCPLEX_ROOT_DIR= argument when calling cmake"
       << endl; 
#endif
}


Bound* createBound(const string& name, const AlgOptions& options) {
  Bound* bound = NULL;
#ifdef WITH_CPLEX
  if(name == "iterdep")
    bound = new HybridBound(new IterDepBound(options), options);
  if(name == "dep")
    bound = new HybridBound(new DepBound(options), options);
  if(name == "area")
    bound = new HybridBound(new AreaBound(options), options);
  if(name == "mixed")
    bound = new HybridBound(new AreaStart(options), options);
  if(name == "interval")
    bound = new IntervalBound(options);
  if(name == "cp")
    bound = new CriticalPath(options); 
#endif
  if(bound == NULL){
    cerr << "Unknown bound " << name <<". For a list of bounds, use --help" << endl;
    exit(-1);
  }
  return bound;
}

Algorithm* createAlg(const string& name, const AlgOptions& options) {
  Algorithm* alg = NULL;
  if(name == "heft")
    alg = new HeftAlgorithm(options);
  if(name == "list")
    alg = new ListAlgorithm(options);
  if(name == "hetprio")
    alg = new HeteroPrio(options);
  if(name == "truehp")
      alg = new TrueHeteroPrio(options);
  if(name == "indep")
    alg =  makeIndep(options);
  if(name == "dmdas") 
    alg = new Dmdas(options); 
  if(name == "greedy")
    alg = new PreAllocatedGreedy(options);
  if(name == "zhang")
    alg = new OnlineZhang(options);
  if(name == "rep")
    alg = new ReproduceAlgorithm(options);
  if(name == "qa")
    alg = new OnlineQA(options);
  if(name == "ect")
    alg = new OnlineECT(options);
  if(name == "erls")
      alg = new OnlineERLS(options);
  if(name == "lg")
      alg = new OnlineLG(options);
  if(name == "mg")
      alg = new OnlineMG(options);
#ifdef WITH_CPLEX
  if(name == "lp")
    alg = new SchedLPIndep(options);
#endif
 if(alg == NULL){
    cerr << "Unknown algorithm " << name <<". For a list of algorithms, use --help" << endl;
    exit(-1);
  }
  return alg;
}


template <class A, typename Func> 
void ProgramOptions::insertComputable(vector< pair<A, AlgOptions> > & elems, string description, Func func) {
    string name;
  string additionalOptions;
  vector<AlgOptions> optList =
    globalOptions.multiParseWithName(description,  name);
  if(name == "hlp") {
      static bool hlpAlreadyUsed = false;
    if(! hlpAlreadyUsed) {
        insertBound("dep:hybrid=no:share=depNoHybrid");
        hlpAlreadyUsed = true;
    }
    name = "greedy";
    additionalOptions = "key=depNoHybrid[alloc]";
  }
  if(name == "hlpb") {
      static bool hlpbAlreadyUsed = false;
    if(! hlpbAlreadyUsed) {
        insertBound("dep:hybrid=no:share=depNoHybridBversion:bversion=true");
        hlpbAlreadyUsed = true;
    }
    name = "greedy";
    additionalOptions = "key=depNoHybridBversion[alloc]";
  }
  for(AlgOptions options: optList) {
    if(!additionalOptions.empty())
      options.parse(additionalOptions);
    elems.push_back(make_pair(func(name, options), options));
    for(auto & s: options)
        if(s.first != "name")
            optionKeys.insert(s.first);
  }
}

void ProgramOptions::insertAlg(string description) {
  insertComputable(algs, description, createAlg);
}
void ProgramOptions::insertBound(string description) {
  insertComputable(bounds, description, createBound);
}
