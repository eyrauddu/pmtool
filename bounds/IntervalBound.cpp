#include "IntervalBound.h"

using namespace std;

IntervalBound::IntervalBound(const AlgOptions &options) {
  verbosity = options.asInt("verbosity", 1); 
  weight = options.asString("weight", "min"); 
  if(weight != "min" && weight != "heft") {
    cerr << "IntervalBound: unknown weight " << weight <<". Using min" << endl; 
    weight = "min"; 
  }
  limit = options.asInt("limit", 0); 
  gap = options.asDouble("gap", -1); 
  nbIntervalsStart = options.asInt("pre", -1); 
  nbIntervalsEnd = options.asInt("post", -1); 
  addPaths = options.asString("paths", "yes") == "yes";
  shareName = options.asString("share");
  saveName = options.asString("save"); 
}

string IntervalBound::name() {
  return "interval"; 
}

void IntervalBound::clear() {
  env.end();
  env=IloEnv(); 
  critPath.clear(); 
}


// Interval l is between IVar(l) and IVar(l+1)
// l between 0 and nbInt
// Task #k in CP executes in interval 2*k+1
IloExpr IntervalBound::getIntervalVar(int l) {
  if(l == 0){
    IloExpr e = IloExpr(env); 
    return e; 
  }
  if(l == nbIntervals)
    return load; 
  if(l % 2 == 1) {
    int t = critPath[(l-1)/2]; 
    return startTime[t]; 
  }
  else {
    int t = critPath[(l/2)-1]; 
    return (startTime[t] + execTime[t]);
  }
}

double IntervalBound::compute(Instance & ins) {
  if(instance) clear();
  instance = &ins; 
  nbWorkerTypes = instance->nbWorkerTypes;
  nbTasks = instance->nbTasks;
  if(verbosity >= 3) 
    cerr << "IntBound: Computing ancestors..." << endl;
  vector< vector<int> > ancestors = instance->getAncestors(); 
  if(verbosity >= 3) 
    cerr << "IntBound: done ancestors" << endl; 

  // First find a critical path
  vector<double> w; 
  if(weight == "heft")
    w = ins.computeHEFTRank();
  else if(weight == "min")
    w = ins.computeMinRank(); 
  ins.computeLongestPath(w, false, &critPath);

  vector<double> wbar(nbTasks, 0);
  for(int i = 0; i < nbTasks; i++) {
    wbar[i] = ins.execType(0, i);
    for(int j = 1; j < nbWorkerTypes; j++)
      wbar[i] = min(wbar[i], ins.execType(j, i));
  }


  vector<vector<int> > pathsToCP; 
  if(addPaths) {
    vector<bool> touchedByPaths(nbTasks, false); 
    vector<int> topOrder = ins.getTopOrder();
    for(auto& t: critPath) touchedByPaths[t] = true; 
    for(unsigned int i = 0; i < nbTasks; i++) {
      if(!touchedByPaths[i]) {
	int from = -1; unsigned int j; int to = -1;
	for(j = 0; j < critPath.size(); j++) {
	  if(ancestors[i][critPath[j]] == 1) from = critPath[j]; 
	  else if(from != -1)
	    break; 
	}
	for(; j < critPath.size(); j++) {
	  if(ancestors[critPath[j]][i] == 1) {
	    to = critPath[j]; 
	    break; 
	  }
	}
	vector<int> path; 
	ins.computeLongestPath(wbar, false, &path, i, from);
	path.pop_back(); 
	ins.computeLongestPath(wbar, false, &path, to, i); 
	for(auto &k: path) touchedByPaths[k] = true; 
	pathsToCP.push_back(path); 
	if(verbosity >= 6) 
	  cerr << "IntBound: path added for " << i << " : " << path << endl; 
      }
    }
  }

  if(nbIntervalsStart >= 0 && nbIntervalsEnd >= 0)
    critPath.erase(critPath.begin() + nbIntervalsStart, critPath.end() - nbIntervalsEnd); 
  unsigned int lp = critPath.size();

  if(verbosity >= 3) 
    cerr << "IntBound: critPath = " << critPath << endl; 

  nbIntervals = 2*lp + 1; 

  model = IloModel(env);
  // Declaring variables
  useByInterval = NumVarMatrix3D(env, nbTasks); 
  wType = NumVarMatrix(env, nbTasks); 
  startTime = IloNumVarArray(env, nbTasks, 0.0, IloInfinity, ILOFLOAT); 
  load = IloNumVar(env); 
  execTime = IloExprArray(env, nbTasks); 

  vector< vector<int> > isPossible(nbTasks, vector<int>(nbIntervals, 1)); 


  if(verbosity >= 3)
    cerr << "IntBound: Generating variables with " << nbIntervals << " intervals..." << endl; 

  for(int i = 0; i < nbTasks; i++) {
    useByInterval[i] = NumVarMatrix(env, nbWorkerTypes); 
    wType[i] = IloNumVarArray(env, nbWorkerTypes, 0.0, 1.0, ILOFLOAT); 
    for(int j = 0; j < nbWorkerTypes; j++) 
      useByInterval[i][j] = IloNumVarArray(env, nbIntervals, 0.0, IloInfinity, ILOFLOAT);
  }

  if(verbosity >= 3)
    cerr << "IntBound: Generating interval fraction expressions..." << endl; 
  IloArray< IloArray<IloExpr> > fractionUpToInterval(env, nbTasks); 
  for(int i = 0; i < nbTasks; i++) {
    fractionUpToInterval[i] = IloArray<IloExpr>(env, nbIntervals); 
    fractionUpToInterval[i][0] = IloExpr(env); 
    for(int l = 0; l < nbIntervals; l++) {
      if(l > 0) fractionUpToInterval[i][l] = fractionUpToInterval[i][l-1]; 
      for(int r = 0; r < nbWorkerTypes; r++) 
	if(instance->isValidType(r, i))
	  fractionUpToInterval[i][l] += useByInterval[i][r][l] / instance->execType(r, i); 
    }
  }


  if(verbosity >= 3)
    cerr << "IntBound: Generating constraints ..." << endl;

  // Constraints: 
  for(int i = 0; i < nbTasks; i++) {
    //  Each task is performed only once
    model.add(IloSum(wType[i]) == 1); 
    execTime[i] = IloExpr(env); 
    for(int r = 0; r < nbWorkerTypes; r++) {
      if(instance->isValidType(r, i)) {
	// Total usage over all intervals is equal to exec time.
	model.add(IloSum(useByInterval[i][r]) == 
		  wType[i][r] * instance->execType(r, i));
	// ExecTime is the sum over all resources.
	execTime[i] += wType[i][r] * instance->execType(r, i); 
      } else {
	model.add(IloSum(useByInterval[i][r]) == 0);
	model.add(wType[i][r] == 0); 
      }
    }

    for(auto & prec : instance->dependencies[i]) {
      model.add(startTime[i] >= startTime[prec] + execTime[prec]);
      for(int l = 0; l < nbIntervals; l++) 
	model.add(fractionUpToInterval[prec][l] >= fractionUpToInterval[i][l]);
    }

    // Total finish time is more than any finish time
    model.add(load >= startTime[i] + execTime[i]);

    for(int k = 0; k < lp; k++) {
      // Tasks that release a task in the CP cannot be executing after it starts. 
      if(ancestors[critPath[k]][i] == 1 && i != critPath[k]){
	int l = 2*k+1; // This is the interval in which critPath[k] executes
	for(int m = l; m < nbIntervals; m++) 
	  for(int r = 0; r < nbWorkerTypes; r++) {
	    model.add(useByInterval[i][r][m] == 0); 
	    isPossible[i][m] = 0; 
	  }
      }
      // Tasks that depend on a task in CP cannot execute until after it's done
      if(ancestors[i][critPath[k]] == 1 && i != critPath[k]) {
	int l = 2*k+1;  // This is the interval in which critPath[k] executes
	for(int m = 0; m <= l; m++) 
	  for(int r = 0; r < nbWorkerTypes; r++) {
	    model.add(useByInterval[i][r][m] == 0); 
	    isPossible[i][m] = 0; 
	  }

      }
	      
    }
  }

  if(verbosity >= 3) 
    cerr << "IntBound: done  constraints" << endl; 


  if(addPaths) 
    for(auto& path:pathsToCP) {
      for(int l = 0; l < nbIntervals; l++) {
	IloExpr len(env); 
	for(auto & i: path) {
	  for(int r = 0; r < nbWorkerTypes; r++) 
	    len += useByInterval[i][r][l]; 
	}
	model.add(len <= getIntervalVar(l+1) - getIntervalVar(l));
      }
    }

  // Tasks in the CP can only execute in their interval. 
  for(int k = 0; k < lp; k++) 
    for(int m = 0; m < nbIntervals; m++) 
      if(m != 2*k+1)
	for(int r = 0; r < nbWorkerTypes; r++) {
	  model.add(useByInterval[critPath[k]][r][m] == 0); 
	  isPossible[critPath[k]][m] = 0; 
	}

    
  for(int l = 0; l < nbIntervals; l++) {
    for(int r = 0; r < nbWorkerTypes; r++) {
      IloExpr usage = IloExpr(env); 
      for(int i = 0; i < nbTasks; i++) {
	usage += useByInterval[i][r][l]; 
      }
      // Total usage in interval is less than available in interval
      model.add(usage <= (getIntervalVar(l+1) - getIntervalVar(l)) * instance->nbWorkers[r]); 
    }		  
  }

  if(verbosity >= 7){
    cerr << "Is Possible: " << endl; 
    for(int i = 0; i < nbTasks; i++) {
      cerr << i << " " << isPossible[i] << endl; 
    }
  }
    
    
  // Objective function & params
  IloObjective objective = IloMinimize(env, load);
  model.add(objective);
  IloCplex modcplex = IloCplex(model);
  if(verbosity < 5) 
    modcplex.setOut(env.getNullStream());

  double result = 0; 
  
  if(limit > 0 || gap > 0) {
    for(int i = 0; i < nbTasks; i++) 
      model.add(IloConversion(env, wType[i], ILOBOOL));
  }
  if(limit > 0) 
    modcplex.setParam(IloCplex::TiLim, limit);
  if(gap > 0)
    modcplex.setParam(IloCplex::EpGap, gap); 


  if(verbosity >= 6) 
    cerr << "IntervalBound: Going for solve..." << endl; 
  IloBool b = modcplex.solve();
  if(verbosity >= 5) {
    cerr << "solve status: " << b << " " << modcplex.getStatus() << " " << modcplex.getCplexStatus() << endl;
  }
  if(limit > 0 || gap > 0) {
    result = modcplex.getBestObjValue();
    if(verbosity >= 1) {
      double intSol = modcplex.getObjValue(); 
      cerr << "IntervalBound: integer solution: " << intSol << " best bound: " << result << " gap:" << (intSol - result)/result << endl; 	
    }
  }
  else 
    result = modcplex.getObjValue();

  /* Now, let us fill the instance data with the results of this */
  vector<int> alloc(nbTasks, -1); 
  vector<double> averageInterval(nbTasks, 0); 
  vector<double> medianInterval(nbTasks, -1); 
  
  /* Get values */
  IloArray< IloArray< IloNumArray > > useByIntervalVal(env, nbTasks); 
  IloArray<IloNumArray> wTypeVal(env, nbTasks); 
  for(int i = 0; i < nbTasks; i++) {
    useByIntervalVal[i] = IloArray<IloNumArray>(env, nbWorkerTypes); 
    wTypeVal[i] = IloNumArray(env); 
    modcplex.getValues(wTypeVal[i], wType[i]);
    for(int r = 0; r < nbWorkerTypes; r++) {
      useByIntervalVal[i][r] = IloNumArray(env); 
      modcplex.getValues(useByIntervalVal[i][r], useByInterval[i][r]);
    }
  }
  for(int i = 0; i < nbTasks; i++) {
    alloc[i] = 0; 
    for(int r = 1; r < nbWorkerTypes; r++) 
      if(wTypeVal[i][r] > wTypeVal[i][alloc[i]]) 
	alloc[i] = r; 
    double cumulFraction = 0; 
    for(int l = 0; l < nbIntervals; l++) {
      double fraction = 0; 
      for(int r = 0; r < nbWorkerTypes; r++) 
	if(instance->isValidType(r, i))
	  fraction += useByIntervalVal[i][r][l] / instance->execType(r, i); 
      cumulFraction += fraction; 
      averageInterval[i] += fraction * l; 
      if(cumulFraction > 0.5 && medianInterval[i] == -1)
	medianInterval[i] = l;
    }
  }

  if(verbosity >= 6) {
    cerr << "IntBound: alloc: " << alloc << endl; 
    cerr << "IntBound: med: " << medianInterval << endl; 
    cerr << "IntBound: avg: " << averageInterval << endl; 
  }

  if(!shareName.empty()) {
    instance->extraData.insert(shareName + "[alloc]", new vector<int>(alloc)); 
    instance->extraData.insert(shareName + "[med]", new vector<double>(medianInterval)); 
    instance->extraData.insert(shareName + "[avg]", new vector<double>(averageInterval)); 
  }

  if(!saveName.empty()) {
    ofstream output(saveName); 
    for(int &i: alloc)
      output << i << endl; 
    output.close();
  }

  return result; 

}
