#include <vector>
#include <ilcplex/ilocplex.h>

#include "algorithm.h"
#include "AreaBound.h"

using namespace std;

ILOSTLBEGIN

AreaBound::AreaBound(const AlgOptions & options) {
  verbosity = options.asInt("verbosity", 0); 
  share = options.asString("share", "");
}

void AreaBound::clear() {
  countTasks.clear();
  env.end(); 
  env = IloEnv(); 
}

void AreaBound::init(Instance& inputIns) {
  if(ins) clear();
  ins = &inputIns; 
  nbWorkerTypes = ins->nbWorkerTypes;
  nbTaskTypes = ins->nbTaskTypes;
  indepModel = IloModel(env); 
  qtityConstraints = IloRangeArray(env, nbTaskTypes);


  countTasks.insert(countTasks.begin(), nbTaskTypes, 0); 

  int i, j; 
  for(i = 0; i < ins->nbTasks; i++) countTasks[ins->taskTypes[i]]++;
  wType = NumVarMatrix(env, nbTaskTypes);
  IloNumVar load(env, 0.0, IloInfinity, ILOFLOAT);
  
  // Quantity constraints
  for(i = 0; i < nbTaskTypes; i++) {
    wType[i] = IloNumVarArray(env, nbWorkerTypes, 0.0, IloInfinity, ILOINT);
    qtityConstraints[i] = (IloSum(wType[i]) == countTasks[i]);
    indepModel.add(qtityConstraints[i]);
  }
  
    // load constraints
    for(j = 0; j < nbWorkerTypes; j++) {
      IloExpr e(env);
      for(i = 0; i < nbTaskTypes; i++) {
	double v = ins->execTimes[j][i];
	if(ins->isValidValue(v)) {
	  e += wType[i][j] * v; 
	} else {
	  indepModel.add(wType[i][j] == 0); 
	}
      }
      indepModel.add(e / ins->nbWorkers[j] <= load);
      e.end();
    }

    // objective function & params
    indepModel.add(IloMinimize(env, load));
    
    indepCplex = IloCplex(indepModel); 
    if(verbosity <= 6) 
      indepCplex.setOut(env.getNullStream());
  }
double AreaBound::compute() {
  // Update linear model (quantity constraints are 
  // equality constraints)
  int i; 
  for(i = 0; i < nbTaskTypes; i++) {
    qtityConstraints[i].setBounds(countTasks[i], countTasks[i]);
  }
  
  if(indepCplex.getNMIPStarts() > 0) 
    indepCplex.deleteMIPStarts(0, indepCplex.getNMIPStarts());
  
  // Compute the pure area bound
  indepCplex.solve();

  double result = indepCplex.getObjValue();
  return(result); 
}  

void AreaBound::finalize() {
    if (!share.empty()) {
      vector<vector<double>> repart = getRepartition();
        ins->extraData.insert(share + "[repart]", (void *) new vector<vector<double>>(repart));
    }
}

void AreaBound::removeTask(int taskID) { 
  countTasks[ins->taskTypes[taskID]]--;
}

void AreaBound::restoreTask(int taskID) { 
  countTasks[ins->taskTypes[taskID]]++;
}
  
string AreaBound::name() {
  return "area"; 
}



std::vector<std::vector<double>> AreaBound::getRepartition() {
  vector<vector<double>> result(nbTaskTypes, vector<double>(nbWorkerTypes, -1));
  IloArray<IloNumArray> values(env, nbTaskTypes);
  for (int i = 0; i < nbTaskTypes; i++) {
    values[i] = IloNumArray(env);
    indepCplex.getValues(values[i], wType[i]);
    for (int j = 0; j < nbWorkerTypes; j++)
      result[i][j] = values[i][j];
  }

  return result;
}
