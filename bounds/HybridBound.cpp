#include <iostream>
#include <vector>
#include <set>
#include <limits>
#include "HybridBound.h"

using namespace std;

HybridBound::HybridBound(ModifiableBound *_alg, const AlgOptions &options) : 
  alg(_alg) {
  verbosity = options.asInt("verbosity", 0); 
  timeLimit = options.asInt("limit", 0);
  string hybridStr = options.asString("hybrid", "yes"); 
  if(hybridStr == "yes") {
    doHybrid = true; 
  } else if(hybridStr == "no") {
    doHybrid = false; 
  } else {
    cerr << "HybridBound: hybrid option should be yes or no, not " << hybridStr << ". Defaulting to yes." << endl; 
    doHybrid = true;
  }
  shareKey = options.asString("share", ""); 
}

string HybridBound::name() {
  return alg->name(); 
}

double HybridBound::compute(Instance& ins) {

  if(!doHybrid || ins.isIndependent) {
    double result; 
    alg->init(ins); 
    if(timeLimit > 0) 
      result = alg->tryHard(timeLimit); 
    else
      result = alg->compute();
    alg->finalize();
    return result;
  }

  unsigned int nbTask = ins.nbTasks;
  unsigned int nbTaskTypes = ins.nbTaskTypes;
  unsigned int nbWorkerTypes = ins.nbWorkerTypes;

  vector<int> topOrder = ins.getTopOrder();
  vector< vector<int> > revDep = ins.getRevDependencies(); 

  double hybridBound; 
  
  // First: compute minimum task times
  vector<double> wmin(nbTaskTypes);
  vector<int> zmin(nbTaskTypes); 
  
  for(int i = 0; i < nbTaskTypes; i++) {
    wmin[i] = ins.execTimes[0][i]; 
    zmin[i] = 0; 
    for(int j = 1; j < nbWorkerTypes; j++)
      if(wmin[i] > ins.execTimes[j][i]) {
	wmin[i] = ins.execTimes[j][i];
	zmin[i] = j;
      }
  }
  
  if(verbosity >= 5) 
    cout << "HybridBound: Init algorithm..." << endl;  
  alg->init(ins); 
  if(verbosity >= 5) 
    cout << "HybridBound: ... computing first bound ..." << endl; 
  hybridBound = alg->compute();

  if(verbosity >= 1) 
    cout << "HybridBound: pure bound " << hybridBound << endl; 
  
  // First: Compute distances from start
  vector<double> distancesFromStart(nbTask); 
  for(int j = 0; j < nbTask; j++) {
    int i = topOrder[j];
    double d;
    if(ins.dependencies[i].empty())
      d = 0; 
    else {
      auto it = ins.dependencies[i].begin();
      d = distancesFromStart[*it];
      for(; it != ins.dependencies[i].end(); it++)
	d = max(d, distancesFromStart[*it]);
    }
    distancesFromStart[i] = d + wmin[ins.taskTypes[i]];
  }
  // Similarly, comput critRank, distances to end.
  vector<double> critRank = ins.computeCriticalPath(false, nullptr);
  
  if(verbosity >= 6) 
    cout << "HybridBound: Distances from start: " << distancesFromStart << endl;

  int sizeV1 = nbTask;
  set<int> removedTasks; 
  set<int> frontier; 
  // The frontier is the set of nodes in V_2 which have 
  // at least one successor in V_1.
  // From the start of the graph first.

  set<int> recentlyAdded;

  // Identify nodes without predecessors, 
  // place them on the first frontier
  for(int i = 0; i < nbTask; i++)
    if(ins.dependencies[i].empty()) {
      frontier.insert(i);
      recentlyAdded.insert(i);
      alg->removeTask(i);
      removedTasks.insert(i); 
    }
  sizeV1 -= frontier.size();
  

  int stepNum = 0; 
  double finalA = 0, finalB = hybridBound, finalC = 0; 
  int removedNode = -1; 
  while(sizeV1 > 0) {
    set<int>::iterator closestFrontierNodeIndex;
    double bBound = 0, aBound = 0; 
    double cBound = 0;

     if(verbosity >= 6) 
       cout << "HybridBound: Frontier for step " << stepNum << " : " << frontier << endl;
    
    // Compute critical path part of the bound
    // Equal to the smallest distance from start to a frontier node
    aBound = std::numeric_limits<double>::infinity(); 
    for(auto it = frontier.begin(); it != frontier.end(); it++) 
      if(aBound > distancesFromStart[*it]){
	aBound = distancesFromStart[*it];
	closestFrontierNodeIndex = it;
      }
    
    bBound = alg->compute();
    if(verbosity >= 4) 
      cout << "Start-Hybrid bound for step " << stepNum << " = " << aBound + bBound + cBound << " ( " << aBound << " + " << bBound << " + " << cBound << " )" << endl;
    
    //TODO : investigate stopping criterion !
    if(cBound + aBound + bBound >= hybridBound) {
      /* Bound improves, we record this. */
      hybridBound = aBound + bBound + cBound;
      finalA = aBound; 
      finalB = bBound; 
      finalC = cBound; 
    } else {
      /* Go back to previous state, bound was better ! */
      for(int task: recentlyAdded) {
	alg->restoreTask(task); 
	auto toErase = frontier.find(task); 
	if(toErase == frontier.end())
	  cout << "HybridBound: Big problem !! Should be in frontier, but isn't: " << task << endl;
	frontier.erase(toErase);
	removedTasks.erase(task); 
	sizeV1++; 
      }
      if(removedNode >= 0) 
	frontier.insert(removedNode);
      break;
    }

    recentlyAdded.clear();
    // Then compute the next frontier
    removedNode = *closestFrontierNodeIndex;
    frontier.erase(closestFrontierNodeIndex);
    for(int task: revDep[removedNode]) { 
      if(frontier.find(task) == frontier.end()) {
	frontier.insert(task);
	recentlyAdded.insert(task);
	alg->removeTask(task);
	removedTasks.insert(task); 
	sizeV1--;
      }
    }
    
    stepNum++;
  }

  if(verbosity >= 1) 
    cout << "HybridBound: Start-hybrid bound: " << hybridBound << " ( " << finalA << " + " << finalB << " + " << finalC << " )" << " for step " << stepNum << endl;

  // if(verbosity >= 3)
  //   cout << "Corresponding Frontier: " << frontier << endl;
  
  // Now : same thing, at the end of the graph.
  frontier.clear();
  recentlyAdded.clear(); 
 
 // Identify nodes without successors, place them on the first frontier
  for(int i = 0; i < nbTask; i++)
    if(revDep[i].empty()) {
      frontier.insert(i);
      recentlyAdded.insert(i); 
      alg->removeTask(i);
      removedTasks.insert(i); 
    }
  sizeV1 -= frontier.size();
  removedNode = -1; 

  stepNum = 0; 
  while(sizeV1 > 0) {
    double bBound = 0;
    double cBound = 0;
    set<int>::iterator closestFrontierNodeIndex;
    if(verbosity >= 6) 
      cout << "Frontier for step " << stepNum << " : " << frontier << endl;
	 
    // Compute critical path part of the bound
    cBound = std::numeric_limits<double>::infinity(); 
    for(auto it = frontier.begin(); it != frontier.end(); it++) 
      if(cBound > critRank[ *it ]){
	cBound = critRank[ *it ];
	closestFrontierNodeIndex = it;
      }
	 
    bBound = alg->compute();
	 
    if(verbosity >= 4) 
      cout << "Finish-Hybrid bound for step " << stepNum << " = " << finalA + bBound + cBound << " ( " << finalA << " + " << bBound << " + " << cBound << " )" << endl;
    
    if(finalA + bBound + cBound >= hybridBound) {
      /* Bound improves, we record this. */
      hybridBound = finalA + bBound + cBound;
      finalB = bBound; 
      finalC = cBound; 
    } else {
      /* Go back to previous state, bound was better ! */
      for(int task: recentlyAdded) {
	alg->restoreTask(task); 
	frontier.erase(task);
	removedTasks.erase(task); 
	sizeV1++; 
      }
      if(removedNode >= 0) 
	frontier.insert(removedNode);
      break;
    }
    
    recentlyAdded.clear();
    // Update the next frontier
    
    removedNode = *closestFrontierNodeIndex;
    frontier.erase(closestFrontierNodeIndex);
    for(int task:ins.dependencies[removedNode]) { 
      if(frontier.find(task) == frontier.end()) {
	frontier.insert(task);
	recentlyAdded.insert(task);
	alg->removeTask(task);
	sizeV1--;
	removedTasks.insert(task); 
      }
    }
    
    stepNum++;
  }

  if(timeLimit > 0) {
    finalB = alg->tryHard(timeLimit); 
    hybridBound = finalA + finalB + finalC; 
  } else {
    // Compute the final bound (last compute() call
    // was with too many removed tasks)
    alg->compute(); 
  }

  if(verbosity >= 6)
    cout << "HybridBound: finalizing..." << endl; 
  
  alg->finalize(); 
  
  if(!shareKey.empty()
     && ins.extraData.hasValue(shareKey + "[alloc]")) {
    // alg->compute() should have filled the share vector
    // with non-removed tasks
    vector<int>* alloc = (vector<int>*) ins.extraData.get(shareKey + "[alloc]");
    for(int i: removedTasks) {
      int t = ins.taskTypes[i];
      (*alloc)[i] = zmin[t];
    }
  }

  if(verbosity >= 1) 
    cout << "HybridBound: Finish-Hybrid bound: " << hybridBound << " ( " << finalA << " + " << finalB << " + " << finalC << " )" << " for step " << stepNum << endl;

      
  return hybridBound; 

}

