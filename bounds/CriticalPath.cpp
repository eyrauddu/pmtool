#include <vector>
#include "algorithm.h"
#include "CriticalPath.h"
#include "util.h"

using namespace std;

CriticalPath::CriticalPath(const AlgOptions & options) {
  
}

double CriticalPath::compute(Instance& ins) {
  vector<double> ranks = ins.computeMinRank();
  return getMax(ranks);
}
