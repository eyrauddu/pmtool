#include "IterDepBound.h"


using namespace std; 


void IterDepBound::init(Instance& ins) {
  DepBound::init(ins); 
  ancestors = ins.getAncestors(); 
  topOrder = ins.getTopOrder();
}

void IterDepBound::findViolatedArea(std::vector< IloNumArray> weights, 
				    IloNumArray startTimes, 
				    int& source, int& dst) {

    
    int u, v, w;
    for(int i = 0; i < nbTasks; i++) {
      u = topOrder[i]; 
      for(int j = i+1; j < nbTasks; j++) {
	v = topOrder[j]; 
	if(ancestors[v][u] == 1) {
	  std::vector<double> ws(nbWorkerTypes, 0); 
	  for(int k = i; k < j; k++) {
	    w = topOrder[k];
	    if((ancestors[w][u] == 1) && (ancestors[v][w] == 1) && weights[w].getSize() > 0)
	      for(int l = 0; l < nbWorkerTypes; l++)
		ws[l] += weights[w][l]; 
	  }
	  if(verbosity >= 6)
	    cout << "IDP: Area " << u << " " << v << " : " << ws << " " << startTimes[v] - startTimes[u] << endl; 
	  for(int l = 0; l < nbWorkerTypes; l++) 
	    if((ws[l] / instance->nbWorkers[l]) > startTimes[v] - startTimes[u] + 1e-7) {
	      source = u; 
	      dst = v; 
	      return; 
	    }
	}
      }
    }

}

double IterDepBound::compute() {
  
  double result = 0; 
  bool finished = false; 
  int step = 0; 
  
  while(not finished) {
    result = DepBound::compute();

    step++; 
    if(verbosity >= 3) 
      cout << "Bound in step " << step << " : " << result << endl;

    // Compute weights from current solution
    std::vector<IloNumArray> weights(nbTasks);
    IloNumArray starts(env); 
    idcCplex.getValues(starts, startTime);
    for(int i = 0; i < nbTasks; i++) {
      weights[i] = IloNumArray(env); 
      if(not isRemoved[i]) {
	idcCplex.getValues(weights[i], wType[i]); 
	for(int j = 0; j < nbWorkerTypes; j++) 
	  if(instance->isValidType(j, i)) 
	    weights[i][j] = weights[i][j] * instance->execType(j, i);
	  else 
	    weights[i][j] = 0; 
      }	
    }
    int s = -1, t = -1; 
    findViolatedArea(weights, starts, s, t);
    if(s != -1) {
      if(verbosity >= 3) 
	cout << "Violation found between " << s << " and " << t << endl; 
      std::vector<int> included(nbTasks, 0); 
      for(int k = 0; k < nbTasks; k++) {
	if((ancestors[k][s] == 1) && (ancestors[t][k] == 1) && (k!=t) && not isRemoved[k])
	  included[k] = 1; 
      }
      IloExprArray e(env, nbWorkerTypes);
      for(int l = 0; l < nbWorkerTypes; l++) e[l] = IloExpr(env);
      for(int k = 0; k < nbTasks; k++) 
	if(included[k] == 1)
	  for(int l = 0; l < nbWorkerTypes; l++) 
	    if(instance->isValidType(l, k)) {
	      e[l] += wType[k][l] * instance->execType(l, k);
	    }
      for(int l = 0; l < nbWorkerTypes; l++) {
	idc.add(startTime[t] - startTime[s] >= e[l] / instance->nbWorkers[l]);
      }
    } else {
      if(verbosity >= 3)
	cout << "Finished." << endl;
      finished = true; 
    }
    
    }
    return result; 

}
