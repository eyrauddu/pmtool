#include <iostream>


#include "util.h"
#include "AreaStart.h"

// Idea of this bound: compute max time to the end for each task
//   Sort these values
//   For each value, write an area bound with all tasks that must end
//   before that date to get an earliest starting time

using namespace std;

AreaStart::AreaStart(const AlgOptions & options): DepBound(options), area(options) {
  cout << "Creating areastart" << endl; 
  save = options.asString("save", ""); 
}

void AreaStart::init(Instance &ins) {
  DepBound::init(ins); 
  area.init(ins); 
  ancestors = ins.getAncestors(); 
}

double AreaStart::tryHard(int timeLimit) {
  // I am slightly bending the purpose of this, but never mind. I just
  // ignore the time limit

  // So. I have some removed tasks
  vector<bool> originallyRemoved(isRemoved); 
  vector<double> timeToEnd(nbTasks, -1); 


  // For all tasks that are not removed, keep only its (not originally
  // removed) descendants, and compute dep bound. 
  for(int i = 0; i < nbTasks; i++) {
    if(!originallyRemoved[i]) {
      for(int j = 0; j < nbTasks; j++) {
	if( !originallyRemoved[j] ) {
	  if ( (j == i) || (ancestors[j][i] != 1) ) {
	    if(!isRemoved[j]) removeTask(j);
	  } else {
	    if(isRemoved[j]) restoreTask(j); 
	  }
	}
      }
      timeToEnd[i] = compute(); 
    }
  }
  
  if(verbosity >= 2) {
    cout << "AreaStart: TimeToEnd: " << endl; 
    cout << timeToEnd << endl;
  }
  for(int i = 0; i < nbTasks; i++) {
    //    if(originallyRemoved[i]) {
      area.removeTask(i); 
      //    }
  }
  
  vector<double> bounds(nbTasks, -1); 
  double bestBound = -1; 
   

  for(int i = 0; i < nbTasks; i++) {
    if(!originallyRemoved[i]) {
      for(int j = 0; j < nbTasks; j++) {
	if( (!originallyRemoved[j])
	    && (timeToEnd[j] >= timeToEnd[i]) ) {
	  area.restoreTask(j); 
	}
      }
      
      bounds[i] = timeToEnd[i] + area.compute(); 
      if((bestBound == -1) || (bounds[i] > bestBound) )
	bestBound = bounds[i]; 
      
      for(int j = 0; j < nbTasks; j++) {
	if( (!originallyRemoved[j])
	    && (timeToEnd[j] >= timeToEnd[i]) ) {
	  area.removeTask(j); 
	}
      }

      
    }
  }


  if(!save.empty()) {
    ofstream o(save); 
    o << "Tid tte" << endl; 
    for(int i = 0; i < nbTasks; i++) {
      o << i << " " << timeToEnd[i] << endl; 
    }
  }
  
  if(verbosity >= 2) {
    cout << "AreaStart: bounds" << endl; 
    cout << bounds << endl; 
    
    for(int i = 0; i < nbTasks; i++) {
      cout << i << " " << timeToEnd[i]<< " " << bounds[i] << endl; 
    }
    
  }
  
  return bestBound; 
}

